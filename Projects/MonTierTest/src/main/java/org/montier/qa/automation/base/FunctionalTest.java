package org.montier.qa.automation.base;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.montier.qa.automation.base.IListener.MyConfigMethodListener;
import org.montier.qa.automation.base.testutils.TestUtils.ValidationFields;
import org.montier.qa.automation.base.utils.FilesUtils;
import org.montier.qa.automation.base.utils.RunTestException;
import org.montier.qa.automation.elements.FadedMessageElement.Message;
import org.montier.qa.automation.elements.SideMenu.Selected;
import org.montier.qa.automation.elements.TopPanel;
import org.montier.qa.automation.elements.TopPanel.ProductType;
import org.montier.qa.automation.toolkit.actions.Login;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

import com.jcraft.jsch.JSchException;

import lombok.Getter;

@Listeners({ MyConfigMethodListener.class })
public class FunctionalTest {
	private static @Getter Driver driver;
	private int step;
	protected static ValidationFields validationValues;
	private static ArrayList<Message> warnings;
	private static @Getter Simulator simulator;
	public static PagesFactory pages;
	public static UsersRoles usersRoles;
	private static TestsResults results;

	@BeforeSuite
	public void initAndLogin(ITestContext ctx) throws RunTestException {
		String suite_name = ctx.getCurrentXmlTest().getSuite().getName();
		prepareLog(suite_name);
		Log.info("Start suite " + suite_name);
		Log.info("Start system environment initialization");
		validationValues = new ValidationFields();
		Log.debug("Init simulator singelton");
		simulator = Simulator.getInstance();
		if (System.getProperty("project.build") != null) {
			Log.debug("Build simulator");
			try {
				simulator.initSimulator();
			} catch (JSchException e) {
				throw new RunTestException(e.getMessage(), e);
			}
		}
		Log.debug("Init driver singelton");
		driver = Driver.getInstance("chrome");
		Log.debug("Init PagesFactory singelton");
		pages = PagesFactory.getInstance();
		Selected.getSelected();
		usersRoles = UsersRoles.getInstance();
		results = TestsResults.getInstance();
		Log.debug("Login to simulator");
		Login.login(System.getProperty("project.build") != null);
		Assert.assertEquals(pages.currentPage.getClass(), PagesFactory.getPages().getProductDefaultPage(pages.getProduct()).getClass(),
				"Page is not System overview as expected");
	}

	@BeforeClass(alwaysRun = true)
	public void beforeClass() {
		Log.info("Before class - Bring out to the default state, user is admin, product is idg and page is system overview");
		if (!UsersRoles.getCurrentUserName().equals("admin")) {
			Login.switchUserToAdmin();
			return;
		}
		Log.debug("Choose IDG as product");
		if (PagesFactory.getInstance().getProduct() != ProductType.IDG)
			TopPanel.getInstance().selectProduct(ProductType.IDG);
		else {
			if (!pages.currentPage.getClass().equals(PagesFactory.getPages().getProductDefaultPage("Dashboards", false, false).getClass())) {
				// HACK FOR BUG!
				if (pages.currentPage.getClass().getSuperclass().getSimpleName().contains("AbstractTransactionPage"))
					PageObject.getFilterElem().addFilterToCurrent("Time", "Last 24 hours");
				// Log.debug("open default page of Dashboards");
				// PagesFactory.getPages().getProductDefaultPage("Dashboards", true, true);
			}
			// Log.debug("reset filters and refresh page after");
			// PageObject.getFilterElem().resetFilters(false, true);
		}
		Log.debug("End of beforeClass method");
	}

	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) {
		Log.debug("Before Method test");
		String test_name = this.getClass().getName().split("tests.")[1] + "." + method.getName();
		Log.debug("Create new log file for the test");
		Log.newLogFile(test_name + "_" + new Date().getTime());
		Log.description("Log of test " + System.getProperty("logFileName"));
		step = 0;
		validationValues.cleanAll();
		Log.debug("open default page of Dashboards and reset filters if needed");
		if (PagesFactory.getInstance().currentPage.getClass()
				.equals(PagesFactory.getPages().getProductDefaultPage("Dashboards", false, false).getClass()))
			PageObject.getFilterElem().resetFilters(false, true);
		else {
			try {
				// PageObject.getFilterElem().resetFilters(false, true);
				PagesFactory.getPages().getProductDefaultPage("Dashboards", true, true);
			} catch (Throwable t) {
				Assert.assertTrue(Pattern.matches("EXPECTED.*BUT FOUND.*", t.getMessage()),
						"Expected for filters unmatch assertion but another exception raised \n" + t.getMessage());
				PageObject.getFilterElem().resetFilters(false, true);
				// PagesFactory.getPages().getProductDefaultPage("Dashboards", true, true);
			}
		}
//		this.addValidations();
		warnings = new ArrayList<Message>();
		Log.startTestCase(test_name);
	}

	@AfterMethod(alwaysRun = true)
	public void after(ITestResult result) {
		Log.debug("Start after configuration method, test " + result.getMethod().getMethodName());
		tearDown(result);
	}

	public static void tearDown(ITestResult result) {
		Log.debug("print end message");
		printEndTestMessage(result);
		Log.endTestCase(result.getMethod().getMethodName());
		Log.debug("switch to parent window");
		PagesFactory.getInstance().currentPage.switchToParentWindow();
		Log.debug("Close open dialog if exists");
		pages.currentPage.closeDialogModal();
	}

	private static void printEndTestMessage(ITestResult result) {
		switch (result.getStatus()) {
		case ITestResult.CREATED:
			Log.error("Test failed while initialization\n*****************************************************");
		case ITestResult.FAILURE:
			saveScreenshot();
			Log.failTest(result.getMethod().getMethodName(), System.getProperty("logFileName"), result.getEndMillis() - result.getStartMillis(),
					getFailureMessage(result));// , notifications);
			break;
		case ITestResult.SUCCESS:
			// Log.info("***Test finished with notifications: " + notifications);
			Log.info("Test Log: " + System.getProperty("logFileName"));
			Log.info("Test ended successfully!");
			break;
		default:
			Log.info("Test end with status " + result.getStatus());
		}
		/*
		 * //TODO: make get notifications works and print it before endMessage Log.debug("Get notifications"); String notifications =
		 * SideMenu.getInstance().getNotifications(); Log.info("***Test finished with notifications: " + notifications);
		 */
	}

	public static void saveScreenshot() {
		String screenshot_file_name = System.getProperty("logsDirectory") + "/screenshots/" + System.getProperty("logFileName") + ".png";
		Log.info("Test failed, Save screenshot to file " + screenshot_file_name);
		driver.takeScreenshot(screenshot_file_name);
	}

	public static String getFailureMessage(ITestResult result) {
		StringBuilder msg = new StringBuilder("Test Failed");
		Throwable t = result.getThrowable();
		if (t != null) {
			String nl = System.getProperty("line.separator");
			msg.append(nl);
			msg.append("    ");
			msg.append(t.toString());
			if (!(t instanceof org.testng.internal.thread.ThreadTimeoutException)) {
				for (StackTraceElement e : t.getStackTrace()) {
					msg.append(nl);
					msg.append("    ");
					msg.append(e.toString());
				}
			}
		}
		return msg.toString();
	}

	public void stepValidation(PageObject page) {
		validationValues.equals(page.pageValidationValues);
	}

	public void stepValidation() {
		// validationValues.equals(page.pageValidationValues);
	}

	public void validate() {
		validationValues.equals(pages.currentPage.pageValidationValues);
	}

	@AfterSuite
	public void cleanUp() {
		driver.getDriver().manage().deleteAllCookies();
		// driver.getDriver().close();
	}

	public void step(String message, Runnable block) {
		step += 1;
		Log.step("STEP " + step + ") " + message);
		block.run();
	}

	public static void addWarning(Message msg) {
		warnings.add(msg);
	}

	private void prepareLog(String suite_name) {
		System.setProperty("logsDirectory", "logs_output");
		Log.newLogFile(suite_name + "_" + new Date().getTime());
		long dir_size = FilesUtils.getSizeOfDirectory(System.getProperty("logsDirectory"));
		Log.debug("Size of log directory = " + dir_size + "bytes");
		if (dir_size > FileUtils.ONE_MB * 5) {
			Log.debug("Size of log directory " + System.getProperty("logsDirectory") + " greater than % MB and need to delete");
			FilesUtils.deleteDirectory(System.getProperty("logsDirectory"));
			Log.debug("folder " + System.getProperty("logsDirectory") + " deleted");
		}
	}
}