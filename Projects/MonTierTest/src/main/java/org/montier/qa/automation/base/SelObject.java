package org.montier.qa.automation.base;

import java.lang.reflect.Field;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.RunTestException;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
import org.testng.Assert;

public abstract class SelObject {
	protected Driver driver = null;

	public SelObject() {
		init();
	}

	public void init() {
		this.driver = Driver.getInstance();
	}

	protected abstract SearchContext getSearchBy();

	protected void initWebElements() {
		Class<?> cls = this.getClass();
		while (cls != null && !cls.getName().equals(PageObject.class.getName())) {
			this.initWebElements(cls);
			cls = cls.getSuperclass();
		}
	}

	protected void initWebElements(Class<?> tClass) {
		// Collect Elements
		Field[] fields = tClass.getDeclaredFields();
		boolean access;
		boolean is_list = false;
		List<WebElement> elems;
		for (int i = 0; i < fields.length; i++) {
			Find find_annotation = fields[i].getAnnotation(Find.class);
			if (find_annotation != null) {
				// Log.debug("\tFind web element " + fields[i].getName());
				try {
					ByPath by_path = new ByPath(find_annotation.by().toString(), find_annotation.value(), find_annotation.from_root());

					access = fields[i].isAccessible();
					fields[i].setAccessible(true);
					if (fields[i].getType().getSimpleName().equals(WebElement.class.getSimpleName())
							|| fields[i].getType().getName().equals(List.class.getName())) {
						elems = this.findElements(by_path);
						if (elems.size() == 0)
							Log.debug("Not found element for field element" + fields[i].getName());
						is_list = fields[i].getType().getName().equals(List.class.getName());
						Assert.assertTrue(elems.size() > 0, "Not find Elements for field " + fields[i].getName());
						fields[i].set(this, is_list ? elems : elems.get(0));
					} else
						fields[i].set(this, by_path);
					fields[i].setAccessible(access);

				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
					throw new RunTestException(e);
				}
			}
		}
	}

	protected WebElement findElement(ByPath by_path) {
		List<WebElement> elements = this.findElements(by_path);
		if (elements.size() == 0)
			Log.debug("Element Not Founded: search thru" + this.getSearchBy().getClass().getSimpleName() + " " + by_path.toString());
		return elements.get(0);
	}

	protected List<WebElement> findElements(ByPath by_path) {
		return this.find(by_path, false);
	}

	private List<WebElement> find(ByPath by_path, boolean... from_root) {
		// Log.debug("\tfind element " + by_path.toString());
		SearchContext srch = this.getSearchBy();
		// srch - srch is the base object we trying to findElement from. there are 2 options: from driver (and then seeking elements from the root of
		// the page) and from element (find webelements exists under the specific element).
		// In most of the cases, srch object will be WebDriver if the class is PageObject and WebElement if it Element.
		if (srch.getClass().getName().equals(RemoteWebElement.class.getName()) && by_path.from_root)
			srch = driver.getDriver();
		if (by_path.byMethodName == FBy.xpath.toString() && srch.getClass().getName().equals(RemoteWebElement.class.getName())
				&& from_root[0] == false && !by_path.path.startsWith(".")) {
			// Log.debug("\tAdded dot (.) in the start of the xpath of sub element");
			by_path.setPath("." + by_path.path);
			// Log.debug("\tfind element " + by_path.toString() + " hash " + srch.hashCode() + " by " + srch.toString());
		}
		List<WebElement> elements = driver.findElements(by_path, srch);
		// Log.debug("\tFound " + elements.size() + " elements");
		return elements;
	}

	protected String getFullPath(ByPath by_path) {
		return by_path.path;
	}

	protected List<WebElement> findElements(ByPath by_path, String... args) {
		ByPath args_path = this.getArgsPath(by_path, args);
		return this.find(args_path, false);
	}

	protected WebElement findElement(ByPath by_path, String... args) {
		List<WebElement> elems = findElements(by_path, args);
		if (elems.size() > 1)
			Log.debug("\tFound " + elems.size() + " Elements");
		WebElement elem = null;
		try {
			elem = elems.get(0);
		} catch (Exception e) {
			Log.warn("Exception while find element " + this.getArgsPath(by_path, args).toString() + ". raised exception " + e.getMessage());
		}
		return elem;
	}

	protected ByPath getArgsPath(ByPath by_path, String... args) {
		String path = by_path.path;
		for (int i = 0; i < args.length; i++)
			path = path.replaceFirst("(\\$)+([A-Z])+(\\$)", args[i]);
		ByPath args_path = new ByPath(by_path.byMethodName, path, by_path.from_root);
		return args_path;
	}

	protected List<WebElement> findElementIfExists(ByPath by_path, String... args) {
		driver.getDriver().manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		List<WebElement> elems = this.findElements(by_path, args);
		driver.getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		return elems;
	}
}
