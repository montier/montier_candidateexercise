package org.montier.qa.automation.elements;

import java.util.ArrayList;
import java.util.Date;

import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.utils.IConditions.BiCondition;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.testng.internal.collections.Pair;

public class SVGLineChartElement extends SVGChartElement {

	public SVGLineChartElement(WebElement chart_elem) {
		super(chart_elem);
	}

	public Pair<Date, Float> getValue(Date date, String line_name) {
		float delta_x = this.clacXDelta(date);
		return this.readValue(delta_x, line_name);
	}

	public boolean validateAllPointsSatisfies(String line_name, BiCondition... conditions) {
		ArrayList<Pair<Date, Float>> points = this.getLinePoints(line_name);
		Boolean res = true;
		Integer point;
		for (int i = 1; i < points.size() - 1; i++) {
			for (BiCondition cond : conditions) {
				point = points.get(i).second().intValue();
				if (!cond.apply(point)) {
					res = false;
					break;
				}
			}
			if (!res)
				break;
		}
		return res;
	}

	public Pair<Date, Float> getHighestValue(String line_name, Date start_date, Date end_date) {
		// int end_x = this.clacXDelta(x_end_date);
		// Timestamp timestamp = new Timestamp(x_end_date.getTime());
		Point p = lines.get(line_name).getHighestPoint(this.fromDateToXLength(start_date), this.fromDateToXLength(end_date));
		return this.readValue(p.getX(), line_name);
	}

	public Pair<Date, Float> getLastValueLowerThan(String line_name, Float value, Date end_date) {
		Float end_x = this.fromDateToXLength(end_date);
		Float y_pix = this.fromYValueToHeight(value);
		Pair<Float, Float> lower_point = this.lines.get(line_name).getLastValueLowerThan(y_pix, end_x);
		return lower_point != null ? pointFromPixToValues(lower_point) : null;
	}

	public int timeRangeValueHigherThan(String line_name, Float value) {
		Pair<Date, Float> last_higher = getLastValueHigherThan(line_name, value);
		Log.debug("Last higher = " + last_higher);
		Pair<Date, Float> first_higher = getLastValueLowerThan(line_name, value, last_higher.first());
		Log.debug("First higher = " + first_higher);
		Date first_higher_date = first_higher == null ? this.x_start_date : first_higher.first();
		Log.info("Time Range when value higher than " + value + " is " + "[" + first_higher_date.toString() + ", " + last_higher.first().toString()
				+ "]");
		return (int) (last_higher.first().getTime() - first_higher_date.getTime()) / 1000;

		// Float pix_value = this.fromYValueToHeight(value);
		// int seconds = Math.round(this.lines.get(line_name).xRangeWhenValueHigherThan(pix_value));
		// return seconds;
	}

	public Pair<Date, Float> getLowerValue(String line_name, Date start_date, Date end_date, float y_value) {
		Point p = lines.get(line_name).getLowerPoint(this.fromDateToXLength(start_date), this.fromDateToXLength(end_date),
				this.fromYValueToHeight(y_value));
		return this.readValue(p.getX(), line_name);
	}

	public Pair<Date, Float> getLowestValue(String line_name, Date start_date, Date end_date) {
		Log.info("Get lowest value in line " + line_name + " between " + start_date + " and " + end_date);
		Point p = lines.get(line_name).getLowestPoint(this.fromDateToXLength(start_date), this.fromDateToXLength(end_date));
		Pair<Date, Float> point_val = this.pointFromPixToValues(p);
		Log.debug("Lowest point is " + point_val);
		return point_val;
	}

	public Pair<Date, Float> getLastValueHigherThan(String line_name, Float value) {
		Float y_pix = this.fromYValueToHeight(value);
		Pair<Float, Float> higher_point = this.lines.get(line_name).getLastValueHigherThan(y_pix);
		return pointFromPixToValues(higher_point);
	}

	public int getLineHighLow(String line_name, float high_val, float low_val, int sec_range) {
		int count_radical = 0;
		int count_cycle = 0;
		Date start_date = this.x_start_date;
		Date end_date = new Date(start_date.getTime() + sec_range);
		high_val = this.fromYValueToHeight(high_val);
		low_val = this.fromYValueToHeight(low_val);
		Log.debug("Range is " + end_date.getTime() + " " + this.x_end_date.getTime());
		while (end_date.getTime() <= this.x_end_date.getTime() - sec_range) {
			Point H = lines.get(line_name).getHighestPoint(this.fromDateToXLength(start_date), this.fromDateToXLength(end_date));
			Point L = lines.get(line_name).getLowestPoint(this.fromDateToXLength(start_date), this.fromDateToXLength(end_date));
			start_date = end_date;
			end_date = new Date((start_date.getTime() + sec_range));
			count_cycle++;
			if (H.y < high_val && L.y > low_val)
				count_radical++;
		}
		int redical_percent = (int) (((float) count_radical / (float) count_cycle) * 100);
		Log.debug("Redical count percent is " + redical_percent);
		return redical_percent;
	}
}
