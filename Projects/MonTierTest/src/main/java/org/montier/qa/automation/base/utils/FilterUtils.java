package org.montier.qa.automation.base.utils;

import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PageObject;

public class FilterUtils {
	public enum FilterStatus {
		NORMAL, URL, LESS;
	}

	public String filterName;
	public FilterStatus filterStatus;

	public FilterUtils(String s) {
		this.filterName = s;
		filterStatus = FilterStatus.NORMAL;
	}

	public FilterUtils(String s, FilterStatus fil_status) {
		this.filterName = s;
		filterStatus = fil_status;
	}

	@Override
	public String toString() {
		return filterName;
	}

	public FilterUtils applyFilter(String value) {
		Log.info("Apply filter " + filterName + " to " + value);
		if (filterStatus == FilterStatus.URL)
			PageObject.getFilterElem().addFilterToCurrent(this.filterName, value);
		else {
			if (filterStatus == FilterStatus.LESS && !PageObject.getFilterElem().isShowMoreOpen())
				PageObject.getFilterElem().clickShowMoreLess();
			PageObject.getFilterElem().applyFilter(this.filterName, value);
		}
		return this;
	}

	public FilterUtils select(int index) {
		PageObject.getFilterElem().selectOption(this.filterName, index);
		return this;
	}

}
