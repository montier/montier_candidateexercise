package org.montier.qa.automation.base;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.montier.qa.automation.elements.TopPanel.ProductType;
import org.montier.qa.automation.pages.RoleViewPage;
import org.montier.qa.automation.pages.RoleViewPage.Permissions;
import org.montier.qa.automation.pages.RoleViewPage.Resources;
import org.montier.qa.automation.pages.RoleViewPage.RolesOfUserCols;
import org.montier.qa.automation.toolkit.actions.UserPermissions;

import com.google.common.collect.Sets;

import lombok.Getter;
import lombok.Setter;

public class UsersRoles {
	public static class UserRole {
		private @Getter String name;
		private @Getter @Setter String userGroup;
		private HashMap<Permissions, String> actionsAndPermissions;
		private HashMap<Resources, HashMap<String, String[]>> deniedResources;
		private HashMap<Resources, HashMap<String, String[]>> allowedResources;

		public UserRole(String user_name) {
			super();
			name = user_name;
			if (user_name.equals("default") || user_name.equals("admin"))
				this.loadDefaultValues();
			else
				this.loadUserPermissions(user_name);
		}

		public void loadDefaultValues() {
			actionsAndPermissions = new HashMap<>();
			for (Permissions permission : Permissions.values()) {
				actionsAndPermissions.put(permission, "YES");
			}
			deniedResources = new HashMap<>();
			allowedResources = new HashMap<>();
			userGroup = "OpDashAdminsGroup";
		}

		public void setRole(Permissions permission, Boolean value) {
			actionsAndPermissions.put(permission, value ? "YES" : "NO");
			// TODO: change it to Boolean
		}

		public String getRole(Permissions permission) {
			return actionsAndPermissions.get(permission);
		}

		/*
		 * public static UserRoles getInstance() { if (INSTANCE == null) INSTANCE = new UserRoles(); return INSTANCE; }
		 */

		public void loadUserPermissions(String user_name) {
			Log.info("Load user " + user_name + " permissions");
			RoleViewPage role_view_page = UserPermissions.loadUserPermissions(user_name);
			actionsAndPermissions = role_view_page.actionsAndPermissions.getValues();
			Log.info("actionsAndPermissions:\n" + actionsAndPermissions);
			deniedResources = role_view_page.getResourcesPerProduct("denied");
			Log.info("deniedResources:\n" + deniedResources);
			allowedResources = role_view_page.getResourcesPerProduct("allowed");
			Log.info("allowedResources:\n" + allowedResources);
			role_view_page.usersInRolesTable.getRow(0).click(RolesOfUserCols.NAME);
			PagesFactory.getPages().userViewPage.openPage();
			userGroup = PagesFactory.getPages().userViewPage.findUserGroups();
			Log.info("userGroup: " + userGroup);
		}

		public HashMap<String, String[]> getDeniedResourcesPerProduct(ProductType product_type) {
			return getResourcesPerProduct(product_type, deniedResources);
		}

		private HashMap<String, String[]> getResourcesPerProduct(ProductType product_type,
				HashMap<Resources, HashMap<String, String[]>> resources_map) {
			HashMap<String, String[]> resources = new HashMap<>();
			if (resources_map.isEmpty())
				return resources;
			switch (product_type) {
			case IDG:
				resources.putAll(resources_map.get(Resources.GATEWAY));
				break;
			case APIC:
				resources.putAll(resources_map.get(Resources.API_CONNECT));
				break;
			}
			resources.putAll(resources_map.get(Resources.GATEWAY_APIC));
			return resources;
		}

		public HashMap<String, String[]> getAllowedResourcesPerProduct(ProductType product_type) {
			return getResourcesPerProduct(product_type, allowedResources);
		}

		public Boolean getPermission(Permissions permission) {
			String yes_no = actionsAndPermissions.get(permission);
			return yes_no.equals("YES");
		}

		private HashMap<String, String[]> getResources(String type) {
			ProductType product = PagesFactory.getInstance().getProduct();
			return type.equals("denied") ? this.getDeniedResourcesPerProduct(product) : this.getAllowedResourcesPerProduct(product);

		}

		public HashMap<String, String[]> getDeniedResources() {
			return getResources("denied");
		}

		public HashMap<String, String[]> getAllowedResources() {
			return getResources("allowed");
		}
	}

	private static @Getter @Setter String currentUserName = null;
	private static UsersRoles INSTANCE = null;
	private static HashMap<String, UserRole> roles = new HashMap<>();

	private UsersRoles() {
		super();
		roles = new HashMap<>();
		currentUserName = "admin";// TODO: getCurrentUserName
		roles.put(currentUserName, new UserRole(currentUserName));
	}

	public static UsersRoles getInstance() {
		if (INSTANCE == null)
			INSTANCE = new UsersRoles();
		return INSTANCE;
	}

	public void addRole(String user_name) {
		roles.put(user_name, new UserRole(user_name));
	}

	public UserRole getUserRole(String user_name) {
		return roles.get(user_name);
	}

	public static Boolean getPermissionOfCurrentUser(Permissions permission) {
		return getCurrentUserRole().getPermission(permission);
	}

	public static UserRole getCurrentUserRole() {
		return currentUserName != null && roles.containsKey(currentUserName) ? roles.get(currentUserName) : null;
	}

	public static HashMap<String, String[]> getCurrentUserDeniedResources() {
		return getCurrentUserResources("denied");
	}

	private static HashMap<String, String[]> getCurrentUserResources(String type) {
		if (!roles.containsKey(currentUserName)) {
			Log.error("No usersRoles for currentUser " + currentUserName + " take default role");
			// user = "default";
		}
		return getCurrentUserRole().getResources(type);

	}

	public static HashMap<String, String[]> getCurrentUserAllowedResources() {
		return getCurrentUserRole().getAllowedResources();
	}

	public static boolean isResourceAllowedForCurrentUser(String resource_type, String resource) {
		String[] allowed = UsersRoles.getCurrentUserAllowedResources().get(resource_type);
		Boolean is_allowed = allowed == null || ArrayUtils.contains(allowed, resource);
		String[] denied = UsersRoles.getCurrentUserDeniedResources().get(resource_type);
		Boolean is_not_denied = denied == null || !ArrayUtils.contains(denied, resource);
		return is_allowed && is_not_denied;
	}

	public static String[] getAllowedResourceFromList(String resource_type, String[] resource_list) {
		Set<String> sys_devices = Sets.newHashSet(resource_list);

		Set<String> allowed_resource_list = new HashSet<String>();
		String[] allowed = getCurrentUserAllowedResources().get(resource_type);
		Set<String> allowed_resources = allowed != null ? Sets.newHashSet(allowed) : new HashSet<String>();
		String[] denied = getCurrentUserDeniedResources().get(resource_type);
		Set<String> denied_resources = denied != null ? Sets.newHashSet(denied) : new HashSet<String>();
		if (allowed_resources != null)// TODO: check if it actually return null
			allowed_resource_list = Sets.intersection(sys_devices, allowed_resources);
		if (denied_resources != null)
			allowed_resource_list = Sets.difference(allowed_resource_list, denied_resources);
		return allowed_resource_list.toArray(new String[allowed_resource_list.size()]);
	}

}
