package org.montier.qa.automation.base.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

public class FilesUtils {

	public static TreeMap<String, String> readKeyValueFile(String file_path, boolean resources) {
		TreeMap<String, String> map = new TreeMap<String, String>();
		List<String> lines = null;
		try (InputStream inputStream = resources ? FilesUtils.class.getClassLoader().getResourceAsStream(file_path)
				: new FileInputStream(new File(file_path))) {
			lines = IOUtils.readLines(inputStream, StandardCharsets.UTF_8);
			for (String line : lines) {
				if (line.trim().length() == 0)
					continue;
				if (line.trim().isEmpty())
					continue;
				String[] pair = line.trim().split("=", 2);
				map.put(pair[0], pair[1].isEmpty() ? pair[1] : pair[1].trim());
			}
		} catch (IOException e1) {
			e1.printStackTrace();
			throw new RunTestException(e1);
		}
		return map;
	}

	public static void deleteDirectory(String dir_name) {
		File dir = new File(dir_name);
		deleteDirectory(dir);
	}

	private static void deleteDirectory(File dir) {
		for (File file : dir.listFiles()) {
			if (file.isDirectory())
				deleteDirectory(file);
			else
				file.delete();
		}
		dir.delete();
	}

	public static long getSizeOfDirectory(String dir_name) {
		return FileUtils.sizeOfDirectory(new File(dir_name));
	}
}
