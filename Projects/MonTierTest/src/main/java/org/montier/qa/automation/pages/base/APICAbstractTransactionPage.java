package org.montier.qa.automation.pages.base;

import java.util.ArrayList;

import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.UsersRoles;
import org.montier.qa.automation.base.testutils.TestUtils;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.FilterUtils.FilterStatus;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.elements.ElapsedTimeImageElementAPIC;
import org.montier.qa.automation.elements.KeyValuePanelElement;
import org.montier.qa.automation.elements.TabsPanelElement;
import org.montier.qa.automation.elements.TabsPanelElement.Tab;
import org.montier.qa.automation.pages.RoleViewPage.Permissions;
import org.montier.qa.automation.pages.apic.APICTransactionExtendedLatencyPage;
import org.montier.qa.automation.pages.apic.APICTransactionPayloadPage;
import org.montier.qa.automation.pages.apic.APICTransactionRawMessagesPage;
import org.montier.qa.automation.pages.apic.APICTransactionSideCallsPage;
import org.montier.qa.automation.toolkit.elements.RequestFlowElement;
import org.montier.qa.automation.toolkit.elements.RequestFlowElement.PoliciesList;
import org.montier.qa.automation.toolkit.elements.TransactionAnalysisPanelAPIC;
import org.montier.qa.automation.toolkit.elements.TransactionComponents;
import org.openqa.selenium.WebElement;

import lombok.AllArgsConstructor;
import lombok.Getter;

public abstract class APICAbstractTransactionPage extends APICAbstractInvestigatePage {

	@AllArgsConstructor
	public static enum APICTransRawVal {
		TIME("Time"), STATUS("Status"), DEVICE("Device"), DOMAIN("Domain"), API_NAME("API Name"),
		API_VERSION("API Version"), TRANSACTION_ID("Transaction ID"), GL_TRANS_ID("Gl. Transaction ID"),
		BE_RESPONSE_CODE("BE Response Code"), FE_RESPONSE_CODE("FE Response Code"), IN_URL("In URL"), IN_URI("In URI"),
		QUEARY_STRING("Query String"), PROVIDER_ORG("Provider Org"), CATALOG_NAME("Catalog Name"),
		SPACE_NAME("Space Name"), PRODUCT_ID("Product ID"), PRODUCT_NAME("Product Name"), PLAN_NAME("Plan Name"),
		HTTP_METHOD("HTTP Method"), CONSUMER_ORG_ID("Consumer Org ID"), CONSUMER_ORG("Consumer Org"),
		CONSUMER_APP("Consumer App"), CLIENT_ID("Client ID"), CLIENT_IP("Client IP"), OAUTH_TSCOPE("OAuth Scope"),
		OAUTH_RES_OWNER("OAuth Res. Owner"), OAUTH_TOKEN_FROM("OAuth Token From"),
		OAUTH_TOKEN_UNTIL("OAuth Token Until"),;

		// PLAN_VERSION("Plan Version"), Deleted

		private final @Getter String stringValue;

		@Override
		public String toString() {
			return stringValue;
		}
	}

	public class TransRawFilters extends PageFilters {
		public FilterUtils TIME = new FilterUtils("Time", FilterStatus.URL);
		public FilterUtils DEVICE = new FilterUtils("Device", FilterStatus.URL);
		public FilterUtils DOMAIN = new FilterUtils("Domain", FilterStatus.URL);
		public FilterUtils TRANS_ID = new FilterUtils("Trans. ID", FilterStatus.URL);
		public FilterUtils CATALOG = new FilterUtils("Catalog", FilterStatus.URL);
		public FilterUtils API_NAME = new FilterUtils("API Name", FilterStatus.URL);

		@Override
		public boolean allAreUrlFilters() {
			return true;
		}
	}

	@Find(by = FBy.xpath, value = ".//span[contains(text(),'Transaction $TITLE$')]//ancestor::div[contains(@class, 'panel panel-default')]")
	ByPath trans_details_panel;
	@Find(by = FBy.xpath, value = "//div[text()=' Transaction Analysis ']/parent::div[contains(@class, 'panel')]")
	ByPath trans_analysis_panel;
	@Find(by = FBy.xpath, value = "//div[contains(@class, 'qaTransactionPageDetailsPanel')]")
	WebElement menuPanelElement;
//	@Find(by = FBy.xpath, value = "//div[text()='Request Flow']/following-sibling::div")
//	ByPath requestFlow;
//	@Find(by = FBy.xpath, value = "/html/body/div[3]/div/div/div/div/div/div[3]/div[2]/div/div/div[2]/div[2]/div[7]/div/div")
	@Find(by = FBy.xpath, value = "//div[contains(@class, 'qaTransactionPagePolicyGraph')]")
	ByPath requestFlow;
//	@Find(by = FBy.xpath, value = "//div[text()='Response Flow']/following-sibling::div")
	ByPath responseflow;

	public String transID;
	public KeyValuePanelElement<APICTransRawVal> transDetails;
	public TransactionAnalysisPanelAPIC transactionAnalysisPanel;
	protected TransactionComponents transactionComponents;
	public RequestFlowElement requestFlowImage;
	public RequestFlowElement responseFlowImage;
	public TabsPanelElement tabsPanelElem;

	public APICAbstractTransactionPage() {
		super();
	}

	@Override
	public void initPageElement() {
		super.initPageElement();
		this.transDetails = new KeyValuePanelElement<APICTransRawVal>(
				this.findElement(trans_details_panel, this.transID), APICTransRawVal.values());
		this.tabsPanelElem = new TabsPanelElement(menuPanelElement, getTabs());
		this.getPageFilters().DEVICE.applyFilter(this.transDetails.getValueOf(APICTransRawVal.DEVICE));
		this.getPageFilters().DOMAIN.applyFilter(this.transDetails.getValueOf(APICTransRawVal.DOMAIN));
		this.getPageFilters().CATALOG.applyFilter(this.transDetails.getValueOf(APICTransRawVal.CATALOG_NAME));
		this.getPageFilters().TRANS_ID.applyFilter(this.transDetails.getValueOf(APICTransRawVal.TRANSACTION_ID));
		this.initValidationValues();
		transactionAnalysisPanel = new TransactionAnalysisPanelAPIC(this.findElement(trans_analysis_panel),
				transDetails.getValueOf(APICTransRawVal.STATUS));
		transactionComponents = new TransactionComponents(menuPanelElement, APICTransactionRawMessagesPage.class,
				APICTransactionPayloadPage.class, APICTransactionSideCallsPage.class,
				APICTransactionExtendedLatencyPage.class);
		WebElement flow_elem = this.findElement(requestFlow);
		if (flow_elem.getText().equals("No data to display"))
			requestFlowImage = null;
		else
			requestFlowImage = new RequestFlowElement(this.findElement(requestFlow));

//		flow_elem = this.findElement(responseflow);
//		if (flow_elem.getText().equals("No data to display"))
//			responseFlowImage = null;
//		else
//			responseFlowImage = new RequestFlowElement(this.findElement(responseflow));
	}

	private Tab[] getTabs() {
		ArrayList<Tab> tabs = new ArrayList<Tab>();
		tabs.add(new Tab("Raw Messages", APICTransactionRawMessagesPage.class,
				!UsersRoles.getPermissionOfCurrentUser(Permissions.AllowRawMessages)));
		tabs.add(new Tab("Payload", APICTransactionPayloadPage.class,
				!UsersRoles.getPermissionOfCurrentUser(Permissions.AllowPayload)));
		tabs.add(new Tab("Extended Latency", APICTransactionExtendedLatencyPage.class));
		tabs.add(new Tab("Side Calls", APICTransactionSideCallsPage.class));
		Tab[] arr = new Tab[tabs.size()];
		return tabs.toArray(arr);
	}

	@Override
	public void openPage(String... transID) {
		this.transID = "";// transID != null ? transID[0] : null;
		super.openPage();
	}

	public abstract Object getComponentPanel();

	@Override
	protected void initValidationValues() {
		// this.addValidationField("service",
		// this.transDetails.getValueOf(APICTransRawVal.TIME));
		this.addValidationField("domain", this.transDetails.getValueOf(APICTransRawVal.DOMAIN));
		this.addValidationField("device", this.transDetails.getValueOf(APICTransRawVal.DEVICE));
	}

	@Override
	public TransRawFilters getPageFilters() {
		if (this.pageFilters == null)
			this.pageFilters = new TransRawFilters();
		return (TransRawFilters) this.pageFilters;
	}

	public void validateElapsedTimeImageVsFlows() {
		Log.debug("Validate ms values of flow vs elapsed times image");
		int num_times = 6;
		ElapsedTimeImageElementAPIC image_elapsed = this.transactionAnalysisPanel.elapsedTimeImage;
		PoliciesList request_policies_list = this.requestFlowImage.getPoliciesList();
		PoliciesList response_policies_list = this.responseFlowImage.getPoliciesList();
		String[] ms_types = { "backEndRequest", "FrontEndRequest", "dataPowerRequestProcessingVal",
				"frontEndResponseVal", "dataPowerResponeseProcessingVal", "backEndResponseVal" };
		Integer[] elapsed_time_image_ms = { image_elapsed.frontEndRequestVal(),
				image_elapsed.dataPowerRequestProcessingVal(), image_elapsed.backEndResponseVal() };
		Object[] flow_ms = { request_policies_list.getBackEndPolicy().getPolicyElement().getMs(),
				request_policies_list.getFrontEndPolicy().getPolicyElement().getMs(),
				this.requestFlowImage.getSumPoliciesMS(),
				response_policies_list.getFrontEndPolicy().getPolicyElement().getMs(),
				this.responseFlowImage.getSumPoliciesMS(),
				response_policies_list.getBackEndPolicy().getPolicyElement().getMs() };

		int _flow_ms;
		for (int i = 0; i < num_times; i++) {
			_flow_ms = TestUtils.StringUtils.parseMS(flow_ms[i].toString());
			// Assert.assertTrue(elapsed_time_image_ms[i].equals(_flow_ms),
			// ms_types[i] + "as different values, flow value is " + _flow_ms + " and image
			// value is " + elapsed_time_image_ms[i]);
		}
	}
}
