package org.montier.qa.automation.elements;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.montier.qa.automation.base.Element;
import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PagesFactory;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table.Col;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class TableElement extends Element {

	public static class PagerTable extends Element {
		@Find(by = FBy.xpath, value = "//i[contains(@class, 'angle-right')]/parent::a")
		ByPath right_angle;
		@Find(by = FBy.xpath, value = "//i[contains(@class, 'angle-left')]/parent::a")
		ByPath left_angle;
		@Find(by = FBy.xpath, value = "//li//strong/parent::span")
		ByPath num_rows;
		@Find(by = FBy.xpath, value = "//a[text()=$NUM$]")
		ByPath pager_num;

		public PagerTable(WebElement base_elem) {
			super(base_elem);
		}

		public int getNumRows() {
			WebElement num = this.findElement(num_rows);
			return Integer.parseInt(num.getText().split("of")[1].replace(",", "").trim());
		}

		public int getNumRowsInPage() {
			WebElement num = this.findElement(num_rows);
			String[] splitted = num.getText().split("of")[0].split("-");
			int num0 = Integer.parseInt(splitted[0].trim());
			int num1 = Integer.parseInt(splitted[1].trim());
			return num1 - num0 + 1;
		}

		public int getStartRange() {
			WebElement num = this.findElement(num_rows);
			String[] splitted = num.getText().split("of")[0].split("-");
			int num0 = Integer.parseInt(splitted[0].trim());
			return num0;
		}

		public void clickNextRowsPage() {
			int snum = getStartRange();
			int num_rows_in_page = getNumRowsInPage();
			if (snum + num_rows_in_page > getNumRows())
				Assert.assertTrue(this.findElement(right_angle).getAttribute("class").equals("deisabled"));
			this.findElement(right_angle).click();
			PagesFactory.getInstance().currentPage.refresh();
			int snumi = getStartRange();
			Assert.assertTrue(snumi == snum + num_rows_in_page);
		}
	}

	@Find(by = FBy.xpath, value = "//thead//th")
	ByPath table_head_elements;
	@Find(by = FBy.xpath, value = ".//tbody//tr[$ROW$]//td[$COL$]/child::*")
	ByPath table_element;
	@Find(by = FBy.xpath, value = ".//tbody//tr[$ROW$]//td[$COL$]//span")
	ByPath table_element_span;
	@Find(by = FBy.xpath, value = ".//tbody//tr[$ROW$]//td/child::*")
	ByPath table_row_ceils;
	@Find(by = FBy.xpath, value = ".//tbody//tr")
	ByPath table_rows;
	@Find(by = FBy.xpath, value = ".//ancestor::div[contains(@class,'panel-body')]//ul[contains(@class, 'Pager')]//li[1]")
	ByPath table_num_rows_pagers;
	@Find(by = FBy.xpath, value = ".//ancestor::div[contains(@class,'panel-body')]//ul[contains(@class, 'Pager')]")
	ByPath table_pager;
	@Find(by = FBy.xpath, value = ".//tbody//tr[$ROW$]//td[$COL$]$HOVERXPATH$")
	ByPath mouse_hover_ceil;
	@Find(by = FBy.xpath, value = "//*[@class='NoDataMessage' or text()='No data to display']")

	ByPath no_data_to_display_msg;

	public String tableName;
	public ArrayList<String> colNames;
	public WebElement tableElement;
	public int current_row;
	public PagerTable pager;
	public int numRowsInPage;

	public TableElement(WebElement table_elem, ArrayList<Col> head_names) {
		super(table_elem);
		this.tableName = table_elem.getAttribute("class").split(" ")[0];
		this.tableElement = table_elem;
		this.colNames = new ArrayList<String>();
		boolean is_there_data_in_table = this.checkTableHead(head_names);
		this.current_row = 1;
		pager = null;
		if (!is_there_data_in_table)
			return;

		if (this.findElementIfExists(table_pager).size() > 0)
			pager = new PagerTable(this.findElement(table_pager));
		numRowsInPage = pager == null ? numRows() : pager.getNumRowsInPage();
	}

	public List<WebElement> getTableHead() {
		return this.findElements(table_head_elements);
	}

	public boolean checkTableHead(ArrayList<Col> tableColNames) {
		Log.debug("Check table head as expected");
		List<WebElement> table_head = this.findElementIfExists(table_head_elements);
		if (table_head.size() == 0) {
			Log.debug("Table is empty, expected for No data to display message");
			Assert.assertEquals(this.findElement(no_data_to_display_msg).getText(), "No data to display",
					"message NO DATA TO DISPLAY expected if there is no table head");
			return false;
		}
		List<String> expected_names_list = tableColNames.stream().map(col -> col.getName()).collect(Collectors.toList());
		List<String> actual_names_list = table_head.stream().map(elem -> elem.getText()).collect(Collectors.toList());
		boolean is_equals = true;
		if (expected_names_list.size() == actual_names_list.size())
			for (int i = 0; i < expected_names_list.size(); i++)
				is_equals = expected_names_list.get(i).equals(actual_names_list.get(i));
		else
			is_equals = false;
		if (!is_equals)
			Assert.fail("Table columns not as expected. expected: " + expected_names_list + " actual: " + actual_names_list);
		return true;
		// String expected_names = tableColNames.stream())
		/*
		 * Assert.assertTrue(table_head.size() == tableColNames.size(), "Table cols name size not equal between what we expected "); for (int i = 0; i
		 * < table_head.size(); i++) { Assert.assertEquals(tableColNames.get(i).getName().trim(), table_head.get(i).getText().trim(), <<<<<<< HEAD
		 * "Table column name not as expected: found "+ table_head.get(i).getText().trim() +" instead of "+tableColNames.get(i).getName().trim());
		 * ======= "Table column name not as expected: found " + table_head.get(i).getText().trim() + " instead of " +
		 * tableColNames.get(i).getName().trim()); >>>>>>> origin/MNTR-1868_FlowElement colNames.add(i, tableColNames.get(i).getName()); }
		 */
	}

	public WebElement getCeilElement(Integer row, String col_name) {
		return this.findElement(table_element, row.toString(), this.getColIndex(col_name).toString());
	}

	public WebElement getCeilElement(Integer row, Integer col) {
		return this.findElement(table_element, row.toString(), col.toString());
	}

	public WebElement getCeilElement(List<WebElement> ceils, Integer col) {
		return ceils.get(col - 1);
	}

	public String getCeilText(Integer row, Integer col) {
		return this.findElement(table_element, row.toString(), col.toString()).getText();
	}

	public String getCeilText(List<WebElement> row_ceils, String col_name) {
		int i = this.getColIndex(col_name);
		return row_ceils.get(i).getText();
	}

	public Integer getColIndex(String col_name) {
		return colNames.indexOf(col_name) + 1;
	}

	public Integer numRows() {
		Integer num_rows;
		if (pager != null)
			num_rows = pager.getNumRows();
		else
			num_rows = this.findElementIfExists(table_rows).size();
		Log.debug("num rows = " + num_rows);
		return num_rows;
	}
	/*
	 * public int getRowValueGreaterThan(Integer col, Integer value) { //TODO: one function all methods/conditions int num_rows = this.numRows(); int
	 * i=0; WebElement ceil = null; int ceil_value; while(i<=num_rows) { i++; ceil = getCeilElement(i, col); if (!ceil.getText().equals("") && value
	 * <= Integer.parseInt(ceil.getText())) break; } return i; }
	 */

	public int getRowValueEqual(int col, String value) {
		int num_rows = this.numRows();
		String ceil_text = null;
		int i;
		for (i = 1; i <= num_rows; i++) {
			ceil_text = getCeilElement(i, col).getText();
			if (value.equals(ceil_text))
				break;
		}
		return i > num_rows ? 0 : i;
	}

	public int getRowValueContains(String col_name, String value) {
		int num_rows = this.numRows();
		String ceil_text = null;
		int i;
		for (i = 1; i <= num_rows; i++) {
			ceil_text = getCeilElement(i, col_name).getText();
			if (ceil_text.contains(value))
				break;
		}
		return i > num_rows ? 0 : i;
	}

	public String clickCeil(Integer row_num, String col_name) {
		WebElement ceil = getCeilElement(row_num, col_name);
		String ceil_text = ceil.getText();
		ceil.click();
		return ceil_text;
	}

	/*
	 * public boolean compareRegex(int row, Integer col, String regex_pattern) { String ceil_string = this.getCeilElement(row, col).getText(); Pattern
	 * r = Pattern.compile(regex_pattern); Matcher m = r.matcher(ceil_string); return m.find(); }
	 */

	private WebElement getCeilSpanElement(Integer row, Integer col) {
		return this.findElement(table_element_span, row.toString(), col.toString());
	}

	public void MouseHoverAndClick(Integer row_num, Integer col, String hover_elem_xpath) {
		WebElement hover_element = getMouseHoverFadedElement(row_num, col, hover_elem_xpath);
		hover_element.click();
	}

	public void mouseHoverOnCeil(Integer row_num, Integer col) {
		WebElement ceil = getCeilSpanElement(row_num, col);
		driver.moveToElement(ceil, 1, 0);
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public WebElement getMouseHoverFadedElement(Integer row_num, Integer col, String hover_elem_xpath) {
		WebElement hover_element = null;
		mouseHoverOnCeil(row_num, col);
		try {
			hover_element = this.findElement(mouse_hover_ceil, row_num.toString(), col.toString(), hover_elem_xpath);
		} catch (Exception e) {
			mouseHoverOnCeil(row_num, col);
			hover_element = this.findElement(mouse_hover_ceil, row_num.toString(), col.toString(), hover_elem_xpath);
		}
		return hover_element;
	}

	public List<WebElement> getRowCeils(int row) {
		return this.findElements(table_row_ceils, String.valueOf(row));
	}
}
