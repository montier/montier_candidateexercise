package org.montier.qa.automation.pages;

import java.util.concurrent.TimeUnit;

import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.elements.FadedMessageElement.Message;
import org.montier.qa.automation.elements.FadedMessageElement.MessageType;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class AlertViewPage extends PageObject {
	@Find(by = FBy.xpath, value = "//div[contains(@class, 'panel-body')]//button[contains(@class,'btn-danger')]", init = true)
	WebElement deleteButton;
	// @Find(by=By.xpath,value="//div[@class='ConfirmationModal']//button[contains(@class, 'btn-danger')])", init=true)
	// WebElement confirmDialog;
	@Find(by = FBy.xpath, value = "//div[@class='ConfirmationModal']//button[contains(@class, 'btn-danger')]", init = false)
	ByPath deleteDialogButton;

	public AlertViewPage() {
		super();
	}

	public void deleteAlert() {
		this.deleteButton.click();
		try {
			TimeUnit.SECONDS.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// Assert.assertEquals(this.confirmDialog.getAttribute("aria-hidden"), "false");
		this.findElement(deleteDialogButton).click();
		Message alert_message = this.fadedMessages.getMessage(MessageType.SUCCESS);
		Assert.assertTrue(alert_message.getText().contains("Alert deleted successfully"));
	}

	@Override
	public void initPageElement() {
		// empty override
	}

	@Override
	public PageFilters getPageFilters() {
		return null;
	}
}
