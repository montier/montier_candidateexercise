package org.montier.qa.automation.pages.apic;

import java.util.Arrays;

import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PagesFactory;
import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.montier.qa.automation.base.utils.Table.TableRow;
import org.montier.qa.automation.pages.base.APICAbstractInvestigatePage;
import org.montier.qa.automation.toolkit.elements.TransactionsTable.TransCols;
import org.openqa.selenium.WebElement;

public class APICEarlyFailedPage extends APICAbstractInvestigatePage {
	public static class APICEarlyFailedTransCols extends TableCols {
		public static final Col API_NAME = new Col("API Name");
		public static final Col TIME = new Col("Time");
		public static final Col DEVICE = new Col("Device");
		public static final Col DOMAIN = new Col("Domain");
		public static final Col CATALOG_NAME = new Col("Catalog Name");
		public static final Col SPACE_NAME = new Col("Space Name");
		public static final Col APP_NAME = new Col("App Name");
		public static final Col CLIENT_ID = new Col("Client IP");
		public static final Col TRANS_ID = new Col("Trans. ID");
		public static final Col CLIENT_IP = new Col("Client IP");
		public static final Col GL_TRANS_ID = new Col("Gl. Trans. ID");

		public APICEarlyFailedTransCols() {
			super();
		}

		@Override
		public void setColsIndices() {
			colsIndices.addAll(Arrays.asList(API_NAME, TIME, DEVICE, DOMAIN, CATALOG_NAME, SPACE_NAME, APP_NAME,
					CLIENT_ID, TRANS_ID, CLIENT_IP, GL_TRANS_ID));

		}
	}

	public class APICEarlyFailedTransactionsFilters extends PageFilters {
		public FilterUtils FREETEXT = new FilterUtils("Apply");
		public FilterUtils TIME = new FilterUtils("Time");
		public FilterUtils DEVICE = new FilterUtils("Device");
		public FilterUtils DOMAIN = new FilterUtils("Domain");
		public FilterUtils CATALOG = new FilterUtils("Catalog");
		public FilterUtils SPACE = new FilterUtils("Space");
		public FilterUtils PRODUCT = new FilterUtils("Product");
		public FilterUtils PLAN = new FilterUtils("Plan");
		public FilterUtils API_NAME = new FilterUtils("API Name");
		public FilterUtils API_VERSION = new FilterUtils("API Version");
		public FilterUtils APP_NAME = new FilterUtils("App Name");
		public FilterUtils TRANS_ID = new FilterUtils("Trans. ID");
		public FilterUtils GLOBAL_TRANS_ID = new FilterUtils("Global Trans. ID");
		public FilterUtils HTTP_METHOD = new FilterUtils("HTTP Method");
		public FilterUtils IN_URL = new FilterUtils("In URL");
		public FilterUtils IN_URI = new FilterUtils("In URI");
		public FilterUtils CONSUMER_ORG_NAME = new FilterUtils("Consumer Org Name");
		public FilterUtils CLIENT_ID = new FilterUtils("Client ID");
		public FilterUtils CLIENT_IP = new FilterUtils("Client IP");
		public FilterUtils FE_HTTP_RES_CODE = new FilterUtils("FE HTTP Res. Code");
		public FilterUtils ERROR_REASON = new FilterUtils("Error Reason");
		public FilterUtils ERROR_MESSAGE = new FilterUtils("Error Message");
	}

	@Find(by = FBy.xpath, value = "//div[contains(@class, 'EarlyFailedTransactionsGrid')]")
	protected WebElement early_failed_transactions_grid;
	public Table transTable;

	public APICEarlyFailedPage() {
		super();
	}

	@Override
	public void initPageElement() {
		this.transTable = new Table(early_failed_transactions_grid, new APICEarlyFailedTransCols());
	}

	@Override
	public APICEarlyFailedTransactionsFilters getPageFilters() {
		if (this.pageFilters == null)
			this.pageFilters = new APICEarlyFailedTransactionsFilters();
		return (APICEarlyFailedTransactionsFilters) this.pageFilters;
	}

	public void clickTransID(TableRow row) {
		String trans_id_text = row.getCeil(TransCols.TRANS_ID).getText();
		Log.info("Click on Trnas.ID " + trans_id_text);
		row.getCeil(TransCols.TRANS_ID).click();
		PagesFactory.getPages().getTransRawMessagesPage().openPage(trans_id_text);
	}
}
