package org.montier.qa.automation.toolkit.elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeMap;

import org.montier.qa.automation.base.Element;
import org.montier.qa.automation.base.testutils.TestUtils;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.toolkit.elements.ApicPolicyDetails.ContextPolicyCols;
import org.montier.qa.automation.toolkit.elements.ApicPolicyDetails.SideCallsPolicyCols;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import lombok.Getter;
import lombok.Setter;

public class RequestFlowElement extends Element {
	public static class PolicyElem extends Element {
		@Find(by = FBy.className, value = "panel-heading")
		ByPath apicHeading;
		@Find(by = FBy.className, value = "panel-body")
		ByPath apicBody;
		@Find(by = FBy.xpath, value = ".//child::div[2]")
		ByPath FrontEndBack;

		private @Getter String name;
		private @Getter String ms;
		private @Getter String elem_type;

		public PolicyElem(WebElement elem) {
			super(elem);
			String[] className = elem.getAttribute("class").split(" ");
			elem_type = className[0];
			if (elem_type.contains("ApicPolicyFrontEndBackEndBox")) {
				name = elem_type;
				ms = this.findElement(FrontEndBack).getText();
				return;
			}
			name = this.findElement(apicHeading).getText();
			ms = this.findElement(apicBody).getText();
		}

		public boolean validate(PolicyElem policy_elem) {
			return elem_type.equals(policy_elem.getElem_type()) && name.equals(policy_elem.getName())
					&& ms.equals(policy_elem.getMs());
		}

	}

	public static class ApicPolicy {
		private @Getter @Setter PolicyElem policyElement;
		private @Getter @Setter String name;
		private @Getter @Setter String type;
		// private @Getter @Setter String direction;
		private @Getter @Setter List<TreeMap<String, String>> sideCalls;
		private @Getter @Setter List<TreeMap<String, String>> contextVariables;

		public ApicPolicy(WebElement elem) {
			policyElement = new PolicyElem(elem);
			name = policyElement.getName();
			type = null;
			// direction = null;
			sideCalls = new ArrayList<TreeMap<String, String>>();
			contextVariables = new ArrayList<TreeMap<String, String>>();
		}

		public ApicPolicy(String name, String type, String direction) {
			this.name = name;
			this.type = type;
			// this.direction = direction;
			sideCalls = new ArrayList<TreeMap<String, String>>();
			contextVariables = new ArrayList<TreeMap<String, String>>();
		}

		public void addSideCall(String type, String status, String elapsed_time, String details) {
			TreeMap<String, String> side_call = new TreeMap<>();
			side_call.put(SideCallsPolicyCols.TYPE.getName(), type);
			side_call.put(SideCallsPolicyCols.STATUS.getName(), status);
			side_call.put(SideCallsPolicyCols.ELAPSED_MS.getName(), elapsed_time);
			side_call.put(SideCallsPolicyCols.DETAILS.getName(), details);
			this.sideCalls.add(side_call);
		}

		public void addContextVariable(String name, String value) {
			TreeMap<String, String> context_variable = new TreeMap<>();
			context_variable.put(ContextPolicyCols.NAME.getName(), name);
			context_variable.put(ContextPolicyCols.VALUE.getName(), value);
			this.contextVariables.add(context_variable);
		}

		public boolean validateData(ApicPolicy policy) {
			boolean details_as_expected = this.name.equals(policy.getName()) && this.type.equals(policy.getType());
			// this.direction.equals(policy.getDirection()) &&

			boolean side_calls_as_expected = validateSideCalls(policy.getSideCalls());
			boolean context_variables_as_expected = validateContextVariables(policy.getContextVariables());
			return details_as_expected && side_calls_as_expected && context_variables_as_expected;
		}

		private boolean validateSideCalls(List<TreeMap<String, String>> expected_side_calls) {
			boolean as_expected = true;
			TreeMap<String, String> expected_sc, current_sc;
			for (int i = 0; i < expected_side_calls.size(); i++) {
				expected_sc = expected_side_calls.get(i);
				current_sc = this.getSideCalls().get(i);
				as_expected = expected_sc.get(SideCallsPolicyCols.TYPE.getName())
						.equals(current_sc.get(SideCallsPolicyCols.TYPE.getName()))
						&& expected_sc.get(SideCallsPolicyCols.STATUS.getName())
								.equals(current_sc.get(SideCallsPolicyCols.STATUS.getName()))
						&& expected_sc.get(SideCallsPolicyCols.DETAILS.getName())
								.equals(current_sc.get(SideCallsPolicyCols.DETAILS.getName()));
				if (!as_expected)
					return false;
			}
			as_expected = expected_side_calls.size() == this.getSideCalls().size();
			return as_expected;
		}

		private boolean validateContextVariables(List<TreeMap<String, String>> expected_context_variables) {

			boolean as_expected = true;
			TreeMap<String, String> expected_sc, current_sc;
			for (int i = 0; i < expected_context_variables.size(); i++) {
				expected_sc = expected_context_variables.get(i);
				current_sc = this.getContextVariables().get(i);
				as_expected = expected_sc.get(ContextPolicyCols.NAME.getName())
						.equals(current_sc.get(ContextPolicyCols.NAME.getName()))
						&& expected_sc.get(ContextPolicyCols.VALUE.getName())
								.equals(current_sc.get(ContextPolicyCols.VALUE.getName()));
				if (!as_expected)
					return false;
			}
			as_expected = expected_context_variables.size() == this.getContextVariables().size();
			return as_expected;
		}

		/*
		 * public boolean isDataEquals(ApicPolicy apicPolicy) { return
		 * name.equals(apicPolicy.getName()) &&
		 * policy_type.equals(apicPolicy.getPolicy_type()) &&
		 * direction.equals(apicPolicy.getDirection()); }
		 */
		@Override
		public String toString() {
			return "Name=" + name + ", Type=" + type;// + " ,Direction=" + direction;
		}
	}

	public static class PoliciesList extends Element {
		@Find(by = FBy.xpath, value = "//div[contains(@class, 'ApicPolicy')]")
		// @Find(by = FBy.xpath, value = ".//div[contains(@class, 'ApicPolicy')]")
		List<WebElement> flowImage;

		private @Getter LinkedHashMap<String, ApicPolicy> userPolicies;
		@Getter
		ApicPolicy frontEndPolicy = null;
		ApicPolicy frontInternal = null;
		@Getter
		ApicPolicy apimsecurityInternal = null;
		ApicPolicy oauthsecurityInternal = null;
		ApicPolicy backInternal = null;
		@Getter
		ApicPolicy backEndPolicy = null;

		public PoliciesList(WebElement base_elem) {
			super(base_elem);
			this.buildPoliciesList();
		}

		public PoliciesList(WebElement base_elem, PoliciesList policies_list) {
			super(base_elem);
			this.buildPoliciesList();
			for (String name : policies_list.userPolicies.keySet()) {
				Assert.assertTrue(userPolicies.get(name).policyElement
						.validate(policies_list.userPolicies.get(name).policyElement));

				Assert.assertTrue(userPolicies.get(name).policyElement
						.validate(policies_list.userPolicies.get(name).policyElement));
			}
			/*
			 * for (String name : policies_list.policies.keySet()) {
			 * 
			 * ApicPolicy new_policy = new ApicPolicy(this.flowImage.get(i++));
			 * System.err.println(new_policy.dataIsEquals(policies_list.policies.get(name)))
			 * ;
			 * Assert.assertTrue(new_policy.dataIsEquals(policies_list.policies.get(name)));
			 * this.policies.put(new_policy.getName(), new_policy);
			 * 
			 * }
			 */

		}

		public void buildPoliciesList(Boolean... is_oauth) {

			userPolicies = new LinkedHashMap<>();
			int i = 0;

			for (i = 1; i < this.flowImage.size() - 1; i++) {
				String policyType = this.flowImage.get(i).getAttribute("class");
				ApicPolicy policy = new ApicPolicy(this.flowImage.get(i));

//				if (policyType.contains("ApicPolicyFrontEndBox"))
//					frontEndPolicy = policy;
				if (policyType.contains("ApicPolicyInternalBox"))
					if (frontInternal == null)
						frontInternal = policy;
					else if (policy.name.equals("apim.security"))
						apimsecurityInternal = policy;
					else
						backInternal = policy;
				else if (is_oauth.length > 0 && is_oauth[0]) {
					Assert.assertTrue(policyType.contains("ApicPolicyUserBox"));
					userPolicies.put(policy.getName(), policy);
					Assert.assertEquals(policy.name, "oauth2-server");
				} else if (policyType.contains("ApicPolicyInternalBox")) {
					apimsecurityInternal = policy;
					Assert.assertEquals(apimsecurityInternal.name, "apim.security");
				} else if (policyType.contains("ApicPolicyUserBox")) {
					userPolicies.put(policy.getName(), policy);
				} else if (policyType.contains("ApicPolicyInternalBox")) {
					if (policy.name.equals("apim.security"))
						apimsecurityInternal = policy;
					else
						backInternal = policy;
				}
			}

//			if (this.flowImage.get(i).getAttribute("class").contains("ApicPolicyFrontEndBox")) {
//				i++;
//			}
//
//			if (this.flowImage.get(i).getAttribute("class").contains("ApicPolicyInternalBox")) {
//				frontInternal = new ApicPolicy(this.flowImage.get(i++));
//			}
//
//			if (is_oauth.length > 0 && is_oauth[0]) {
//				Assert.assertTrue(this.flowImage.get(i).getAttribute("class").contains("ApicPolicyUserBox"));
//				ApicPolicy policy = new ApicPolicy(this.flowImage.get(i++));
//				userPolicies.put(policy.getName(), policy);
//				Assert.assertEquals(policy.name, "oauth2-server");
//			} else if (this.flowImage.get(i).getAttribute("class").contains("ApicPolicyInternalBox")) {
//				apimsecurityInternal = new ApicPolicy(this.flowImage.get(i++));
//				Assert.assertEquals(apimsecurityInternal.name, "apim.security");
//			}
//
//			while (this.flowImage.get(i).getAttribute("class").contains("ApicPolicyUserBox")) {
//
//				ApicPolicy policy = new ApicPolicy(this.flowImage.get(i++));
//				userPolicies.put(policy.getName(), policy);
//			}
//			if (this.flowImage.get(i).getAttribute("class").contains("ApicPolicyInternalBox")) {
//				backInternal = new ApicPolicy(this.flowImage.get(i++));
//			}
//			// Assert.assertTrue(this.flowImage.get(i++).getAttribute("class").contains("ApicPolicyFrontEndBox"));
//			// backEndPolicy = new ApicPolicy(this.flowImage.get(i++));
//			i++;
//			// Assert.assertTrue(this.flowImage.get(i).getAttribute("class").contains("ApicPolicyFrontEndBox"));
//			// backEndPolicy = new ApicPolicy(this.flowImage.get(i));

			i++;
			Assert.assertTrue(i == this.flowImage.size(),
					"expected for " + i + " policies number but found " + this.flowImage.size());

		}

	}

	@Find(by = FBy.className, value = "ApicPolicyDetailsModal", from_root = true)
	// @Find(by = FBy.xpath, value =
	// "//div[@class='ApicPolicyDetailsModal']//div[contains(@class, 'modal') and
	// @role='dialog']//div[contains(@class,
	// 'modal-body')]/*[1]", from_root = true)
	ByPath details_page;

	private @Getter PoliciesList policiesList;
	private @Getter ApicPolicyDetails apicPoliciesDetailsPanel;

	public RequestFlowElement(WebElement elem) {
		super(elem);
		apicPoliciesDetailsPanel = new ApicPolicyDetails(this.findElement(details_page));
		policiesList = new PoliciesList(elem);
	}

	public void clickOnPolicy(String policy_name) {
		policiesList.getUserPolicies().get(policy_name).getPolicyElement().baseElement.click();
		apicPoliciesDetailsPanel.open(policy_name, policiesList);
	}

	// public void getPolicy(String policy_name) {
	// policiesList.getUserPolicies().get(policy_name);
	public HashMap<String, List<TreeMap<String, String>>> getAllSideCalls() {
		this.clickOnPolicy(policiesList.getUserPolicies().keySet().toArray()[0].toString());
		return apicPoliciesDetailsPanel.getAllSideCalls();
	}

	public int getSumPoliciesMS() {
		int sum_ms = 0;
		String ms_text;
		for (String plc_name : policiesList.getUserPolicies().keySet()) {
			ms_text = policiesList.getUserPolicies().get(plc_name).getPolicyElement().getMs();
			sum_ms += TestUtils.StringUtils.parseMS(ms_text);
		}
		sum_ms += policiesList.frontEndPolicy == null ? 0
				: TestUtils.StringUtils.parseMS(policiesList.frontEndPolicy.getPolicyElement().getMs());
		sum_ms += policiesList.apimsecurityInternal == null ? 0
				: TestUtils.StringUtils.parseMS(policiesList.apimsecurityInternal.getPolicyElement().getMs());
		sum_ms += policiesList.oauthsecurityInternal == null ? 0
				: TestUtils.StringUtils.parseMS(policiesList.oauthsecurityInternal.getPolicyElement().getMs());
		sum_ms += TestUtils.StringUtils.parseMS(policiesList.frontInternal.getPolicyElement().getMs());
		sum_ms += TestUtils.StringUtils.parseMS(policiesList.backInternal.getPolicyElement().getMs());
		return sum_ms;
	}
}
