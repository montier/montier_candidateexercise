package org.montier.qa.automation.pages.apic;

import org.montier.qa.automation.pages.base.APICAbstractTransactionPage;
import org.montier.qa.automation.toolkit.elements.TransactionComponents.Payload;

public class APICTransactionPayloadPage extends APICAbstractTransactionPage {
	public APICTransactionPayloadPage() {
		super();
	}

	@Override
	public void initPageElement() {
		super.initPageElement();
		this.transactionComponents.payload = (Payload) this.transactionComponents.getTabContent(Payload.class);
	}

	@Override
	public Payload getComponentPanel() {
		return this.transactionComponents.payload;
	}
}
