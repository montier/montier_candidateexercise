package org.montier.qa.automation.base.testutils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.montier.qa.automation.base.Log;

public class TimeUtils {
	public static Date toDate(String date_str) {
		Date date = null;
		try {
			date = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS").parse(date_str);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	public static Date addMillis(Date date, int num) {
		long new_date = date.getTime() + num;
		return new Date(new_date);
	}

	public static boolean checkTimeInRange(Date start_date, int seconds, Date date) {
		return (start_date.getTime() + seconds * 1000 > date.getTime() && start_date.getTime() - seconds * 1000 < date.getTime());
	}

	public static boolean checkTimeInRange(String start_date, int seconds, String date) {
		Log.info("Check that date value " + date + " is in range of " + seconds + "seconds of " + start_date);
		Date sdate = new Date();
		Date d = new Date();
		try {
			sdate = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS").parse(start_date);
			d = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS").parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (sdate.getTime() + seconds * 1000 > d.getTime() && sdate.getTime() - seconds * 1000 < d.getTime());
	}

}
