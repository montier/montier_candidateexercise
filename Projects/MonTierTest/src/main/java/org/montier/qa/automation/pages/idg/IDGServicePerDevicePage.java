package org.montier.qa.automation.pages.idg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.PagesFactory;
import org.montier.qa.automation.base.Simulator;
import org.montier.qa.automation.base.UsersRoles;
import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.montier.qa.automation.base.utils.Table.TableRow;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.google.common.collect.Sets;

public class IDGServicePerDevicePage extends PageObject {

	public static class DeviceTransCols extends TableCols {
		public static final Col DEVICE = new Col("Device");
		public static final Col SERVICE = new Col("Service");
		public static final Col TRANSACTION_COUNT = new Col("Transactions Count");

		@Override
		public void setColsIndices() {
			this.colsIndices.addAll(Arrays.asList(DEVICE, SERVICE, TRANSACTION_COUNT));
		}
	}

	public class ServicePerDeviceFilters extends PageFilters {
		public FilterUtils TIME = new FilterUtils("Time");
		public FilterUtils DEVICE = new FilterUtils("Device");
		public FilterUtils DOMAIN = new FilterUtils("Domain");
		public FilterUtils SERVICE = new FilterUtils("Service");
		public FilterUtils STATUS = new FilterUtils("Status");
		public FilterUtils CLIENT_IP = new FilterUtils("Client IP");
	}

	@Find(by = FBy.xpath, value = "//table")
	WebElement table;

	public Table deviceTransTable;

	public IDGServicePerDevicePage() {
		super();

	}

	@Override
	public void initPageElement() {
		this.deviceTransTable = new Table(table, new DeviceTransCols());
	}

	@Override
	public ServicePerDeviceFilters getPageFilters() {
		if (this.pageFilters == null)
			this.pageFilters = new ServicePerDeviceFilters();
		return (ServicePerDeviceFilters) this.pageFilters;
	}

	public boolean validateTableContent(String[] service_list) {
		int table_num_rows = this.deviceTransTable.getNumRows();
		String[] allowed_d = UsersRoles.getAllowedResourceFromList("Device", Simulator.devicesList);
		Set<String> allowed_devices = allowed_d != null ? Sets.newHashSet(allowed_d) : new HashSet<String>();
		String[] allowed_s = UsersRoles.getAllowedResourceFromList("Service", service_list);
		Set<String> allowed_services = allowed_s != null ? Sets.newHashSet(allowed_s) : new HashSet<String>();
		Assert.assertTrue(table_num_rows == allowed_devices.size() * allowed_services.size());
		List<String> expected_service_per_device = new ArrayList<String>();
		Iterator<String> dvcs_iter = allowed_devices.iterator();
		while (dvcs_iter.hasNext()) {
			Iterator<String> srvc_iter = allowed_services.iterator();
			while (srvc_iter.hasNext())
				expected_service_per_device.add(dvcs_iter.next() + srvc_iter.next());
		}
		TableRow row = null;
		String service_device_pair;
		for (int i = 0; i < table_num_rows; i++) {
			row = this.deviceTransTable.getRow(i);
			service_device_pair = row.getValue(DeviceTransCols.DEVICE).trim() + row.getValue(DeviceTransCols.SERVICE).trim();
			if (!expected_service_per_device.remove(service_device_pair)) {
				Log.error("Pair " + service_device_pair + " unexpected exists in table");
				return false;
			}
		}
		if (!expected_service_per_device.isEmpty()) {
			Log.error("Not all expected values appears in the table, remains pairs: " + expected_service_per_device);
			return false;
		}
		return true;
	}

	public boolean validateServiceColContent() {
		int table_num_rows = this.deviceTransTable.getNumRows();
		String servicei;
		for (int i = 0; i < table_num_rows; i++) {
			servicei = this.deviceTransTable.getRow(i).getValue(DeviceTransCols.SERVICE).trim();
			Assert.assertTrue(UsersRoles.isResourceAllowedForCurrentUser("Service", servicei),
					"Service " + servicei + " not in allowed services per user");
		}
		return true;
	}

	public String clickOnRow(int row_num) {
		TableRow row = this.deviceTransTable.getRow(row_num);
		String device = row.getValue(DeviceTransCols.DEVICE);
		String service = row.getValue(DeviceTransCols.SERVICE);
		Log.info("Click on row number " + row_num + " with pair of device " + device + " and service " + service);
		PageObject.getFilterElem().addFilterToCurrent(this.getPageFilters().DEVICE.filterName, device);
		PageObject.getFilterElem().addFilterToCurrent(this.getPageFilters().SERVICE.filterName, service);
		row.click(DeviceTransCols.SERVICE);
		PagesFactory.getInstance().idg.trans.openPage();
		return device + "," + service;
	}
}
