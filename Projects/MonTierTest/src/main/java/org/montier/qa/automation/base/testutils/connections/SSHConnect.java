package org.montier.qa.automation.base.testutils.connections;

import java.io.InputStream;
import java.util.Properties;

import org.montier.qa.automation.base.Log;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

// TODO AMIT: Change to SSHJ with Avi 
public class SSHConnect {
	JSch jsch;
	private Session session;
	private Channel channel;
	private ChannelExec channelExec;
	private String user;
	private String host;
	private int port;
	private String password;

	public SSHConnect(String host, int port, String user, String password) {
		jsch = new JSch();
		this.user = user;
		this.host = host;
		this.port = port;
		this.password = password;
	}

	public void getSession() throws JSchException {
		session = null;
		session = jsch.getSession(user, host, port);
		session.setPassword(password);
		Properties config = new Properties();
		config.put("StrictHostKeyChecking", "no");
		config.put("PreferredAuthentications", "password");
		session.setConfig(config);
		Log.debug("Establishing Connection..." + session.getTimeout());
		session.connect();
		Log.debug("Connection established." + session.getTimeout());
	}

	public boolean connect() throws JSchException {
		this.getSession();
		channel = session.openChannel("exec");
		channelExec = (ChannelExec) channel;
		return true;
	}

	public void disconnect() {
		Log.debug("disconnect session and channel");
		session.disconnect();
		channel.disconnect();
		channelExec.disconnect();
		Log.debug("Disconnected");
	}

	public boolean run(String cmd) throws JSchException {
		boolean exit = false;
		this.connect();
		exit = this.runCmd(cmd);
		this.disconnect();
		return exit;
	}

	// TODO AMIT: Change "throws" to try-catch that converts to RuntimeException like in this method below
	private boolean runCmd(String cmd) {
		try {
			Log.info("run cmd => " + cmd);
			channelExec.setCommand(cmd);
			channel.setInputStream(null);
			// InputStream commandOutput = channel.getExtInputStream();

			StringBuilder outputBuffer = new StringBuilder();
			StringBuilder errorBuffer = new StringBuilder();

			// TODO AMIT: Use try
			// try (InputStream in = ...; InputStream err = ...;) {}
			InputStream in = null;
			InputStream err = null;
			String line = "";
			int i;
			byte[] tmp = new byte[1024];
			in = channel.getInputStream();
			err = channel.getExtInputStream();
			channel.connect();

			while (true) {
				while (in.available() > 0) {
					i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					line = new String(tmp, 0, i);
					Log.debug(line);
					outputBuffer.append(new String(tmp, 0, i));
				}
				while (err.available() > 0) {
					i = err.read(tmp, 0, 1024);
					if (i < 0)
						break;
					errorBuffer.append(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					if ((in.available() > 0) || (err.available() > 0))
						continue;
					Log.debug("exit-status: " + channel.getExitStatus());
					break;
				}

				Thread.sleep(1000);
			}

			Log.debug("output: " + outputBuffer.toString());
			if (channel.getExitStatus() == 1 || line.contains("ERROR"))
				return false;
			else
				return true;

		} catch (Exception e) {
			throw e instanceof RuntimeException ? (RuntimeException) e : new RuntimeException(e);
		}
	}
}