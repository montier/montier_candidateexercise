package org.montier.qa.automation.pages.apic;

import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.elements.SVGLineChartElement;
import org.openqa.selenium.WebElement;

public class APICApiLatencyPage extends PageObject {
	public class ApiLatencyFilters extends PageFilters {

		public FilterUtils TIME = new FilterUtils("Time");
		public FilterUtils DEVICE = new FilterUtils("Device");
		public FilterUtils DOMAIN = new FilterUtils("Domain");
		public FilterUtils CATALOG = new FilterUtils("Catalog");
		public FilterUtils SPACE = new FilterUtils("Space");
		public FilterUtils PRODUCT = new FilterUtils("Product");
		public FilterUtils PLAN = new FilterUtils("Plan");
		public FilterUtils API_NAME = new FilterUtils("API Name");
		public FilterUtils API_VERSION = new FilterUtils("API Version");
		public FilterUtils APP_NAME = new FilterUtils("App Name");

	}

	@Find(by = FBy.className, value = "ApicApiAvgLatencyChart")
	WebElement ApicApiAvgLatencyChart;
	@Find(by = FBy.className, value = "ApicApiMaxLatencyChart")
	WebElement ApicApiMaxLatencyChart;
	@Find(by = FBy.className, value = "ApicApiSuccessfulTransactionsChart")
	WebElement ApicApiSuccessfulTransactionsChart;
	// @Find(by = FBy.className, value = "ApicApiErrorTransactionsChart")
	// WebElement ApicApiErrorTransactionsChart;
	@Find(by = FBy.className, value = "ApicApiRequestSizeChart")
	WebElement ApicApiRequestSizeChart;
	@Find(by = FBy.className, value = "ApicApiResponseSizeChart")
	WebElement ApicApiResponseSizeChart;

	public SVGLineChartElement ApiAvgLatencyChart;
	public SVGLineChartElement ApiMaxLatencyChart;
	public SVGLineChartElement ApiSuccessfulChart;
	// public SVGLineChartElement ApiErrorChart;
	public SVGLineChartElement ApiRequestSizeChart;
	public SVGLineChartElement ApiResponseSizeChart;

	public APICApiLatencyPage() {
		super();
	}

	@Override
	public void refresh() {
		super.refresh();
		this.initCharts();
	}

	public void initCharts() {
//		this.ApiAvgLatencyChart = new SVGLineChartElement(ApicApiAvgLatencyChart);
//		this.ApiMaxLatencyChart = new SVGLineChartElement(ApicApiMaxLatencyChart);
//		this.ApiSuccessfulChart = new SVGLineChartElement(ApicApiSuccessfulTransactionsChart);
//		// this.ApiErrorChart = new SVGLineChartElement(ApicApiErrorTransactionsChart);
//		this.ApiRequestSizeChart = new SVGLineChartElement(ApicApiRequestSizeChart);
//		this.ApiResponseSizeChart = new SVGLineChartElement(ApicApiResponseSizeChart);
	}

	@Override
	public ApiLatencyFilters getPageFilters() {
		if (this.pageFilters == null)
			this.pageFilters = new ApiLatencyFilters();
		return (ApiLatencyFilters) this.pageFilters;

	}

	@Override
	public void initPageElement() {
		// TODO Auto-generated method stub

	}

}
