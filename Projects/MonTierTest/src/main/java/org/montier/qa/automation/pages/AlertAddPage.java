package org.montier.qa.automation.pages;

import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;

public class AlertAddPage extends PageObject {
	@Find(by = FBy.xpath, value = "//label[text()='$NAME$']/following-sibling::div//input")
	static ByPath input_textbox;
	@Find(by = FBy.xpath, value = "//button[text()='Details']")
	static ByPath details_button;
	@Find(by = FBy.xpath, value = "//button[contains(@class, 'btn btn-primary') and text()='Add']")
	static ByPath addButton;
	@Find(by = FBy.xpath, value = "//button[text() = 'Details']")
	static ByPath details;
	@Find(by = FBy.xpath, value = "//div[@class='collapse in']//div[@class = 'form-group']//textarea[contains(@placeholder, 'Store Query')]")
	static ByPath query;

	public AlertAddPage() {
		super();
	}

	public String getValueOfTextBox(String textbox_name) {
		return this.findElement(input_textbox, textbox_name).getAttribute("value");
	}

	public boolean isValueContains(String textbox_name, String value) {
		return this.getValueOfTextBox(textbox_name).contains(value);
	}

	public void addAlert() {
		this.findElement(addButton).click();
		Log.info("Clicked on button addAlert");
	}

	@Override
	protected void initValidationValues() {
		this.addValidationField("service", this.getValueOfTextBox("Name"));
	}

	@Override
	public void initPageElement() {
		// empty override
	}

	@Override
	public PageFilters getPageFilters() {
		return null;
	}

	public void clickDetails() {
		this.findElement(details).click();
		Log.info("Clicked on button Details");
	}

	public String getQueryDetails() {
		return this.findElement(query).getAttribute("value");
	}
}
