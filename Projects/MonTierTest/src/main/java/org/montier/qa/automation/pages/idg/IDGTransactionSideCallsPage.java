package org.montier.qa.automation.pages.idg;

import org.montier.qa.automation.pages.base.IDGAbstractTransactionPage;
import org.montier.qa.automation.toolkit.elements.TransactionComponents.SideCalls;

public class IDGTransactionSideCallsPage extends IDGAbstractTransactionPage {

	public IDGTransactionSideCallsPage() {
		super();
	}

	@Override
	public void initPageElement() {
		super.initPageElement();
		this.transactionComponents.sideCalls = (SideCalls) this.transactionComponents.getTabContent(SideCalls.class);
	}

	@Override
	public SideCalls getComponentPanel() {
		return this.transactionComponents.sideCalls;
	}

}
