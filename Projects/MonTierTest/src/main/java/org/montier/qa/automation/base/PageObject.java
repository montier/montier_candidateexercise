package org.montier.qa.automation.base;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.montier.qa.automation.base.testutils.TestUtils.ValidationFields;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.FilterUtils.FilterStatus;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.RunTestException;
import org.montier.qa.automation.elements.FadedMessageElement;
import org.montier.qa.automation.elements.FadedMessageElement.Message;
import org.montier.qa.automation.elements.FiltersPanelElement;
import org.montier.qa.automation.elements.SideMenu;
import org.montier.qa.automation.elements.TopPanel;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.common.collect.Sets;

public abstract class PageObject extends SelObject {
	@Find(by = FBy.className, value = "LoadingImagePanel")
	ByPath loading_image;
	@Find(by = FBy.xpath, value = "//div[contains(@class,'FiltersPanel')]")
	ByPath filter_panel;
	@Find(by = FBy.xpath, value = "//head/title")
	ByPath page_title;
	@Find(by = FBy.xpath, value = "//div[contains(@class, 'error-code') and contains(@jscontent, 'errorCode')]")
	ByPath http_error_code;
	@Find(by = FBy.xpath, value = "//*[@role='dialog' and contains(@class,'in') and contains(@style, 'display: block')]//button[@class='close']")
	ByPath fade_dialog_close;

	private static String pageUrl;
	public FadedMessageElement fadedMessages;
	public ValidationFields pageValidationValues;
	public TopPanel topPanel;
	public static SideMenu sideMenu;
	private static FiltersPanelElement filterElem = null;
	protected PageFilters pageFilters = null;
	public String[] args;
	public PageObject parentPage;
	private static String last_url = null;
	private static String last_page_class_name = "";

	public PageObject() {
		super();
		this.pageValidationValues = new ValidationFields();
		parentPage = null;
	}

	public void openPage(String... args) {
		Log.debug("open page " + this.getClass().getSimpleName());
		this.args = args;
		PagesFactory.getInstance().currentPage = this;
		// init pathes shared for all pages
		this.initGeneralPathes();
		// wait for page loading
		this.loadPageWait();
		// check there is no HTTP errors
		this.checkHTTPErrors();
		// init top panel that commons all pages
		topPanel = TopPanel.getInstance();
		// wait for elements loading by waiting to loading image ignore from the top of the page
		this.loadElementsWait();
		// check if faded message come when the page is loading
		this.readFadedMessages();
		// init webElements and pathes of page with annotation "Find"
		this.initWebElements();
		// init elements of the specific page (override function)
		this.initPageElement();
		// init filters
		this.initPageFilters();
		driver.scrollPageStart();
	}

	public void openTransitionPage() {
		/**
		 * Transition Page is the page automaticly opened when we moved between section in menu page. For example, if we are under Manage section and
		 * we want open Dashboards->Analytics->ServiceLatency page, after the 1st click on Dashboards, SystemOverviewPage loaded and may change the
		 * filters content. That page called TranstionPage.
		 */
		PagesFactory.getInstance().currentPage = this;
		// init pathes shared for all pages
		this.initGeneralPathes();
		// init filters
		this.initPageFilters();
	}

	public void checkHTTPErrors() {
		Log.info("check HTTP errors, driver title is " + Driver.getInstance().getDriver().getTitle());
		if (!Driver.getInstance().getDriver().getTitle().contains("IBM DataPower Operations Dashboard")) {
			Log.error("page has no DPOD title, it may point on http error");
			List<WebElement> elem = this.findElementIfExists(http_error_code);
			if (elem.size() != 0)
				Assert.fail("HTTP error code is " + elem.get(0).getText());
			throw new RunTestException();
		}
	}

	public static abstract class PageFilters {
		public PageFilters() {
		}

		public FilterStatus getfilterStatus(FilterUtils filter) {
			return filter.filterStatus;
		}

		public boolean allAreUrlFilters() {
			return false;
		}

		public List<String> getFilters(boolean url_filters) {
			ArrayList<String> filters_names = new ArrayList<String>();
			String pagefil_name = null;
			Field[] pagefil = this.getClass().getDeclaredFields();
			for (Field fil : pagefil)
				if (fil.getType().getSimpleName().equals(FilterUtils.class.getSimpleName()))
					try {
						if (!filterElem.isShowMoreOpen() && this.getfilterStatus((FilterUtils) fil.get(this)) == FilterStatus.LESS)
							continue;
						if (!url_filters && this.getfilterStatus((FilterUtils) fil.get(this)) == FilterStatus.URL)
							continue;
						pagefil_name = fil.get(this).toString();
						filters_names.add(pagefil_name);
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}
			return filters_names;
		}
	}

	protected void checkFilters() {
		filterElem.ensureAllFiltersExists(this.getPageFilters());
		filterElem.updateCurrentFilters(this.getPageFilters());
	}

	public abstract PageFilters getPageFilters();

	private void initPageFilters() {
		this.pageFilters = this.getPageFilters();
		if (this.pageFilters == null)
			return;
		if (!this.getPageFilters().allAreUrlFilters())
			if (filterElem == null)
				filterElem = new FiltersPanelElement();
			else {
				this.checkFilters();
				filterElem.initFilters(true);
			}

		this.checkFilters();
	}

	public boolean validateFilterOptionsForUser() {
		Set<String> user_allowed_resources_name = UsersRoles.getCurrentUserAllowedResources().keySet();
		List<String> page_filter_names = this.getPageFilters().getFilters(false);
		Set<String> inter_ans = Sets.intersection(user_allowed_resources_name, Sets.newHashSet(page_filter_names));
		List<String> filter_options;
		for (String resource : inter_ans) {
			filter_options = filterElem.findFilterOptionsText(resource);
			for (String opt : filter_options)
				if (!UsersRoles.isResourceAllowedForCurrentUser(resource, opt)) {
					Log.error("Option " + opt + " of filter " + resource + " is not allowed for user");
					return false;
				} else
					Log.info("Option " + opt + " of filter " + resource + " is allowed for user");
		}
		return true;
	}

	public static FiltersPanelElement getFilterElem() {
		if (filterElem == null)
			filterElem = new FiltersPanelElement();
		return filterElem;
	}

	protected abstract void initPageElement();

	@Override
	protected SearchContext getSearchBy() {
		return this.driver.getDriver();
	}

	protected void loadPageWait() {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {

			@Override
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver.getDriver(), 20);
		wait.until(pageLoadCondition);
		try {
			if (last_url != null)
				if (!last_page_class_name.equals(this.getClass().getSimpleName()))
					wait.until(ExpectedConditions.not(ExpectedConditions.urlContains(last_url)));
		} catch (Exception e) {
			Log.warn("url dosn't changes if its not reload existing page it may fail in checkUrl function");
		}

		this.checkUrl();
		last_url = driver.getDriver().getCurrentUrl();
		last_page_class_name = this.getClass().getSimpleName();
	}

	protected void loadElementsWait() {
		boolean wait = true;
		try {
			if (this.findElementIfExists(this.loading_image).size() > 0) {
				Log.debug("Wait for elements loaded by waiting to loading image in top panel to be hidden");
				wait = new WebDriverWait(driver.getDriver(), 60)
						.until(ExpectedConditions.attributeToBe(By.className("LoadingImagePanel"), "style", "visibility: hidden;"));
			}
		} catch (NoSuchElementException e) {
			Log.debug("No such element like Loading image");
		}
		if (!wait)
			throw new RunTestException("Timeout waiting elements to load", new Throwable());
	}

	private void readFadedMessages() {
		this.fadedMessages = FadedMessageElement.getInstance();
		List<Message> msgs = this.fadedMessages.getAllMessages();
		for (Message msg : msgs)
			if (msg.isError())
				Log.error("Error faded message: " + msg.toString());
	}

	private void checkUrl() {
		String page_name = this.getClass().getName();
		if (page_name.contains("SignInPage"))
			return;
		String class_name = page_name.substring(page_name.lastIndexOf('.') + 1).toLowerCase().replace("page", "");
		String url = driver.getDriver().getCurrentUrl();
		Log.info("Current URL > " + url);

		String url_anchor = null;
		try {
			url_anchor = url.substring(url.indexOf('#') + 1, url.indexOf(":-")).toLowerCase();
		} catch (StringIndexOutOfBoundsException e) {
			Log.error(
					"assuming that page name appears in url after between # and :-, check the correction of the url or maybe page has special structure. page name "
							+ page_name + " url " + url);
			throw new RunTestException();
		}
		Assert.assertEquals(class_name, url_anchor, "Instead of page " + page_name + " found url " + url);
	}

	protected void initValidationValues() {
		//
	}

	public void addValidationField(String key, String value) {
		this.pageValidationValues.add(key, value);
	}

	public void refresh() {
		openPage();
		// this.initWebElements();
		// this.initPageElement();
	}

	protected void initGeneralPathes() {
		this.initWebElements(PageObject.class);
	}

	public PageObject switchToParentWindow() {
		if (this.parentPage == null)
			return this;
		driver.switchToParentHandler();
		PageObject parent_page = this.parentPage;
		PagesFactory.getInstance().currentPage = this.parentPage;
		this.parentPage = null;
		Log.debug("switched to parent page " + parent_page);
		return parent_page;
	}

	public PageObject switchToSubWindow(PageObject sub_window) {
		sub_window.parentPage = this;
		driver.switchToSubWindow();
		return this;
	}

	public void closeDialogModal() {
		List<WebElement> close_button = this.findElementIfExists(this.fade_dialog_close);
		if (close_button.size() == 0)
			return;
		Log.debug("Found opened dialog, closing it");
		close_button.get(0).click();
		Assert.assertTrue(this.findElementIfExists(this.fade_dialog_close).size() == 0, "unexpected open dialog found");
	}
}
