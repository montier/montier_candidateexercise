package org.montier.qa.automation.toolkit.actions;

import java.util.concurrent.TimeUnit;

import org.montier.qa.automation.base.Driver;
import org.montier.qa.automation.base.FunctionalTest;
import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.PagesFactory;
import org.montier.qa.automation.base.Simulator;
import org.montier.qa.automation.base.UsersRoles;
import org.montier.qa.automation.elements.TopPanel;
import org.montier.qa.automation.pages.EulaPage;
import org.montier.qa.automation.toolkit.elements.WhatsNewModal;

public class Login {
	public static void login(Boolean... first_time) {
		Log.debug("Login to user appears in configuration page");
		String user_name = FunctionalTest.getSimulator().getConf("uiuser");
		String password = FunctionalTest.getSimulator().getConf("uipassword");
		String host = "https://" + FunctionalTest.getSimulator().getConf("host") + ":" + FunctionalTest.getSimulator().getConf("uiport");
		Log.info("Open URL " + host);
		FunctionalTest.getDriver().openUrl(host);
		loginAction(user_name, password, first_time);
	}

	public static void switchUserToAdmin() {
		Log.debug("Switch user to admin");
		switchUser(FunctionalTest.getSimulator().getConf("uiuser"), FunctionalTest.getSimulator().getConf("uipassword"));
	}

	public static void switchUser(String user_name, String password) {
		if (UsersRoles.getCurrentUserName().equals(user_name)) {
			Log.info("User already loggedin");
			return;
		}
		Log.instep("Sign out");
		TopPanel.getInstance().signOut();
		PageObject.getFilterElem().resetFilters(true, false);
		loginAction(user_name, password);
		WhatsNewModal whatNewModal = new WhatsNewModal(false);
		whatNewModal.checkAndClick(false);
	}

	private static void loginAction(String user_name, String password, Boolean... first_time) {
		Log.instep("Signing in to host as user " + user_name);
		PagesFactory.getPages().signIn.openPage();
		String current_url = Driver.getInstance().getDriver().getCurrentUrl();
		PagesFactory.getPages().signIn.logIn(user_name, password);
		UsersRoles.setCurrentUserName(user_name);
		// WebDriverWait wait = new WebDriverWait(Driver.getInstance().getDriver(), );
		// wait.until(ExpectedConditions.not(ExpectedConditions.url(current_url)));
		try {
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if (Driver.getInstance().getDriver().getCurrentUrl().contains("eula")) {
			// if (first_time != null && first_time.length > 0 && first_time[0] != null && first_time[0] == true)
			firstLogin(PagesFactory.getPages().eulaPage);
		}
	}

	public static void firstLogin(EulaPage eula_page) {
		Log.debug("First Login after build");
		eula_page.openPage();
		Log.debug("Close on Whats new message");
		eula_page.whatsNewCheckAndClick();
		Log.debug("Agree to licensee");
		eula_page.agreeLicense();
		PagesFactory.getPages().monitorDevicesPage.openPage();
		Log.debug("Check num of devices suitable to conf page");
		Simulator.getInstance().getDevices();
		PagesFactory.getPages().getProductDefaultPage("Dashboards", true, false);
	}
}
