package org.montier.qa.automation.toolkit.elements;

import org.montier.qa.automation.base.Element;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

public class WhatsNewModal extends Element {
	@Find(by = FBy.xpath, value = "//div[@class='WhatsNewModal']")
	ByPath modal;
	@Find(by = FBy.xpath, value = "//input[@type='checkbox']")
	ByPath wnCheckbox;
	@Find(by = FBy.xpath, value = "//div[@class='modal-footer']//button")
	ByPath wncloseButton;
	@Find(by = FBy.xpath, value = "//div[contains(@class, 'modal fade')]")
	ByPath modal_fade;
	boolean inform;

	public WhatsNewModal(boolean inform) {
		super();
		this.inform = inform;
		this.baseElement = this.findElement(modal);
	}

	public void checkAndClick(boolean force_exists) {
		boolean is_modal_exists = this.isModalDisplayed();
		if (force_exists && !is_modal_exists)
			Assert.fail("WhatsNew modal unexists as expected");
		if (!is_modal_exists)
			return;
		WebElement checkbox = this.findElement(wnCheckbox);
		WebElement closeButton = this.findElement(wncloseButton);

		Actions actions = new Actions(driver.getDriver());

		actions.moveToElement(checkbox).click().build().perform();
		actions.moveToElement(closeButton).click().build().perform();
	}

	public void close() {
		if (this.inform)
			Assert.assertTrue(this.findElementIfExists(wnCheckbox).size() == 0);
		this.findElement(wncloseButton).click();
	}

	public boolean isModalDisplayed() {
		return this.findElement(modal_fade).getAttribute("style").contains("display: block;");
	}
}
