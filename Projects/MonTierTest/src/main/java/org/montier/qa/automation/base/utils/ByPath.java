package org.montier.qa.automation.base.utils;

public class ByPath {
	public String byMethodName;
	public String path;
	public boolean from_root;

	public ByPath(String by, String path, boolean from_root) {
		this.byMethodName = by;
		this.path = path;
		this.from_root = from_root;
	}

	public ByPath(String by_path) {
		String[] pair = by_path.trim().split(",", 2);
		this.byMethodName = pair[0];
		this.path = pair[1];
	}

	public ByPath(ByPath by_path) {
		this.byMethodName = by_path.byMethodName;
		this.path = by_path.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public String toString() {
		return "By." + this.byMethodName + "(\"" + this.path + "\")";
	}

}
