package org.montier.qa.automation.pages.idg;

import java.util.Arrays;

import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.openqa.selenium.WebElement;

public class IDGCertificatesExpirationPage extends PageObject {

	public class CertificatesFilters extends PageFilters {
		public FilterUtils TIME = new FilterUtils("Time");
		public FilterUtils DEVICE = new FilterUtils("Device");
		public FilterUtils DOMAIN = new FilterUtils("Domain");

	}

	public static class ExpCertifCols extends TableCols {
		public static final Col DEVICE = new Col("Device");
		public static final Col TIME = new Col("Time");
		public static final Col DETAILS = new Col("Details");

		public ExpCertifCols() {
			super();
		}

		@Override
		public void setColsIndices() {
			colsIndices.addAll(Arrays.asList(DEVICE, TIME, DETAILS));
		}
	}

	@Find(by = FBy.className, value = "ExpiredCertificatesGrid")
	WebElement expriedGridTable;

	@Find(by = FBy.className, value = "AboutToExpireCertificatesGrid")
	WebElement aboutToExpireTeble;

	public Table expriedTable;
	public Table aboutToExpireTable;

	public IDGCertificatesExpirationPage() {
		super();

	}

	@Override
	public void initPageElement() {
		this.expriedTable = new Table(expriedGridTable, new ExpCertifCols());
		this.aboutToExpireTable = new Table(aboutToExpireTeble, new ExpCertifCols());

	}

	@Override
	public CertificatesFilters getPageFilters() {
		if (this.pageFilters == null)
			this.pageFilters = new CertificatesFilters();
		return (CertificatesFilters) this.pageFilters;
	}
}
