package org.montier.qa.automation.pages.base;

import java.util.ArrayList;

import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.UsersRoles;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.elements.TabsPanelElement;
import org.montier.qa.automation.elements.TabsPanelElement.Tab;
import org.montier.qa.automation.pages.RoleViewPage.Permissions;
import org.montier.qa.automation.pages.apic.APICEarlyFailedPage;
import org.montier.qa.automation.pages.apic.APICTransactionsPage;
import org.openqa.selenium.WebElement;

public abstract class APICAbstractInvestigatePage extends PageObject {

	// @Find(by = FBy.className, value = "InvestiagteMenu")
	@Find(by = FBy.xpath, value = "//div[contains(@class, 'InvestiagteMenu')]")
	WebElement investiagteMenu;

	public TabsPanelElement tabsElem;

	public APICAbstractInvestigatePage() {
		super();
	}

	@Override
	public void initPageElement() {
		this.tabsElem = new TabsPanelElement(investiagteMenu, getTabs());
	}

	private Tab[] getTabs() {
		ArrayList<Tab> tabs = new ArrayList<Tab>();
		tabs.add(new Tab("Transactions", APICTransactionsPage.class));
		tabs.add(new Tab("Early Failing", APICEarlyFailedPage.class));
		tabs.add(new Tab("Gateway Early Failing", PageObject.class));
		tabs.add(new Tab("Payload Capture", PageObject.class, !UsersRoles.getPermissionOfCurrentUser(Permissions.managePayloadCapture)));
		tabs.add(new Tab("Policy Variables Capture", PageObject.class));
		// tabs.add(new Tab("Policy Variables Capture", PageObject.class),
		// !UsersRoles.getPermissionOfCurrentUser(Permissions.allowViewingAPICPolicyVariables))); By MNTR-1920

		Tab[] arr = new Tab[tabs.size()];
		return tabs.toArray(arr);

	}

}
