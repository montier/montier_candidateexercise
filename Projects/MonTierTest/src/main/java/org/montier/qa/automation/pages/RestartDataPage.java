package org.montier.qa.automation.pages;

import java.util.Arrays;

import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.montier.qa.automation.base.utils.Table.TableRow;
import org.montier.qa.automation.elements.SVGLineChartElement;
import org.montier.qa.automation.elements.SVGTimeLineChartElement;
import org.openqa.selenium.WebElement;

public class RestartDataPage extends PageObject {
	public class restartsFilters extends PageFilters {
		public FilterUtils DOWNTIME_STARTED = new FilterUtils("Downtime Started");
		public FilterUtils DEVICE = new FilterUtils("Device");
		public FilterUtils DOMAIN = new FilterUtils("Domain");
		public FilterUtils CAUSE = new FilterUtils("Cause");
	}

	public static class RestartsCols extends TableCols {
		public static final Col DEVICE = new Col("Device");
		public static final Col DOMAIN = new Col("Domain");
		public static final Col DOWN_TIME = new Col("Down Time");
		public static final Col UP_TIME = new Col("Up Time");
		public static final Col EST_DURATION = new Col("Est. Duration");
		public static final Col CAUSE = new Col("Cause");
		public static final Col TYPE = new Col("Type");
		public static final Col REASON = new Col("Reason");
		public static final Col FIRMWARE = new Col("Firmware");

		public RestartsCols() {
			super();
		}

		@Override
		public void setColsIndices() {
			colsIndices.addAll(Arrays.asList(DEVICE, DOMAIN, DOWN_TIME, UP_TIME, EST_DURATION, CAUSE, TYPE, REASON, FIRMWARE));
		}
	}

	public static class SampleErrorsCols extends TableCols {
		public static final Col DEVICE = new Col("Device");
		public static final Col LAST_SAMPLE_TIME = new Col("Last Sample Time");
		public static final Col LAST_ANALYZE_TIME = new Col("Last Analyze Time");
		public static final Col ERRORS = new Col("Errors");

		public SampleErrorsCols() {
			super();
		}

		@Override
		public void setColsIndices() {
			colsIndices.addAll(Arrays.asList(DEVICE, LAST_SAMPLE_TIME, LAST_ANALYZE_TIME, ERRORS));
		}
	}

	@Find(by = FBy.className, value = "RestartSampleErrorsGrid", init = true)
	WebElement RestartSampleErrorsGrid;
	@Find(by = FBy.className, value = "RestartDetailsGrid", init = true)
	WebElement RestartDetailsGrid;
	@Find(by = FBy.className, value = "RestartTimelineChart", init = true)
	WebElement RestartTimelineChart;
	@Find(by = FBy.className, value = "DeviceResourcesMemoryChart", init = true)
	WebElement DeviceResourcesMemoryChart;
	@Find(by = FBy.xpath, value = "//div[@class='popover fade left in']//div[@class='popover-content']")
	ByPath PopOverFade;

	public SVGTimeLineChartElement TimelineChart;
	public SVGLineChartElement DeviceResourcesChart;
	public Table RestartsTable;
	public Table SampleErrorsGridTable;

	public RestartDataPage() {
		super();
	}

	@Override
	public restartsFilters getPageFilters() {
		if (this.pageFilters == null)
			this.pageFilters = new restartsFilters();
		return (restartsFilters) this.pageFilters;

	}

	@Override
	public void initPageElement() {
		this.TimelineChart = new SVGTimeLineChartElement(RestartTimelineChart);
		this.DeviceResourcesChart = new SVGLineChartElement(DeviceResourcesMemoryChart);
		this.RestartsTable = new Table(RestartDetailsGrid, new RestartsCols());
		this.SampleErrorsGridTable = new Table(RestartSampleErrorsGrid, new SampleErrorsCols());
	}

	public String mouseHoverGetText(TableRow row, Col col) {
		this.driver.moveToElement(row.getCeil(col), 1, 1);
		String elem = findElement(PopOverFade).getText();
		return elem;
	}

}
