package org.montier.qa.automation.base;

import org.montier.qa.automation.pages.AlertAddPage;
import org.montier.qa.automation.pages.AlertViewPage;
import org.montier.qa.automation.pages.AlertsSetupPage;
import org.montier.qa.automation.pages.EulaPage;
import org.montier.qa.automation.pages.MonitoredDevicesPage;
import org.montier.qa.automation.pages.ReportsPage;
import org.montier.qa.automation.pages.RoleViewPage;
import org.montier.qa.automation.pages.SignInPage;
import org.montier.qa.automation.pages.UserViewPage;
import org.montier.qa.automation.pages.UsersPage;

import lombok.Getter;

public abstract class PagesContainer extends Pages {
	@Getter
	public SignInPage signIn;
	@Getter
	public AlertsSetupPage alertSetup;
	@Getter
	public AlertViewPage alertViewPage;
	@Getter
	public MonitoredDevicesPage gatewaysPage;
	@Getter
	public UsersPage usersPage;
	@Getter
	public UserViewPage userViewPage;
	@Getter
	public RoleViewPage roleViewPage;
	@Getter
	public AlertAddPage alertAddPage;
	@Getter
	public ReportsPage reportsPage;
	@Getter
	public MonitoredDevicesPage monitorDevicesPage;
	@Getter
	public EulaPage eulaPage;

	public abstract PageObject getTransRawMessagesPage();

	public abstract PageObject getTransPayload();

	public abstract PageObject getSideCallsPage();

	public abstract PageObject getExtendLatePage();

	public abstract PageObject getEarlyFailedPage();

	public abstract PageObject getTransPage();
}
