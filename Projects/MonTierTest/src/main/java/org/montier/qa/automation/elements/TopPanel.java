package org.montier.qa.automation.elements;

import org.montier.qa.automation.base.Element;
import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.PagesFactory;
import org.montier.qa.automation.base.UsersRoles;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.RunTestException;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import lombok.Getter;

public class TopPanel extends Element {

	public static enum ProductType {
		IDG("#home:-productView:idg"), APIC("#home:-productView:apic");

		private final @Getter String stringValue;

		ProductType(String href) {
			stringValue = href;
		}

		@Override
		public String toString() {
			return this.stringValue;
		}
	}

	@Find(by = FBy.xpath, value = "//a[@href='$HREF$']")
	ByPath productHref;
	@Find(by = FBy.className, value = "LoadingImagePanel")
	ByPath loading_image;
	@Find(by = FBy.xpath, value = "//div[contains(@class,'TopNavbarPanel')]//span[text()='Gateway'  or text()='API-C']/parent::a")
	ByPath productsMenu;
	@Find(by = FBy.className, value = "ReloadOptionsSelect")
	ByPath reloadOptions;
	@Find(by = FBy.className, value = "LD2IVJC-a-Ch")
	ByPath reload_page;
	@Find(by = FBy.xpath, value = "") // ADD CLASS
	ByPath login_status;
	@Find(by = FBy.xpath, value = "//span[text()='$USERNAME$']/parent::a") // ADD CLASS
	ByPath user_text;
	@Find(by = FBy.linkText, value = "Sign Out")
	ByPath sign_out;

	private static TopPanel INSTANCE = null;

	public TopPanel() {
		super();
	}

	public static TopPanel getInstance() {
		if (INSTANCE == null)
			INSTANCE = new TopPanel();
		return INSTANCE;
	}

	public void selectProduct(ProductType product_type) {
		if (PagesFactory.getInstance().getProduct().equals(product_type)) {
			Log.debug("Current product is already " + product_type);
			return;
		}
		PageObject.getFilterElem().resetFilters(true, false);
		this.findElement(productsMenu).click();
		this.findElement(productHref, product_type.toString()).click();
		PagesFactory.changeProduct(product_type);
	}

	public void loadElementsWait() {
		boolean wait = true;
		try {
			if (this.findElementIfExists(this.loading_image).size() > 0) {
				wait = new WebDriverWait(driver.getDriver(), 20)
						.until(ExpectedConditions.attributeToBe(By.className("LoadingImagePanel"), "style", "visibility: hidden;"));
			}
		} catch (NoSuchElementException e) {
			Log.debug("No such element like Loading image");
		}
		if (!wait)
			throw new RunTestException("Timeout waiting elements to load", new Throwable());
	}

	public void signOut() {
		this.findElement(user_text, UsersRoles.getCurrentUserName()).click();
		this.findElement(sign_out).click();
		PagesFactory.getPages().signIn.openPage();
	}

	public void reloadPage() {
		this.findElement(reload_page).click();
	}
}
