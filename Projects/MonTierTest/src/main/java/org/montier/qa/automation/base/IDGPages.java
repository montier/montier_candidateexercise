package org.montier.qa.automation.base;

import org.montier.qa.automation.elements.TopPanel.ProductType;
import org.montier.qa.automation.pages.RestartDataPage;
import org.montier.qa.automation.pages.idg.IDGCertificatesExpirationPage;
import org.montier.qa.automation.pages.idg.IDGEarlyFailedPage;
import org.montier.qa.automation.pages.idg.IDGFailedObjectsPage;
import org.montier.qa.automation.pages.idg.IDGRecentActivityPage;
import org.montier.qa.automation.pages.idg.IDGServiceActivityPage;
import org.montier.qa.automation.pages.idg.IDGServiceConfigurationPage;
import org.montier.qa.automation.pages.idg.IDGServiceLatencyPage;
import org.montier.qa.automation.pages.idg.IDGServicePerDevicePage;
import org.montier.qa.automation.pages.idg.IDGServiceURICalls;
import org.montier.qa.automation.pages.idg.IDGServicesConfiguration;
import org.montier.qa.automation.pages.idg.IDGSystemOverviewPage;
import org.montier.qa.automation.pages.idg.IDGTransactionExtendedLatency;
import org.montier.qa.automation.pages.idg.IDGTransactionPayloadPage;
import org.montier.qa.automation.pages.idg.IDGTransactionRawMessagesPage;
import org.montier.qa.automation.pages.idg.IDGTransactionSideCallsPage;
import org.montier.qa.automation.pages.idg.IDGTransactionsPage;

public class IDGPages extends PagesContainer {
	public IDGServiceActivityPage srvcActivityPage;
	public IDGFailedObjectsPage failObjectPage;
	public IDGCertificatesExpirationPage expiredCertifPage;
	public IDGTransactionRawMessagesPage transRawMessages;
	public IDGTransactionPayloadPage transPayload;
	public IDGTransactionSideCallsPage sideCallsPage;
	public IDGTransactionExtendedLatency extendLatePage;
	public IDGEarlyFailedPage earlyFailedPage;
	public IDGTransactionsPage trans;
	public IDGSystemOverviewPage sysOverview;
	public IDGServiceConfigurationPage confDetails;
	public IDGServiceURICalls srvcURICalls;
	public IDGServiceLatencyPage srvcLatency;
	public IDGServicesConfiguration srvcsConf;
	public RestartDataPage restartData;
	public IDGRecentActivityPage recentActivity;
	public IDGServicePerDevicePage srvcPerDevice;

	public IDGPages() {
		super();
	}

	@Override
	protected PageObject getProductDefaultPage(ProductType product_type) {
		return getProductDefaultPage("Dashboards", false, false);
	}

	@Override
	public IDGTransactionRawMessagesPage getTransRawMessagesPage() {
		return this.transRawMessages;
	}

	@Override
	public IDGTransactionPayloadPage getTransPayload() {
		return this.transPayload;
	}

	@Override
	public IDGTransactionSideCallsPage getSideCallsPage() {
		return this.sideCallsPage;
	}

	@Override
	public IDGTransactionExtendedLatency getExtendLatePage() {
		return this.extendLatePage;
	}

	@Override
	public IDGEarlyFailedPage getEarlyFailedPage() {
		return this.earlyFailedPage;
	}

	@Override
	public IDGTransactionsPage getTransPage() {
		return this.trans;
	}

	@Override
	protected PageObject implementProductDefaultPage(String menu_name) {
		switch (menu_name) {
		case "Dashboards":
			if (UsersRoles.getCurrentUserRole() != null && UsersRoles.getCurrentUserRole().getUserGroup().equals("OpDashInvestigatorsGroup"))
				return this.recentActivity;
			return this.sysOverview;
		case "Manage":
			return this.gatewaysPage;
		case "Reports":
			return this.reportsPage;
		}
		return null;
	}
}