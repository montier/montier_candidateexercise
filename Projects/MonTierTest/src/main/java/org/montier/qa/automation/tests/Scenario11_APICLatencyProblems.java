package org.montier.qa.automation.tests;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeMap;

import org.montier.qa.automation.base.FunctionalTest;
import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.utils.Table.TableRow;
import org.montier.qa.automation.elements.SideMenu;
import org.montier.qa.automation.elements.TopPanel;
import org.montier.qa.automation.elements.TopPanel.ProductType;
import org.montier.qa.automation.pages.apic.APICApiPoliciesPage.PoliciesCols;
import org.montier.qa.automation.pages.apic.APICTransactionsPage.APICTransCols;
import org.montier.qa.automation.pages.base.APICAbstractTransactionPage.APICTransRawVal;
import org.montier.qa.automation.toolkit.elements.RequestFlowElement.ApicPolicy;
import org.montier.qa.automation.toolkit.elements.TransactionsTable.TransCols;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.internal.collections.Pair;

public class Scenario11_APICLatencyProblems extends FunctionalTest {

	TreeMap<APICTransRawVal, String> value = new TreeMap<>();
	ArrayList<ApicPolicy> policiesData = new ArrayList<>();
	LinkedHashMap<String, ApicPolicy> trans_policies_data = new LinkedHashMap<>();

	@BeforeClass
	public void enterAPIC() {
		this.step("Select API-C product", () -> {
			TopPanel.getInstance().selectProduct(ProductType.APIC);

		});

	}

	@BeforeMethod
	public void openTransactionsAndClean() {
		defineAccountsTransactionPolicies();
		PageObject.getFilterElem().resetFilters(true, false);

	}

	private void defineAccountsTransactionPolicies() {
		String[] names = { "getBalance: input", "GetCurrency", "invoke-GetBalance", "getBalance: output",
				"GetcustomerBalance", "json-to-xml", "invoke-GetExchangeRate", "map-CurrencyRateResponse",
				"GetExchangeRate", "ConvertBalance: output", "GetConvertedBalance" };
		String[] types = { "map", "apim.setvariable", "invoke", "map", "apim.setvariable", "json-to-xml", "invoke",
				"map", "apim.setvariable", "map", "apim.setvariable" };

		for (int i = 0; i < types.length; i++) {
			trans_policies_data.put(names[i], new ApicPolicy(names[i], types[i], "REQUEST"));
		}

		trans_policies_data.get("invoke-GetBalance").addSideCall("HTTP", "OK", null,
				"HTTP response code 200 for 'https://172.17.100.70/montierproviderorg508/bankcatalog/accounts-balance-102/balance?customer_id=7777777'");
		trans_policies_data.get("invoke-GetExchangeRate").addSideCall("HTTP", "OK", null,
				"HTTP response code 200 for 'http://172.17.100.70:2050/services/CurrencyRate-USD'");

		for (int i = 1; i < names.length; i++) {
			trans_policies_data.get(names[i]).addContextVariable("DesiredCurrency", "EUR");
		}
		for (int i = 4; i < names.length; i++) {
			trans_policies_data.get(names[i]).addContextVariable("CustomerBalanceToConvert", "20000");
		}
		for (int i = 7; i < names.length; i++) {
			trans_policies_data.get(names[i]).addContextVariable("ExchangeRate", "0.2");
		}
		for (int i = 9; i < names.length; i++) {
			trans_policies_data.get(names[i]).addContextVariable("ConvertedBalance", "5000");
		}

	}

	Pair<Date, Float> checkpoint = null;
	float point;
	String globtransID;
	String transID;
	TableRow l_latency;
	TableRow h_latency;
	int latencyHigh = 0;
	int latencyLow = 0;
	String elapsed;
	String details_sidecalls = null;
	String details_context_variable = null;
	List<TreeMap<String, String>> side_calls = null;
	List<TreeMap<String, String>> context_variable = null;

	@Test // (priority = 0)
	public void SlowlatenciesCase_1() {

		value.put(APICTransRawVal.IN_URL,
				"https://172.17.100.70:443/montierproviderorg508/bankcatalog/balance-convert-102/convert");
		value.put(APICTransRawVal.API_NAME, "balance-convert");
		value.put(APICTransRawVal.STATUS, "OK");
		value.put(APICTransRawVal.PROVIDER_ORG, "montierproviderorg508");
		value.put(APICTransRawVal.API_VERSION, "1.0.2");
		value.put(APICTransRawVal.CATALOG_NAME, "bankcatalog");
		value.put(APICTransRawVal.CLIENT_IP, "172.17.100.130");
		value.put(APICTransRawVal.PRODUCT_ID, "digitalbankingproduct102");
		value.put(APICTransRawVal.PRODUCT_NAME, "digitalbankingproduct102");
		value.put(APICTransRawVal.PLAN_NAME, "default");
		value.put(APICTransRawVal.HTTP_METHOD, "POST");
		value.put(APICTransRawVal.CONSUMER_ORG_ID, "5c20c011e4b0ad0671d5571d");
		value.put(APICTransRawVal.CONSUMER_ORG, "montierproviderorg508");
		value.put(APICTransRawVal.CONSUMER_APP, "digitalbankingapp102");
		value.put(APICTransRawVal.CLIENT_ID, "ecf6c45e-ded1-40fa-bc19-753a859aa631");
		value.put(APICTransRawVal.FE_RESPONSE_CODE, "200");

		String APP_NAME = "digitalbankingapp102";

		int latency = 10000;
		this.step("Filter API latency page to find digitalbankingapp102 behavior", () -> {

			Log.step("Go to Dashboard->analytics→API Latency");
			SideMenu.MenuSelect.Dashboards_Analytics_API_Latency.getPage();

			Log.step(
					"Filter API Name by-" + value.get(APICTransRawVal.API_NAME) + "and filter App Name by " + APP_NAME);
			pages.apic.apiLatencyPage.getPageFilters().API_NAME.applyFilter(value.get(APICTransRawVal.API_NAME));
			pages.apic.apiLatencyPage.getPageFilters().APP_NAME.applyFilter(APP_NAME);
			pages.apic.apiLatencyPage.refresh();
		});

//		this.step("Check API Avg Elapsed Time chart of " + value.get(APICTransRawVal.API_NAME)
//				+ " and see latency higher than 10000 ms", () -> {
//					pages.apic.apiLatencyPage.ApiAvgLatencyChart.parseChartLines();
//					String line_name = value.get(APICTransRawVal.API_NAME);
//					Assert.assertTrue(pages.apic.apiLatencyPage.ApiAvgLatencyChart.validateAllPointsSatisfies(line_name,
//							IConditions.getGreaterThanCondition(9500, 9500)));
//
//				});
		this.step("Drill down to investigate slow transaction", () -> {
			Log.instep("Open Transactions page and look for a transaction with latency > " + latency + " ms");
			SideMenu.MenuSelect.Investigate.getPage();
			TableRow row = pages.apic.trans.transTable.table.getRowValueGreaterThan(APICTransCols.ELAPSED, latency);
			Log.instep("Drill down into the transaction");
			transID = row.getCeil(TransCols.TRANS_ID).getText();
			pages.apic.trans.transTable.clickTransID(row);
		});

		// TODO: Path URL in details page (instead of in URL & in URI)
//		this.step("Check transaction details", () -> {
//			pages.apic.transRawMessages.transDetails.validateValues(value);
//			pages.apic.transRawMessages.requestFlowImage.getApicPoliciesDetailsPanel();
//		});

		this.step("Find out that high latency of caused by side call to another API", () -> {
			Log.instep("Assert invoke-GetBalance policy has the highest elapsed time");

			LinkedHashMap<String, ApicPolicy> policies = pages.apic.transRawMessages.requestFlowImage.getPoliciesList()
					.getUserPolicies();
			for (String policy : policies.keySet()) {
				String[] ms = policies.get(policy).getPolicyElement().getMs().split(" ");
				int num = Integer.parseInt(ms[0]);
				if (num > 6000) {
					Assert.assertTrue(policies.get(policy).getName().equals("invoke-GetBalance"));
					continue;
				}
			}
			// Log.instep("Find out that the sideCall takes all of the latency of the
			// policy");

			Log.instep("Click on the Apic Policy invoke-GetBalance");
			pages.apic.transRawMessages.requestFlowImage.clickOnPolicy("invoke-GetBalance");

			Log.instep(
					"Assert in SideCalls table is not empty and elapsed (ms) is about 200 ms lees than the apic policy");
			side_calls = pages.apic.transRawMessages.requestFlowImage.getApicPoliciesDetailsPanel().getCurrentPolicy()
					.getSideCalls();
			Assert.assertTrue(!side_calls.isEmpty());

			// Log.instep("Find out the side call transaction calls to another API");

			Log.instep("Assert that Side Call table Details column URL contains montierproviderorg508/bankcatalog");
			details_sidecalls = side_calls.get(0).get("Details");
			Assert.assertTrue(details_sidecalls.contains("montierproviderorg508/bankcatalog"));

			pages.apic.transRawMessages.requestFlowImage.getApicPoliciesDetailsPanel().close();
			pages.apic.transRawMessages.refresh();
			Log.instep("Find global transaction id of the transaction");
			globtransID = pages.apic.transRawMessages.transDetails.getValueOf(APICTransRawVal.GL_TRANS_ID);
		});

		this.step("Check transactions of same global transactions", () -> {
			Log.instep("Open transaction page and clear all filters");
			pages.apic.transRawMessages.tabsElem.selectTab("Transactions");
			PageObject.getFilterElem().resetFilters(false, true);
			Log.instep("Filter Global Trans. ID by the value we found in previous step");
			pages.apic.trans.getPageFilters().GLOBAL_TRANS_ID.applyFilter(globtransID);
			pages.apic.trans.refresh();
			Log.instep("Assert two transactions were found");
			int numRows = pages.apic.trans.transTable.table.getNumRows();
			Assert.assertTrue(numRows == 2, "Expected for 2 rows in transactions table but found " + numRows);
			Log.instep("Assert one of the transaction is the previous one");
			TableRow trans_row = pages.apic.trans.transTable.table.getRowValueContains(TransCols.TRANS_ID, transID);
			TableRow sc_trans_row = pages.apic.trans.transTable.table.getRow(trans_row.index == 0 ? 1 : 0);

			transID = sc_trans_row.getValue(TransCols.TRANS_ID);

			Log.instep("Assert the other one called accounts-balance.");
			value.put(APICTransRawVal.API_NAME, "accounts-balance");
			String api = sc_trans_row.getValue(APICTransCols.API);
			Assert.assertTrue(api.contains(value.get(APICTransRawVal.API_NAME)));

		});

		this.step("Drill down to investigate accounts-balance transaction", () -> {
			Log.instep("Click on accounts-balance transaction id.");
			TableRow row = pages.apic.trans.transTable.table.getRowValueContains(TransCols.TRANS_ID, transID);
			elapsed = pages.apic.trans.transTable.table.getValueOf(row.index, TransCols.ELAPSED);
			pages.apic.trans.transTable.clickTransID(row);
			Log.instep("Assert API Version is also 1.0.2");
			Assert.assertEquals(value.get(APICTransRawVal.API_VERSION),
					pages.apic.transRawMessages.transDetails.getValueOf(APICTransRawVal.API_VERSION));

		});
		this.step("Investigate accounts-balance transaction latency and see it caused by side call", () -> {
			Log.instep("Click on getBalance: invoke policy");
			pages.apic.transRawMessages.requestFlowImage.clickOnPolicy("getBalance: invoke");
			// Log.instep("Find out the side call transaction is not for an API");
			// Log.instep("Assert latency caused by side call");
			// Log.instep("Assert getBalance: invoke policy has the highest elapsed time
			// about 500 ms and less than transaction elapsed time.");

			Log.instep(
					"Assert that SideCalls table is not empty and elapsed (ms) is about 200 ms lees than the apic policy");
			side_calls = pages.apic.transRawMessages.requestFlowImage.getApicPoliciesDetailsPanel().getCurrentPolicy()
					.getSideCalls();
			Assert.assertTrue(!side_calls.isEmpty());

			Log.instep("Assert that Side Call table Details column URL contains (/services/AccountService-102)");
			side_calls = pages.apic.transRawMessages.requestFlowImage.getApicPoliciesDetailsPanel().getCurrentPolicy()
					.getSideCalls();
			details_sidecalls = side_calls.get(0).get("Details");
			Assert.assertTrue(details_sidecalls.contains("/services/AccountService-102"));

			pages.apic.transRawMessages.requestFlowImage.getApicPoliciesDetailsPanel().close();

		});
		this.step(
				"Ensure high latency for accounts-balance is not an isolated incident and its happens in version 1.0.2 only",
				() -> {
					String policy_name = "getBalance: invoke";
					PageObject.getFilterElem().resetFilters(true, false);// fix
					Log.instep("go to Dashboard->analytics→API Policies.");
					SideMenu.MenuSelect.Dashboards_Analytics_API_Policies.getPage();
					Log.instep("Filter API Name by accounts-balance and Filter Policy Name by " + policy_name);
					pages.apic.apiPoliciesPage.getPageFilters().API_NAME
							.applyFilter(value.get(APICTransRawVal.API_NAME));
					pages.apic.apiPoliciesPage.getPageFilters().POLICY_TITLE.applyFilter(policy_name);
					pages.apic.apiPoliciesPage.refresh();
					Log.instep("Assert 2 rows one for API Version 1.0.1 and one for API Version 1.0.2");
					Assert.assertTrue(pages.apic.apiPoliciesPage.apiPoliciesTable.getNumRows().equals(2),
							"Number of rows in API Policies are not as expcted");
					Log.instep("Approve that average latency for 1.0.2 is high but low for 1.0.1");
					h_latency = pages.apic.apiPoliciesPage.apiPoliciesTable.getRowValueEqual(PoliciesCols.API_VERSION,
							"1.0.2");
					l_latency = pages.apic.apiPoliciesPage.apiPoliciesTable.getRowValueEqual(PoliciesCols.API_VERSION,
							"1.0.1");

					Log.instep("assert average latency 1.0.2 high and 1.0,1 low");
					String high_avg = h_latency.getValue(PoliciesCols.AVG);
					String low_avg = l_latency.getValue(PoliciesCols.AVG);
					Assert.assertTrue(Integer.parseInt(high_avg) > Integer.parseInt(low_avg));
				});
		this.step("Check Side Call of account-balance transaction in version 1.0.1", () -> {
			Log.instep("Mouse hover API Version column for transaction version 1.0.1 and click on filter sign.");
			pages.apic.apiPoliciesPage.clickOnFilterTable(l_latency, PoliciesCols.API_VERSION);
			Log.instep("Assert page filtered by API Version 1.0.1");

			Log.instep(
					"Open transaction page and click on one of the transactions (filters are sticky so the version is filtered)");
			SideMenu.MenuSelect.Investigate.getPage();
			TableRow row = pages.apic.trans.transTable.table.getRow(1);
			pages.apic.trans.transTable.clickTransID(row);

		});

		this.step("Drill down and investigate transaction from version 1.0.1", () -> {

			Log.instep("Assert API version is 1.0.1");
			Assert.assertEquals(pages.apic.transRawMessages.transDetails.getValueOf(APICTransRawVal.API_VERSION),
					"1.0.1");
//			Log.instep("Assert url of transaction side call for version 1.0.1 is different than version 1.0.2");
//			Assert.assertNotEquals(value.get(APICTransRawVal.IN_URL),
//					pages.apic.transRawMessages.transDetails.getValueOf(APICTransRawVal.IN_URL));

			Log.instep("Click on invoke-GetBalance policy");
			pages.apic.transRawMessages.requestFlowImage.clickOnPolicy("getBalance: invoke");

			Log.instep("Assert URL not contains /services/AccountService-102");
			side_calls = pages.apic.transRawMessages.requestFlowImage.getApicPoliciesDetailsPanel().getCurrentPolicy()
					.getSideCalls();
			details_sidecalls = side_calls.get(0).get("Details");
			Assert.assertTrue(!details_sidecalls.contains("/services/AccountService-102"));

			pages.apic.transRawMessages.requestFlowImage.getApicPoliciesDetailsPanel().close();
		});
		this.step("Add new alert on balance-convert 1.0.2 API when average latency is above a certain chosen threshold",
				() -> {
					Log.instep("Open API URL Calls page");
					PageObject.getFilterElem().resetFilters(true, false);// fix
					SideMenu.MenuSelect.Dashboards_Analytics_API_URL_Calls.getPage();
					Log.instep("Filter In URL by " + value.get(APICTransRawVal.IN_URL));
					pages.apic.apiServiceUrl.getPageFilters().IN_URL.applyFilter(value.get(APICTransRawVal.IN_URL));
					pages.apic.apiServiceUrl.refresh();

					// TODO: There is no option to add alert from URL Calls age
//					Log.instep("Click on Add alert on average latency column of API");
//					TableRow row = pages.apic.apiServiceUrl.apiServiceUrlTable.getRow(0);
//					int avg_ms = Integer.valueOf(row.getValue(ApiServiceUrlCols.AVG));
//					Log.info("Click on Average ms = " + avg_ms);
//					AlertAddPage addAlert = pages.apic.apiServiceUrl.clickAddAlert(row, ApiServiceUrlCols.AVG);
//					addAlert.openPage();
//					Log.instep("Verify alert automatic fields");
//					Assert.assertTrue(addAlert.isValueContains("Name", "API URL Latency"));
//					Assert.assertTrue(addAlert.isValueContains("Description",
//							"Number of transactions with latency greater than " + avg_ms + " ms"));
//					addAlert.clickDetails();
//					String query_details = addAlert.getQueryDetails();
//					Assert.assertTrue(query_details.contains("\"gte\":" + avg_ms));
//					Assert.assertTrue(query_details.contains(value.get(APICTransRawVal.IN_URL)));
//					Log.instep("Check alert exists for API");
//					AlertActions.AddAlert(addAlert, "");// what is the name of the alert?
//					Driver.getInstance().switchToParentHandler();
				});

	}

	TableRow row;

	@Test // (priority = 1)
	public void VariableLatenciesCase_2() {

		value.clear();
		value.put(APICTransRawVal.API_NAME, "balance-convert");
		value.put(APICTransRawVal.CLIENT_IP, "172.17.100.130");
		value.put(APICTransRawVal.STATUS, "OK");
		value.put(APICTransRawVal.API_VERSION, "1.0.2");
		value.put(APICTransRawVal.CATALOG_NAME, "bankcatalog");
		value.put(APICTransRawVal.PROVIDER_ORG, "montierproviderorg508");
		value.put(APICTransRawVal.PRODUCT_ID, "digitalbankingproduct102");
		value.put(APICTransRawVal.PRODUCT_NAME, "digitalbankingproduct102");
		value.put(APICTransRawVal.PLAN_NAME, "default");
		value.put(APICTransRawVal.HTTP_METHOD, "POST");
//		value.put(APICTransRawVal.PLAN_VERSION, "1.0.0");
		value.put(APICTransRawVal.CONSUMER_ORG_ID, "5c20c011e4b0ad0671d5571d");
		value.put(APICTransRawVal.CONSUMER_ORG, "montierproviderorg508");
		value.put(APICTransRawVal.CONSUMER_APP, "digitalbankingapp102");
		value.put(APICTransRawVal.CLIENT_ID, "ecf6c45e-ded1-40fa-bc19-753a859aa631");
		value.put(APICTransRawVal.FE_RESPONSE_CODE, "200");
		value.put(APICTransRawVal.IN_URL,
				"https://172.17.100.70:443/montierproviderorg508/bankcatalog/balance-convert-102/convert");

		this.step("Filter Transactions page to see balance-convert API", () -> {
			SideMenu.MenuSelect.Investigate.getPage();
			Log.instep("reset Filters and Filter API Name by " + value.get(APICTransRawVal.API_NAME)
					+ " and Filter API Version by " + value.get(APICTransRawVal.API_VERSION));
			PageObject.getFilterElem().resetFilters(false, false);
			pages.apic.trans.getPageFilters().API_NAME.applyFilter(value.get(APICTransRawVal.API_NAME));
			pages.apic.trans.getPageFilters().API_VERSION.applyFilter(value.get(APICTransRawVal.API_VERSION));
			pages.apic.trans.refresh();

		});
		this.step("Find out difference of 2 seconds between transactions.", () -> {
			Log.instep("Take transaction and check the elapsed time");
			latencyHigh = 0;
			latencyLow = 0;
			Log.instep("Ensure slow transaction time is higher than 10K ms and the other one less than 9K.");
			this.findTransElapsedHighLow();
		});
		this.step("Check slow transaction details", () -> {
			row = pages.apic.trans.transTable.table.getRowValueGreaterThan(TransCols.ELAPSED, 10000);
			Log.instep("Drill down into the slow transaction.");
			pages.apic.trans.transTable.clickTransID(row);
//			Log.instep("validate values");
//			 pages.apic.transRawMessages.transDetails.validateValues(value);

			Log.instep("Open invoke-GetExcahgeRate policy.");
			pages.apic.transRawMessages.requestFlowImage.clickOnPolicy("invoke-GetExchangeRate");

			Log.instep("Assert elapsed time is higher than 10 seconds.");
			String[] ms = pages.apic.transRawMessages.requestFlowImage.getApicPoliciesDetailsPanel().getCurrentPolicy()
					.getPolicyElement().getMs().split(" ");
			Assert.assertTrue(Integer.parseInt(ms[0]) > 10);
			Log.instep("Assert there is SideCalls calling to 'CurrencyRate-USD'.");
			side_calls = pages.apic.transRawMessages.requestFlowImage.getApicPoliciesDetailsPanel().getCurrentPolicy()
					.getSideCalls();
			details_sidecalls = side_calls.get(0).get("Details");
			Assert.assertTrue(details_sidecalls.contains("CurrencyRate-USD"));

			Log.instep("Assert There is Variable payload Desired Currency with value USD.");
			context_variable = pages.apic.transRawMessages.requestFlowImage.getApicPoliciesDetailsPanel()
					.getCurrentPolicy().getContextVariables();

			details_context_variable = context_variable.get(1).get("Name");
			Assert.assertTrue(details_context_variable.equals("DesiredCurrency"));

			details_context_variable = context_variable.get(1).get("Value");
			Assert.assertTrue(details_context_variable.equals("USD"));
			pages.apic.transRawMessages.requestFlowImage.getApicPoliciesDetailsPanel().close();
			pages.apic.transRawMessages.refresh();

		});
		this.step("Check regular transaction details", () -> {
			pages.apic.transRawMessages.tabsElem.selectTab("Transactions");
			PageObject.getFilterElem().resetFilters(false, true);
			pages.apic.trans.getPageFilters().API_NAME.applyFilter(value.get(APICTransRawVal.API_NAME));
			pages.apic.trans.getPageFilters().API_VERSION.applyFilter(value.get(APICTransRawVal.API_VERSION));
			pages.apic.trans.refresh();

			Log.instep("Drill down into the transaction.");
			row = pages.apic.trans.transTable.table.getRowValueLowerThan(TransCols.ELAPSED, (float) 9000);
			pages.apic.trans.transTable.clickTransID(row);
//			Log.instep("validate values");
//			pages.apic.transRawMessages.transDetails.validateValues(value);

			Log.instep("Open invoke-GetExcahgeRate policy.");
			pages.apic.transRawMessages.requestFlowImage.clickOnPolicy("invoke-GetExchangeRate");

			Log.instep("Assert elapsed time is less than 9 seconds.");
			String[] ms = pages.apic.transRawMessages.requestFlowImage.getApicPoliciesDetailsPanel().getCurrentPolicy()
					.getPolicyElement().getMs().split(" ");
			Log.info(ms[0]);
			Assert.assertTrue(Integer.parseInt(ms[0]) > 9);

			Log.instep("Assert there is SideCalls calling to 'CurrencyRate-EUR'.");
			side_calls = pages.apic.transRawMessages.requestFlowImage.getApicPoliciesDetailsPanel().getCurrentPolicy()
					.getSideCalls();
			details_sidecalls = side_calls.get(0).get("Details");
			Assert.assertTrue(details_sidecalls.contains("CurrencyRate-EUR"));

			Log.instep("Assert There is Variable payload DesiredCurrency with value EUR.");
			context_variable = pages.apic.transRawMessages.requestFlowImage.getApicPoliciesDetailsPanel()
					.getCurrentPolicy().getContextVariables();
			details_context_variable = context_variable.get(1).get("Name");
			Assert.assertTrue(details_context_variable.equals("DesiredCurrency"));
			details_context_variable = context_variable.get(1).get("Value");
			Assert.assertTrue(details_context_variable.equals("EUR"));

		});

	}

	public boolean findTransElapsedHighLow() {
		for (int i = 0; i <= 9; i++) {
			row = pages.apic.trans.transTable.table.getRow(i);
			String row_elapsed = row.getValue(TransCols.ELAPSED);
			if (Integer.parseInt(row_elapsed) > 10000)
				latencyHigh++;
			else if (Integer.parseInt(row_elapsed) < 9000)
				latencyLow++;
		}

		return latencyHigh == latencyLow;
	}
}