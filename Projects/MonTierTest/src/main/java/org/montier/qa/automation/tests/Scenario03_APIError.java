package org.montier.qa.automation.tests;

import org.montier.qa.automation.base.FunctionalTest;
import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.UsersRoles;
import org.montier.qa.automation.base.UsersRoles.UserRole;
import org.montier.qa.automation.elements.SideMenu.MenuSelect;
import org.montier.qa.automation.pages.RoleViewPage.Permissions;
import org.montier.qa.automation.pages.idg.IDGTransactionsPage;
import org.montier.qa.automation.toolkit.actions.Login;
import org.montier.qa.automation.toolkit.elements.TransactionsTable.TransCols;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Scenario03_APIError extends FunctionalTest {

	String dev_user_name = "ApiDeveloper";
	String device_name;
	String service_name;

//Unexists: bankcatalog
	@Test
	public void APIError() {
		Log.description("Test goal - DPOD supports API transactions processed by API Connect within DataPower Gateway. "
				+ "DPOD is very useful for non-production environments by providing limited access transactional information for developers and service consumers. "
				+ "As a DataPower administrator, you may offload some of the investigation process to developers.");

		this.step("Load roles of user ApiDeveloper", () -> {
			Log.instep("Go into Users page and load ApiDeveloper user role");
			UsersRoles.getInstance().addRole(dev_user_name);
			UserRole inve_role = UsersRoles.getInstance().getUserRole(dev_user_name);
			Log.instep("Verify user has no ability to see payload and raw Messages");
			Assert.assertFalse(inve_role.getPermission(Permissions.AllowRawMessages),
					"Expected for AllowRawMessages premission to be NO");
			Assert.assertFalse(inve_role.getPermission(Permissions.AllowPayload),
					"Expected for AllowPayload premission to be NO");
			Assert.assertFalse(inve_role.getPermission(Permissions.managePayloadCapture),
					"Expected for managePayloadCapture premission to be NO");
			Assert.assertFalse(inve_role.getPermission(Permissions.allowManageAPICPolicyVariablesCaptures),
					"Expected for allowManageAPICPolicyVariablesCaptures premission to be NO");
//			@Sasha - I added the following permissions:
			Assert.assertFalse(inve_role.getPermission(Permissions.validateRemoteWSDL),
					"Expected for validateRemoteWSDL premission to be NO");
			Assert.assertFalse(inve_role.getPermission(Permissions.promoteRemoteWSDL),
					"Expected for promoteRemoteWSDL premission to be NO");
			Assert.assertFalse(inve_role.getPermission(Permissions.allowWSDLURLChange),
					"Expected for allowWSDLURLChange premission to be NO");
			Assert.assertFalse(inve_role.getPermission(Permissions.validateLocalWSDL),
					"Expected for validateLocalWSDL premission to be NO");
			Assert.assertFalse(inve_role.getPermission(Permissions.promoteLocalWSDL),
					"Expected for promoteLocalWSDL premission to be NO");
			Assert.assertFalse(inve_role.getPermission(Permissions.stopStartService),
					"Expected for stopStartService premission to be NO");
			Assert.assertFalse(inve_role.getPermission(Permissions.importService),
					"Expected for importService premission to be NO");
			Log.instep("Verify user allowed domains are  APIMgmt_A72F3635F3, APIMgmt_B82C3775F4");

		});
		this.step("Switch to user ApiDeveloper", () -> {
			Login.switchUser(dev_user_name, dev_user_name);
			Log.instep("Assert user landing page is recentActiviy");
			pages.idg.recentActivity.openPage();
			Log.instep("Validate user DOMAIN filter contains just allowed services");
			Assert.assertTrue(pages.idg.recentActivity.validateFilterOptionsForUser());
			Log.instep("Validate recentActivity tablescontains allowed resources only");
			Assert.assertTrue(pages.idg.recentActivity.validateContentByUserPermission());
		});
		this.step("Enter Service Per Device page and check content", () -> {
			MenuSelect.Dashboards_Analytics_Service_perDevice.getPage();
			Log.instep("Filter Time by Last Hour");
			pages.idg.srvcPerDevice.getPageFilters().TIME.applyFilter("Last Hour");
			Log.instep("Validate device transactions table contains row for each pair of device with allowed service");
			// Assert.assertTrue(pages.idg.srvcPerDevice.validateTableContent(), "Unexpected
			// service-device pair in table");
		});
		this.step("Open error transactions on one of the devices", () -> {
			Log.instep("Filter Status by Error");
			pages.idg.srvcPerDevice.getPageFilters().STATUS.applyFilter("ERROR");
			pages.idg.srvcPerDevice.refresh();
			Log.instep("In service per device page click on one of the services");

			String device_service = pages.idg.srvcPerDevice.clickOnRow(0); // TODO: change the name of the function?
			device_name = device_service.split(",")[0];
			service_name = device_service.split(",")[1];
		});

		this.step("Validate transaction page according to ApiDeveloper user permission", () -> {
			Log.instep(
					"Validate that transactions page not contains RawMessages, PayloadCapture and ContextVariableCapture ans denied in user permission");
			Assert.assertNull(pages.idg.trans.tabsElem.selectTab("Raw Messages"), "Raw Messages tab unexpected exists");
			Assert.assertNull(pages.idg.trans.tabsElem.selectTab("Payload Capture"),
					"Payload Captures tab unexpected exists");
			Log.instep("Validate transactions table also not contains payload column");
			Assert.assertFalse(
					pages.idg.trans.transTable.table.tableElmnt.colNames.contains(TransCols.PAYLOAD.toString()));
		});

		this.step("Drill down to investigate failed transaction", () -> {
			Log.instep("click one of the transactions");
			pages.idg.trans.transTable.clickTransID(pages.idg.trans.transTable.table.getRow(0));
			Log.instep("Validate transaction details");
			// Status = ERROR Device = as we choose step before Service = as we choose step
			// before Operation = (question) clientIP = (question)
			Log.instep("Validate there are no payload and rawMessage tabs");
			Assert.assertNull(pages.idg.transRawMessages.tabsElem.selectTab("RawMessages"),
					"Raw Messages tab unexpected exists");
			// Assert.assertNull(pages.idg.transRawMessages.tabsElem, "Raw Messages tab
			// unexpected exists");
			Log.instep("Validate ExtendedLatenct exists");
			// pages.idg.transRawMessages = null
			// pages.idg.transRawMessages.tabsPanelElem.selectTab("Extended Latency");
		});

		// this.step("extract failure reason from the limited transaction details", ()
		// -> {
		// Log.instep("Assert error analysis contains HTTP.....");
		// Assert.assertTrue(pages.idg.extendLatePage.transactionAnalysisPanel.getErrorMessage().equals("HTTP....."));
		// });
		this.step("Check transactions table filters", () -> {
			pages.idg.trans.tabsElem.moveToTab(IDGTransactionsPage.PAGE_NAME);
			pages.idg.transRawMessages.getFilterElem().resetFilters(false, false);
			pages.idg.trans.getPageFilters().SERVICE.applyFilter("MakeAppointment_WSS.WSP");
			pages.idg.trans.refresh();
			Assert.assertTrue(pages.idg.trans.transTable.table.getNumRows() == 0,
					"unexpected rows for service MakeAppointment_WSS.WSP thats not allowed for user");
		});
	}
}