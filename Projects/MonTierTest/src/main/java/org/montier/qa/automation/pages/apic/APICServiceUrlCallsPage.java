package org.montier.qa.automation.pages.apic;

import java.util.Arrays;

import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.PagesFactory;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.montier.qa.automation.base.utils.Table.TableRow;
import org.montier.qa.automation.pages.AlertAddPage;
import org.openqa.selenium.WebElement;

public class APICServiceUrlCallsPage extends PageObject {
	public class ApiServiceUrlFilters extends PageFilters {
		public FilterUtils TIME = new FilterUtils("Time");
		public FilterUtils DEVICE = new FilterUtils("Device");
		public FilterUtils DOMAIN = new FilterUtils("Domain");
		public FilterUtils CATALOG = new FilterUtils("Catalog");
		public FilterUtils SPACE = new FilterUtils("Space");
		public FilterUtils PRODUCT = new FilterUtils("Product");
		public FilterUtils PLAN = new FilterUtils("Plan");
		public FilterUtils API_NAME = new FilterUtils("API Name");
		public FilterUtils API_VERSION = new FilterUtils("API Version");
		public FilterUtils APP_NAME = new FilterUtils("App Name");
		public FilterUtils IN_URL = new FilterUtils("In URL");
		public FilterUtils IN_URI = new FilterUtils("In URI");

	}

	public static class ApiServiceUrlCols extends TableCols {
		public static Col URL = new Col("URL");
		public static Col API_NAME = new Col("API Name");
		public static Col AVG = new Col("Avg (ms.)");
		public static Col MAX = new Col("Max (ms.)");
		public static Col MIN = new Col("Min (ms.)");
		public static Col CALLS = new Col("Calls");
		public static Col Percentile90 = new Col("90% Percentile");
		public static Col Percentile95 = new Col("95% Percentile");
		public static Col Percentile99 = new Col("99% Percentile");

		public ApiServiceUrlCols() {
			super();
		}

		@Override
		public void setColsIndices() {
			colsIndices.addAll(
					Arrays.asList(URL, API_NAME, AVG, MAX, MIN, CALLS, Percentile90, Percentile95, Percentile99));
		}
	}

	@Find(by = FBy.xpath, value = "//div[contains(@class, 'ApicServiceUrlCallsGrid')]")
	protected WebElement ApicServiceUrlCallsGrid;
	@Find(by = FBy.xpath, value = "//a[contains(@href, 'alertAdd')]")
	ByPath add_alert;

	public Table apiServiceUrlTable;

	public APICServiceUrlCallsPage() {
		super();
	}

	@Override
	public ApiServiceUrlFilters getPageFilters() {
		if (this.pageFilters == null)
			this.pageFilters = new ApiServiceUrlFilters();
		return (ApiServiceUrlFilters) this.pageFilters;

	}

	@Override
	public void initPageElement() {
		this.apiServiceUrlTable = new Table(ApicServiceUrlCallsGrid, new ApiServiceUrlCols());
	}

	public AlertAddPage clickAddAlert(TableRow row, Col col) {
		String hover_elem_xpath = add_alert.path;
		this.apiServiceUrlTable.MouseHoverAndClick(row, col, hover_elem_xpath);
		this.switchToSubWindow(PagesFactory.getPages().getAlertAddPage());
		return PagesFactory.getPages().getAlertAddPage();
	}

}
