package org.montier.qa.automation.elements;

import java.util.ArrayList;
import java.util.List;

import org.montier.qa.automation.base.Element;
import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.openqa.selenium.WebElement;

import lombok.AllArgsConstructor;
import lombok.ToString;

public class FadedMessageElement extends Element {
	@ToString
	@AllArgsConstructor
	public enum MessageType {
		ERROR("alert-danger"), INFO("alert-info"), SUCCESS("alert-success"), EVERYTHING("");
		private final String stringValue;

		public static MessageType getKey(String val) {
			switch (val) {
			case "alert-danger":
				return MessageType.ERROR;
			case "alert-info":
				return MessageType.INFO;
			case "alert-success":
				return MessageType.SUCCESS;
			}
			return MessageType.EVERYTHING;
		}
	}

	public static class Message {
		MessageType type;
		String text;

		Message(MessageType type, String text) {
			this.type = type;
			this.text = text;
		}

		public String getText() {
			return this.text;
		}

		public MessageType getType() {
			return this.type;
		}

		public boolean isError() {
			return this.type == MessageType.ERROR;
		}

		@Override
		public String toString() {
			return this.type.name() + " - " + this.text;
		}
	}

	@Find(by = FBy.xpath, value = "//div[contains(@class, 'alert') and contains(@class, 'fadeInDown')]")
	ByPath faded_message;

	private static FadedMessageElement INSTANCE;
	public String danger_class_name = "alert-danger";
	public String info_class_name = "alert-info";
	public String success_class_name = "alert-success";
	public List<Message> messages;

	private FadedMessageElement() {
		super();
		this.messages = new ArrayList<Message>();
	}

	public void initValues() {
		this.messages = new ArrayList<Message>();
		List<WebElement> messages_elem = this.findElementIfExists(this.faded_message);
		for (WebElement message : messages_elem) {
			this.messages.add(new Message(getAlertType(message), message.getText()));
		}
	}

	public static FadedMessageElement getInstance() {
		if (INSTANCE == null) {
			synchronized (FadedMessageElement.class) {
				if (INSTANCE == null) {
					INSTANCE = new FadedMessageElement();
				}
			}
		} else
			INSTANCE.messages = new ArrayList<Message>();
		return INSTANCE;
	}

	public MessageType getAlertType(WebElement message) {
		// Alert message class is like "alert alert-danger growl-animated animated fadeInDown"
		String classa = message.getAttribute("class");

		String class_name = classa.split(" ")[1];
		return MessageType.getKey(class_name);
	}

	public List<Message> getMessages(MessageType type) {
		this.initValues();
		List<Message> msgs = new ArrayList<Message>();
		for (Message message : messages) {
			if (message.getType().equals(type) || type == MessageType.EVERYTHING)
				msgs.add(message);
		}
		if (type != MessageType.EVERYTHING && msgs.isEmpty())
			Log.debug("No message type " + type);
		return msgs;
	}

	public List<Message> getAllMessages() {
		return this.getMessages(MessageType.EVERYTHING);
	}

	public Message getMessage(MessageType type) {
		List<Message> msgs = this.getMessages(type);
		if (msgs.size() != 1) {
			Log.info("Founded " + msgs.size() + " messages instead of 1");
			for (Message msg : msgs)
				Log.info(msg.getText());
		}
		return msgs.size() > 0 ? msgs.get(0) : null;
	}

	public String getAlertText(WebElement message) {
		return message.getText();
	}
}
