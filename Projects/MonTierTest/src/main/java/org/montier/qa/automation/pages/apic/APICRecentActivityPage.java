package org.montier.qa.automation.pages.apic;

import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.UsersRoles;
import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.montier.qa.automation.elements.PercentileSummaryPanelElement;
import org.openqa.selenium.WebElement;

public class APICRecentActivityPage extends PageObject {
	public class APICRecentActivityFilters extends PageFilters {
		public FilterUtils TIME = new FilterUtils("Time");
		public FilterUtils DEVICE = new FilterUtils("Device");
		public FilterUtils DOMAIN = new FilterUtils("Domain");
		public FilterUtils CATALOG = new FilterUtils("Catalog");
		public FilterUtils SPACE = new FilterUtils("Space");
		public FilterUtils PRODUCT = new FilterUtils("Product");
		public FilterUtils PLAN = new FilterUtils("Plan");
		public FilterUtils APi_NAME = new FilterUtils("API Name");
		public FilterUtils API_VERSION = new FilterUtils("API Version");
		public FilterUtils APP_NAME = new FilterUtils("App Name");
	}

	public static class LatencyCols extends TableCols {
		public static final Col API_NAME = new Col("API Name");
		public static final Col LATENCY = new Col("Latency");
	}

	public static class ActivityByProductCols extends TableCols {
		public static final Col PRODUCT_NAME = new Col("Product Name");
		public static final Col SUCCESSFUL_TRANSACTIONS = new Col("Successful Transactions");
	}

	public static class ActivityByAPICols extends TableCols {
		public static final Col API_NAME = new Col("API Name");
		public static final Col SUCCESSFUL_TRANSACTIONS = new Col("Successful Transactions");
	}

	public static class APIErrorsCols extends TableCols {
		public static final Col API_NAME = new Col("API Name");
		public static final Col ERRORS = new Col("Errors");
	}

	@Find(by = FBy.className, value = "ApicLatencyPercentileSummaryPanel")
	WebElement apic_latency_percentile;
	@Find(by = FBy.className, value = "ApicLatencyPercentileSummaryPanel")
	WebElement apic_successfull_api_percentile;
	@Find(by = FBy.className, value = "ApicErrorApisSummaryPanel")
	WebElement apic_error_api_percentile;
	@Find(by = FBy.className, value = "ApicLatencyPercentileGrid")
	WebElement latency_table;
	@Find(by = FBy.className, value = "ApicProductActivityGrid")
	WebElement activity_by_product_table;
	@Find(by = FBy.className, value = "ApicApiActivityGrid")
	WebElement activity_by_api_table;
	@Find(by = FBy.className, value = "ApicApiErrorsGrid")
	WebElement api_errors_table;

	public PercentileSummaryPanelElement apicLatencyPercentile;
	public PercentileSummaryPanelElement apicSuccessfulApiPercentile;
	public PercentileSummaryPanelElement apicErrorApiPercentile;
	public Table latencyTable;
	public Table activityByProductTable;
	public Table activityByAPITable;
	public Table errorsTable;

	public APICRecentActivityPage() {
		super();
	}

	@Override
	public void initPageElement() {
		// apicLatencyPercentile = new
		// PercentileSummaryPanelElement(apic_latency_percentile);
		// apicSuccessfulApiPercentile = new
		// PercentileSummaryPanelElement(apic_successfull_api_percentile);
		// apicErrorApiPercentile = new
		// PercentileSummaryPanelElement(apic_error_api_percentile);
		this.latencyTable = new Table(latency_table, new LatencyCols());
		this.activityByProductTable = new Table(activity_by_product_table, new ActivityByProductCols());
		this.activityByAPITable = new Table(activity_by_api_table, new ActivityByAPICols());
		this.errorsTable = new Table(api_errors_table, new APIErrorsCols());
	}

	@Override
	public APICRecentActivityFilters getPageFilters() {
		if (this.pageFilters == null)
			this.pageFilters = new APICRecentActivityFilters();
		return (APICRecentActivityFilters) this.pageFilters;
	}

	public boolean validateContentByUserPermission() {
		Boolean cond = validateTableContent(latencyTable, LatencyCols.API_NAME, "API Name")
				&& validateTableContent(activityByProductTable, ActivityByProductCols.PRODUCT_NAME, "Product")
				&& validateTableContent(activityByAPITable, ActivityByAPICols.API_NAME, "API Name")
				&& validateTableContent(errorsTable, APIErrorsCols.API_NAME, "API Name");
		return cond;
	}

	private boolean validateTableContent(Table table, Col col, String resource) {
		for (int i = 0; i < table.getNumRows(); i++)
			if (!UsersRoles.isResourceAllowedForCurrentUser(resource, table.getValueOf(i, col))) {
				Log.error("Line number " + i + 1 + " contains " + resource + " " + table.getValueOf(i, col)
						+ " and its not allowed by user");
				return false;
			}
		return true;
	}

}
