package org.montier.qa.automation.elements;

import java.util.TreeMap;

import org.montier.qa.automation.base.Element;
import org.openqa.selenium.WebElement;

public class PercentileSummaryPanelElement extends Element {
	public static class PercentileChart extends SVGChartElement {

		public PercentileChart(WebElement chart_elem) {
			super(chart_elem);
		}
	}

	private PercentileChart percChart;
	String panelTitle;
	String value;

	public PercentileSummaryPanelElement(WebElement base_elem) {
		super(base_elem);
		String[] elem_text = base_elem.getText().split("\n");
		panelTitle = elem_text[0];
		value = elem_text[1];
		percChart = new PercentileChart(base_elem);
	}

	public boolean lineShowsValues() {
		percChart.parseChartLines();
		int count = 0;
		TreeMap<Float, Float> points = percChart.lines.get("general").getLinePoints();
		for (Float y : points.values()) {
			if (y < percChart.height / 2)
				count++;
		}
		return count > points.size() / 2;
	}
}
