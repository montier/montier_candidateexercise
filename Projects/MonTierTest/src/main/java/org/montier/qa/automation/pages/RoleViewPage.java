package org.montier.qa.automation.pages;

import java.util.Arrays;
import java.util.HashMap;

import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.montier.qa.automation.elements.KeyValuePanelElement;
import org.montier.qa.automation.elements.SideMenu;
import org.openqa.selenium.WebElement;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class RoleViewPage extends PageObject {
	@AllArgsConstructor
	public static enum Permissions {
		AllowRawMessages("Allow Raw Messages"), AllowPayload("Allow Payload"), managePayloadCapture("Manage Payload Capture"),
		allowViewingAPICPolicyVariables("Allow Viewing API-C Policy Variables"),
		allowManageAPICPolicyVariablesCaptures("Allow Manage API-C Policy Variables Captures"), validateRemoteWSDL("Validate Remote WSDL"),
		promoteRemoteWSDL("Promote Remote WSDL"), allowWSDLURLChange("Allow WSDL URL Change"), validateLocalWSDL("Validate Local WSDL"),
		promoteLocalWSDL("Promote Local WSDL"), stopStartService("Stop/Start Service"), importService("Import Service");

		private final @Getter String stringValue;

		@Override
		public String toString() {
			return stringValue;
		}

	}

	public static void navigateTo(String user_name) {
		Log.instep("Go to Users page and click on " + user_name + " user");
		UsersPage users_page = (UsersPage) SideMenu.MenuSelect.Manage_Security_Users.getPage();
		UserViewPage user_view_page = users_page.clickOnUser(user_name);
		Log.instep("In UserViewPage click on user role");
		String user_role = user_view_page.findUserRole();
		Log.info("User role is " + user_role);
		user_view_page.clickOnRole(user_role);
	}

	@AllArgsConstructor
	public static enum Resources {
		GATEWAY_APIC("Gateway/API-C"), GATEWAY("Gateway"), API_CONNECT("API Connect");

		private final @Getter String stringValue;

		@Override
		public String toString() {
			return stringValue;
		}

	}

	public static class RolesOfUserCols extends TableCols {
		public static final Col NAME = new Col("Name");
		public static final Col REMOVE = new Col("");

		public RolesOfUserCols() {
			super();
		}

		@Override
		public void setColsIndices() {
			colsIndices.addAll(Arrays.asList(NAME, REMOVE));
		}
	}

	@Find(by = FBy.className, value = "UsersInRoleTable")
	public ByPath users_in_role_table;
	@Find(by = FBy.xpath, value = "//div[text()='Actions and Permissions']/parent::div[contains(@class, 'panel')]")
	public WebElement actions_and_permissions_panel;
	@Find(by = FBy.xpath, value = "//div[text()='Allowed Resources']/parent::div[contains(@class, 'panel')]")
	public WebElement allowed_resources;
	@Find(by = FBy.xpath, value = "//div[text()='Denied Resources']/parent::div[contains(@class, 'panel')]")
	public WebElement denied_resources;
	@Find(by = FBy.xpath, value = "//div[@class='panel-body']//child::label[text()='$KEY$']//following-sibling::div")
	private ByPath key_path;

	public String userName;
	public Table usersInRolesTable;
	public KeyValuePanelElement<Permissions> actionsAndPermissions;
	public KeyValuePanelElement<Resources> allowedResources;
	public KeyValuePanelElement<Resources> deniedResources;

	public RoleViewPage() {
		super();
	}

	@Override
	public PageFilters getPageFilters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void initPageElement() {
		this.usersInRolesTable = new Table(this.findElement(users_in_role_table), new RolesOfUserCols());
		actionsAndPermissions = new KeyValuePanelElement<>(actions_and_permissions_panel, Permissions.values(), key_path);
		allowedResources = new KeyValuePanelElement<>(allowed_resources, Resources.values(), key_path);
		deniedResources = new KeyValuePanelElement<>(denied_resources, Resources.values(), key_path);
	}

	public HashMap<Resources, HashMap<String, String[]>> getResourcesPerProduct(String type) {
		HashMap<Resources, HashMap<String, String[]>> resources_per_products = new HashMap<>();
		KeyValuePanelElement<Resources> resources = type.equals("denied") ? deniedResources : allowedResources;
		for (Resources product : Resources.values()) {
			resources_per_products.put(product, getChosenResourcesForProduct(product, resources.getValueOf(product)));
		}
		return resources_per_products;
	}

	public HashMap<String, String[]> getChosenResourcesForProduct(Resources product, String chosen_list) {
		HashMap<String, String[]> per_resource = new HashMap<String, String[]>();
		switch (product) {
		case GATEWAY_APIC:
			per_resource.put("Device", getListOfChosenResource("Device", chosen_list));
			per_resource.put("Domain", getListOfChosenResource("Domain", chosen_list));
			per_resource.put("Client IP", getListOfChosenResource("Device", chosen_list));
			break;
		case GATEWAY:
			per_resource.put("Service", getListOfChosenResource("Service", chosen_list));
			break;
		case API_CONNECT:
			per_resource.put("Catalog", getListOfChosenResource("Catalog", chosen_list));
			per_resource.put("Space", getListOfChosenResource("Space", chosen_list));
			per_resource.put("Product", getListOfChosenResource("Product", chosen_list));
			per_resource.put("Plan", getListOfChosenResource("Plan", chosen_list));
			per_resource.put("API Name", getListOfChosenResource("API Name", chosen_list));
			per_resource.put("Consumer Org Name", getListOfChosenResource("Consumer Org Name", chosen_list));
			per_resource.put("App Name", getListOfChosenResource("App Name", chosen_list));
			per_resource.put("Client ID", getListOfChosenResource("Client ID", chosen_list));
			break;
		}
		return per_resource;
	}

	public String[] getListOfChosenResource(String resource, String chosen_list) {
		if (!chosen_list.contains(resource))
			return null;
		int index_of_resource = chosen_list.indexOf(resource);
		String resource_list = chosen_list.substring(chosen_list.indexOf('[', index_of_resource) + 1, chosen_list.indexOf(']', index_of_resource))
				.trim();
		return resource_list.split(",");
	}
}
