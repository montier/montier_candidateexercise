package org.montier.qa.automation.toolkit.elements;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

import org.montier.qa.automation.base.Element;
import org.montier.qa.automation.base.PagesFactory;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.RunTestException;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.montier.qa.automation.elements.TabsPanelElement;
import org.montier.qa.automation.elements.TabsPanelElement.Tab;
import org.montier.qa.automation.elements.TopPanel.ProductType;
import org.openqa.selenium.WebElement;

public class TransactionComponents extends Element {

	public static class SideCalls extends Element {
		public static class SideCallsCols extends TableCols {
			public static final Col TYPE = new Col("Type");
			public static final Col DIRECTION = new Col("Direction");
			public static final Col TIME = new Col("Time");
//			public static final Col POLICY_TITLE = new Col("Policy Title");
			public static final Col ELASPED = new Col("Elasped (ms.)");
			public static final Col STATUS = new Col("Status");
			public static final Col DETAILS = new Col("Details");

			@Override
			public void setColsIndices() {
				colsIndices.addAll(Arrays.asList(TYPE, DIRECTION, TIME, ELASPED, STATUS, DETAILS));
			}
		}

		public static class APICSideCallsCols extends SideCallsCols {
			public static final Col POLICY_TITLE = new Col("Policy Title");

			@Override
			public void setColsIndices() {
				colsIndices.addAll(Arrays.asList(TYPE, TIME, POLICY_TITLE, ELASPED, STATUS, DETAILS));
			}
		}

		@Find(by = FBy.className, value = "TransactionSideCallsPanel")
		ByPath sidecalls_table;

		public Table sideCallsTable;

		public SideCalls(WebElement base_elem) {
			super(base_elem);
			TableCols cols = PagesFactory.getInstance().getProduct() == ProductType.APIC ? new APICSideCallsCols()
					: new SideCallsCols();
			this.sideCallsTable = new Table(this.findElement(sidecalls_table), cols);
		}
	}

	public static class RawMessages extends Element {
		public static class RawMessagesCols extends TableCols {
			public static final Col CATEGORY = new Col("Category");
			public static final Col SEVERITY = new Col("Severity");
			public static final Col TIME = new Col("Time");
			public static final Col DIRECTION = new Col("Direction");
			public static final Col OPJECT_TYPE = new Col("Opject Type");
			public static final Col OPJECT_NAME = new Col("Opject Name");
			public static final Col CLIENT_IP = new Col("Client IP");
			public static final Col MESSAGE_CODE = new Col("Message Code");
			public static final Col MESSAGE = new Col("Message");

			@Override
			public void setColsIndices() {
				colsIndices.addAll(Arrays.asList(CATEGORY, SEVERITY, TIME, DIRECTION, OPJECT_TYPE, OPJECT_NAME,
						CLIENT_IP, MESSAGE_CODE, MESSAGE));
			}
		}

		@Find(by = FBy.tagName, value = "table")
		ByPath raw_messages_table;

		public Table rawMessagesTable;

		public RawMessages(WebElement web_elem) {
			super(web_elem);
			this.rawMessagesTable = new Table(this.findElement(raw_messages_table), new RawMessagesCols());
		}
	}

	public static class Payload extends Element {
		@Find(by = FBy.linkText, value = "Front-End Request")
		ByPath front_end_request;
		@Find(by = FBy.linkText, value = "Back-End Request")
		ByPath back_end_request;
		@Find(by = FBy.linkText, value = "Back-End Response")
		ByPath back_end_response;
		@Find(by = FBy.linkText, value = "Front-End Response")
		ByPath front_end_response;
		@Find(by = FBy.xpath, value = "//pre")
		ByPath payload_text;

		public Payload(WebElement base_elem) {
			super(base_elem);
		}

		public void selectBackEndRequest() {
			this.findElement(back_end_request).click();
		}

		public void selectFrontEndRequest() {
			this.findElement(front_end_request).click();
		}

		public String getPayloadVal() {
			return this.findElement(payload_text).getText();
		}
	}

	public static class ExtendedLatency extends Element {
		public static class EventsCols extends TableCols {
			public static final Col CODE = new Col("Code");
			public static final Col ELAPSED = new Col("Elapsed");
			public static final Col DURATION = new Col("Duration");
			public static final Col DESCRIPTION = new Col("Description");

			@Override
			public void setColsIndices() {
				colsIndices.addAll(Arrays.asList(CODE, ELAPSED, DURATION, DESCRIPTION));
			}
		}

		@Find(by = FBy.xpath, value = "//div[contains(@class, 'panel-body')]")
		ByPath events_tables;

		public Table requestEventsTable;
		public Table responseEventsTable;

		public ExtendedLatency(WebElement base_elem) {
			super(base_elem);
			List<WebElement> tables = this.findElements(events_tables);
			requestEventsTable = new Table(tables.get(0), new EventsCols());
			responseEventsTable = new Table(tables.get(tables.size() - 1), new EventsCols());
		}
	}

	public RawMessages rawMessages;
	public ExtendedLatency extendedLatency;
	public Payload payload;
	public SideCalls sideCalls;

	private TabsPanelElement tabsPanelElem = null;

	public TransactionComponents(WebElement base_elem, Class<?> raw_message_page, Class<?> side_calls_page,
			Class<?> payload_page, Class<?> extended_latency_page) {
		super(base_elem);
		tabsPanelElem = new TabsPanelElement(base_elem, new Tab("Raw Messages", raw_message_page),
				new Tab("Payload", payload_page), new Tab("Side Calls", side_calls_page),
				new Tab("Extended Latency", extended_latency_page));

	}

	public TabsPanelElement getTabsPanelElement() {
		return this.tabsPanelElem;
	}

	public void selectTab(String tab_text) {
		tabsPanelElem.selectTab(tab_text);
	}

	public Object getTabContent(Class<?> panel_class) {
		this.rawMessages = null;
		this.payload = null;
		this.extendedLatency = null;
		this.sideCalls = null;
		try {
			return panel_class.getConstructor(WebElement.class).newInstance(this.baseElement);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			throw new RunTestException("Failed while init constractor Element " + panel_class.getName()
					+ " with parameter " + this.baseElement);
		}
	}
}
