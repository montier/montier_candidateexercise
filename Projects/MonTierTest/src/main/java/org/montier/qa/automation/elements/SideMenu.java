package org.montier.qa.automation.elements;

import java.util.Arrays;
import java.util.List;

import org.montier.qa.automation.base.Driver;
import org.montier.qa.automation.base.Element;
import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.PagesFactory;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.RunTestException;
import org.montier.qa.automation.pages.AlertsSetupPage;
import org.montier.qa.automation.pages.MonitoredDevicesPage;
import org.montier.qa.automation.pages.RestartDataPage;
import org.montier.qa.automation.pages.UsersPage;
import org.montier.qa.automation.pages.apic.APICApiLatencyPage;
import org.montier.qa.automation.pages.apic.APICApiPoliciesPage;
import org.montier.qa.automation.pages.apic.APICRecentActivityPage;
import org.montier.qa.automation.pages.apic.APICServiceUrlCallsPage;
import org.montier.qa.automation.pages.apic.APICTransactionsPage;
import org.montier.qa.automation.pages.idg.IDGCertificatesExpirationPage;
import org.montier.qa.automation.pages.idg.IDGRecentActivityPage;
import org.montier.qa.automation.pages.idg.IDGServiceActivityPage;
import org.montier.qa.automation.pages.idg.IDGServiceLatencyPage;
import org.montier.qa.automation.pages.idg.IDGServicePerDevicePage;
import org.montier.qa.automation.pages.idg.IDGServiceURICalls;
import org.montier.qa.automation.pages.idg.IDGServicesConfiguration;
import org.montier.qa.automation.pages.idg.IDGSystemOverviewPage;
import org.montier.qa.automation.pages.idg.IDGTransactionsPage;
import org.openqa.selenium.WebElement;

import lombok.Getter;

public class SideMenu extends Element {
	public static SideMenu INSTANCE = null;
	@Find(by = FBy.xpath, value = "//div[@class='NotificationsMenuButton']/div")
	ByPath notification_button;
	@Find(by = FBy.xpath, value = "//div[contains(@class, 'sideNavbarMenuPopover')]//div[@class='Notification']", from_root = true)
	ByPath notification_value;

	private SideMenu() {
		super();
	}

	public static SideMenu getInstance() {
		if (INSTANCE == null)
			return new SideMenu();
		else
			return INSTANCE;
	}

	public static void openPage(PageObject page) {
		Arrays.asList(MenuSelect.values()).stream().filter(path -> path.isContainsPage(page)).findFirst().get().getPage();
	}

	public String getNotifications() {
		Log.debug("Click on notification button in side menu bar");
		this.findElement(notification_button).click();
		Log.debug("Collect notification content");
		String content = "";
		for (WebElement notif : this.findElementIfExists(notification_value))
			content += notif.getText() + "\n";
		Log.debug("Refresh page to close notification"); // TODO: BUG! check it
		Driver.getInstance().openUrl(Driver.getInstance().getDriver().getCurrentUrl());
		Log.debug("Refresh page by driver"); // TODO: BUG! check it
		Driver.getInstance().getDriver().navigate().refresh();
		Log.debug("Refresh page elements");
		PagesFactory.getInstance().currentPage.refresh();
		return content;
	}

	public enum MenuSelect {
		Dashboards_Overview_SystemOverview(IDGSystemOverviewPage.class), Dashboards_Analytics_Service_URI_Calls(IDGServiceURICalls.class),
		Dashboards_Analytics_ServiceLatency(IDGServiceLatencyPage.class), Dashboards_Analytics_Service_perDevice(IDGServicePerDevicePage.class),
		Investigate(IDGTransactionsPage.class, APICTransactionsPage.class), Explore(IDGServicesConfiguration.class),
		ReportsAlerts_Alerts_Alerts(AlertsSetupPage.class), Reports_Alerts_Alerts(AlertsSetupPage.class),
		Manage_Devices_Gateways(MonitoredDevicesPage.class), Dashboards_Analytics_ServiceActivity(IDGServiceActivityPage.class),
		Dashboards_Analytics_API_Policies(APICApiPoliciesPage.class), Dashboards_Security_ExpiredCertificates(IDGCertificatesExpirationPage.class),
		Dashboards_Analytics_Restarts(RestartDataPage.class), Manage_Security_Users(UsersPage.class),
		Dashboards_Overview_RecentActivity(APICRecentActivityPage.class, IDGRecentActivityPage.class),
		Dashboards_Analytics_API_Latency(APICApiLatencyPage.class), Dashboards_Analytics_API_URL_Calls(APICServiceUrlCallsPage.class),;
		private @Getter Class<?>[] pageClass;

		MenuSelect(Class<?>... cls) {
			this.pageClass = cls;
		}

		public PageObject getPage() {
			Log.info("Side Menu select " + this.name());
			Selected sm = Selected.getSelected(this.name(), this.pageClass);
			return sm.getPage();
		}

		public void select() {
			Selected.getSelected(this.name(), this.pageClass);
		}

		public boolean isContainsPage(PageObject page) {
			for (Class<?> cls : this.getPageClass())
				if (cls.getName().equals(page.getClass().getName()))
					return true;
			return false;
		}
	}

	public static class Selected extends Element {
		private static Selected SINSTANCE = null;
		private String path;
		private String targetPage;

		@Find(by = FBy.partialLinkText, value = "$NAME$")
		ByPath side_menu;
		@Find(by = FBy.className, value = "SideMenu")
		ByPath side_menu_class;

		private Selected() {
			super();
		}

		public static Selected getSelected() {
			if (SINSTANCE == null)
				SINSTANCE = new Selected();
			return SINSTANCE;
		}

		public static Selected getSelected(String path, Class<?>[] page_cls) {
			SINSTANCE.path = path;
			if (page_cls.length > 1)
				SINSTANCE.targetPage = Arrays.asList(page_cls).stream()
						.filter(cl -> cl.getSimpleName().startsWith(PagesFactory.getInstance().getProduct().name())).findFirst().get()
						.getSimpleName();
			else
				SINSTANCE.targetPage = page_cls[0].getSimpleName();
			return SINSTANCE;
		}

		public PageObject getPage() {
			boolean is_expected_page = this.clickOnPathAndCheckPage();
			if (!is_expected_page) {
				Log.error("Failed while open page " + this.toString() + " try again");
				is_expected_page = this.clickOnPathAndCheckPage();
			}
			if (!is_expected_page)
				throw new RunTestException("Current url is " + this.driver.getDriver().getCurrentUrl() + " but expected for page " + this.targetPage);
			PageObject page = PagesFactory.getPages().openPage(this.targetPage);
			return page;
		}

		private boolean clickOnPathAndCheckPage() {
			String[] selects = path.split("_", 3);
			String slct;
			this.chooseMenu(selects[0]);

			if (PagesFactory.getInstance().currentPage.getClass().getSimpleName().equals(targetPage))
				return true;
			try {
				for (int i = 1; i < selects.length; i++) {
					// slct = selects[i].replaceAll("(([A-Z][a-z].){0,})([A-Z]{0,})([A-Z][a-z].)", "$1 $2 $3 $4").replaceAll(" ", " ").trim();
					slct = selects[i].replaceAll("(.)([A-Z]{0,})([A-Z][a-z].)", "$1 $2$3").replaceAll(" ", " ").trim();
					slct = slct.replaceAll("_", " ");
					// slct = selects[i].replaceAll("(([A-Z][a-z].){0,})([A-Z]{0,})([A-Z][a-z].)", "$1 $2 $3 $4").replaceAll(" ", " ").trim();
					slct = slct.replaceAll("  ", " ");
					this.findElement(side_menu, slct).click();
				}
			} catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
				return false;
			}
			String current_url = this.driver.getDriver().getCurrentUrl();
			boolean is_expected_page = current_url.toLowerCase().contains(this.targetPage.replace("Page", "").toLowerCase());
			if (!is_expected_page)
				Log.debug("Current url is " + current_url + " but expected for page" + this.targetPage
						+ "Check name of page class vs. url they should be exact after lowercase and removing Page word from the end of class name");
			return is_expected_page;
		}

		private void chooseMenu(String menu_name) {
			List<WebElement> elem = this.findElementIfExists(side_menu_class);
			Boolean is_page_in_same_menu = !elem.isEmpty() && elem.get(0).getAttribute("class").contains(menu_name);

			if (!is_page_in_same_menu) {
				this.findElement(side_menu, menu_name).click();
				PageObject transition_page = PagesFactory.getPages().getProductDefaultPage(menu_name, false, false);
				if (transition_page != null)
					transition_page.openTransitionPage();
			}
		}
	}
}
