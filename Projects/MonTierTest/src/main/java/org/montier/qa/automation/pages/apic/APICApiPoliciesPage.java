package org.montier.qa.automation.pages.apic;

import java.util.Arrays;

import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.montier.qa.automation.base.utils.Table.TableRow;
import org.openqa.selenium.WebElement;

public class APICApiPoliciesPage extends PageObject {
	public class ApiPoliciesfilters extends PageFilters {
		public FilterUtils TIME = new FilterUtils("Time");
		public FilterUtils DEVICE = new FilterUtils("Device");
		public FilterUtils DOMAIN = new FilterUtils("Domain");
		public FilterUtils CATALOG = new FilterUtils("Catalog");
		public FilterUtils SPACE = new FilterUtils("Space");
		public FilterUtils PRODUCT = new FilterUtils("Product");
		public FilterUtils PLAN = new FilterUtils("Plan");
		public FilterUtils API_NAME = new FilterUtils("API Name");
		public FilterUtils API_VERSION = new FilterUtils("API Version");
		public FilterUtils App_NAME = new FilterUtils("App Name");
		public FilterUtils POLICY_TITLE = new FilterUtils("Policy Title");
	}

	public static class PoliciesCols extends TableCols {
		public static Col API_NAME = new Col("API Name");
		public static Col API_VERSION = new Col("API Version");
		public static Col POLICY_TITLE = new Col("Policy Title");
		public static Col AVG = new Col("Avg (ms.)");
		public static Col MAX = new Col("Max (ms.)");
		public static Col MIN = new Col("Min (ms.)");
		public static Col CALLS = new Col("Calls");
		public static Col Percentile90 = new Col("90% Percentile");
		public static Col Percentile95 = new Col("95% Percentile");
		public static Col Percentile99 = new Col("99% Percentile");

		public PoliciesCols() {
			super();
		}

		@Override
		public void setColsIndices() {
			colsIndices.addAll(Arrays.asList(API_NAME, API_VERSION, POLICY_TITLE, AVG, MAX, MIN, CALLS, Percentile90,
					Percentile95, Percentile99));
		}
	}

	@Find(by = FBy.xpath, value = "//div[contains(@class, 'ApicApiPoliciesGrid')]")
	protected WebElement ApicPoliciesGrid;
	@Find(by = FBy.xpath, value = "//div[contains(@style, 'display: block;')]//child::a[1]")
	ByPath add_filter;

	public Table apiPoliciesTable;

	public APICApiPoliciesPage() {
		super();
	}

	@Override
	public ApiPoliciesfilters getPageFilters() {
		if (this.pageFilters == null)
			this.pageFilters = new ApiPoliciesfilters();
		return (ApiPoliciesfilters) this.pageFilters;

	}

	@Override
	public void initPageElement() {
		this.apiPoliciesTable = new Table(ApicPoliciesGrid, new PoliciesCols());

	}

	public void clickOnFilterTable(TableRow row, Col col) {
		String version = row.getValue(col);
		this.apiPoliciesTable.MouseHoverAndClick(row, col, add_filter.path);
		PageObject.getFilterElem().addFilterToCurrent(col.getName(), version);
		this.refresh();
	}
}
