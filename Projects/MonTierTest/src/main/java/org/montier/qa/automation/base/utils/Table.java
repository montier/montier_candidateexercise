package org.montier.qa.automation.base.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.utils.IConditions.BiCondition;
import org.montier.qa.automation.elements.TableElement;
import org.openqa.selenium.WebElement;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class Table {
	@AllArgsConstructor
	public static class Col {
		private @Getter String name;
		public static List<String> names = new ArrayList<>();

		public String getValue(TableRow row) {
			return row.getValue(this);
		}
	}

	public abstract static class TableCols {
		public ArrayList<Col> colsIndices = new ArrayList<>();

		protected TableCols() {
			setColsIndices();
		}

		protected TableCols(ArrayList<Col> cols_indices) {
			colsIndices = cols_indices;
		}

		public void setIndexes() {
			Field[] flds = this.getClass().getDeclaredFields();
			try {
				for (int i = 0; i < flds.length; i++)
					colsIndices.add(i, (Col) flds[i].get(this));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				throw new RunTestException();
			}
		}

		public void setColsIndices() {
			setIndexes();
		}

		public void removeCols(Col col) {
			this.colsIndices.remove(col);
		}
	}

	public static class TableRow {
		public static HashMap<Integer, HashMap<String, Integer>> cols = new HashMap<>();
		List<WebElement> cells;
		public int index;
		public int hash;

		public TableRow(int row, int hash, List<WebElement> ceils) {
			this.index = row;
			this.cells = ceils;
			this.hash = hash;
		}

		public TableRow() {
			this.index = -1;
		}

		public int getIndex(Col col) {
			return cols.get(hash).get(col.getName());
		}

		public WebElement getCeil(Col col) {
			return cells.get(getIndex(col));
		}

		public String getValue(Col col) {
			return cells.get(getIndex(col)).getText();
		}

		public void click() {
			this.cells.get(0).click();
		}

		public void click(Col col) {
			this.getCeil(col).click();
		}

		@Override
		public String toString() {
			String str = "";
			for (WebElement elem : cells)
				str += elem.getText() + ", ";
			return str;
		}
		/*
		 * public TableRow(List<WebElement> ceils, ArrayList<String> col_names) { Map<String, Field> fields =
		 * Arrays.asList(this.getClass().getDeclaredFields()).stream() .collect(Collectors.toMap(Field::getName, Function.identity())); for(int i=0;
		 * i<col_names.size(); i++) try { fields.get(col_names.get(i)).set(this, ceils.get(i)); } catch (IllegalArgumentException |
		 * IllegalAccessException e) { // TODO Auto-generated catch block e.printStackTrace(); } } public TableRow() {} public WebElement
		 * getCeil(String col_name) { Field[] fds = null; WebElement we = null; try { fds = this.getClass().getDeclaredFields(); we =
		 * (WebElement)(this.getClass().getDeclaredField(col_name).get(this)); } catch (IllegalArgumentException | IllegalAccessException |
		 * NoSuchFieldException | SecurityException e) { // TODO Auto-generated catch block e.printStackTrace(); } return we; }
		 */

	}

	public TableElement tableElmnt;
	private Integer numRows;
	TreeMap<Integer, TableRow> rows;
	private int hash;

	public Table(WebElement table_panel_elem, TableCols table_cols) {
		tableElmnt = new TableElement(table_panel_elem, table_cols.colsIndices);
		numRows = tableElmnt.numRows();
		rows = new TreeMap<>();
		HashMap<String, Integer> colss = new HashMap<>();
		hash = table_cols.hashCode();
		String col_names = "";
		for (int i = 0; i < table_cols.colsIndices.size(); i++) {
			colss.put(table_cols.colsIndices.get(i).getName(), i);
			col_names += table_cols.colsIndices.get(i).getName() + ", ";
		}
		Log.debug("Cols names: " + col_names);
		TableRow.cols.put(hash, colss);
	}

	public TableRow getRow(Integer i) {
		if (rows.get(i) == null)
			this.rows.put(i, new TableRow(i, hash, tableElmnt.getRowCeils(i + 1)));
		return rows.get(i);
	}

	/*
	 * private TableRow<T> getRow(List<WebElement> ceils) { TableRow<T> ins = null; ins = new TableRow<T>(ceils); return ins; }
	 */

	public Integer getNumRows() {
		return numRows;
	}

	public String getValueOf(int row_num, Col col) {
		return this.getRow(row_num).getValue(col);
	}

	public TableRow findRowSatisfies(BiCondition... conditions) {
		int i = 0;
		String ceil_text = null;
		Boolean res = true;
		while (i <= this.numRows) {
			i++;
			res = true;
			for (BiCondition cond : conditions) {
				ceil_text = this.getRow(i).getCeil((Col) cond.getObj()).getText();
				if (!cond.apply(ceil_text)) {
					res = false;
					break;
				}
			}
			if (res)
				break;
		}
		return res ? this.rows.get(i) : null;
	}

	public String getIter(int i, Object obj) {
		return this.getRow(i).getCeil((Col) obj).getText();
	}

	public TableRow findRowSatisfies(String find_what, boolean first_page, BiCondition... conditions) {
		int i = 0;
		String ceil_text = null;
		Boolean cond_satisfies = false;
		int num_rows = numRows <= this.tableElmnt.numRowsInPage ? numRows : (first_page ? this.tableElmnt.numRowsInPage : this.numRows);
		// while ((first_page && i < this.tableElmnt.numRowsInPage) || (!first_page && i < this.numRows)) {
		while (i < num_rows) {
			cond_satisfies = true;
			for (BiCondition cond : conditions) {
				Object obj = cond.getClass().getSimpleName().contains("WebElement") ? this.getRow(i).getCeil((Col) cond.getObj())
						: this.getRow(i).getCeil((Col) cond.getObj()).getText();
				if (!cond.apply(obj)) {
					cond_satisfies = false;
					break;
				} else if (find_what == "find_most")
					cond.compare_to_value = ceil_text;

			}
			if (cond_satisfies)
				if (find_what == "find_first")
					break;
				else if (find_what == "find_last" || find_what == "find_most")
					continue;
			i++;
		}
		return cond_satisfies ? this.rows.get(i) : null;
	}

	public TableRow getRowValueGreaterThan(Col col, int value) {
		// TODO: one function all methods/conditions
		int i = 0;
		WebElement ceil = null;
		while (i <= this.numRows) {
			i++;
			ceil = this.getRow(i).getCeil(col);
			if (!ceil.getText().equals("") && value <= Integer.parseInt(ceil.getText()))
				break;
		}
		return this.rows.get(i);
	}

	public TableRow getRowValueLowerThan(Col col, Float value) {
		// TODO: one function all methods/conditions
		int i = 0;
		WebElement ceil = null;
		while (i <= this.numRows) {
			i++;
			ceil = this.getRow(i).getCeil(col);
			if (!ceil.getText().equals("") && value >= Integer.parseInt(ceil.getText()))
				break;
		}
		return this.rows.get(i);
	}

	public TableRow getRowValueEqual(Col col, String value) {
		int num_rows = this.numRows;
		String ceil_text = null;
		int i;
		for (i = 0; i < num_rows; i++) {
			ceil_text = this.getRow(i).getCeil(col).getText();
			if (value.equals(ceil_text))
				break;
		}
		TableRow row = i >= num_rows ? null : this.getRow(i);
		return row;
	}

	public TableRow getRowValueContains(Col col, String value) {
		int num_rows = this.numRows;
		String ceil_text = null;
		int i;
		for (i = 0; i < num_rows; i++) {
			ceil_text = this.getRow(i).getCeil(col).getText();
			if (ceil_text.contains(value))
				break;
		}
		TableRow row = i >= num_rows ? null : this.getRow(i);
		return row;
	}

	public void MouseHoverAndClick(TableRow row, Col col, String hover_elem_xpath) {
		this.tableElmnt.MouseHoverAndClick(row.index + 1, row.getIndex(col) + 1, hover_elem_xpath);
	}

	public void mouseHover(TableRow row, Col col) {
		this.tableElmnt.mouseHoverOnCeil(row.index + 1, row.getIndex(col) + 1);
	}

}
