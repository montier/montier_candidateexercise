package org.montier.qa.automation.pages.apic;

import org.montier.qa.automation.pages.base.APICAbstractTransactionPage;
import org.montier.qa.automation.toolkit.elements.TransactionComponents.SideCalls;

public class APICTransactionSideCallsPage extends APICAbstractTransactionPage {
	public APICTransactionSideCallsPage() {
		super();
	}

	@Override
	public void initPageElement() {
		super.initPageElement();
		this.transactionComponents.sideCalls = (SideCalls) this.transactionComponents.getTabContent(SideCalls.class);
	}

	@Override
	public SideCalls getComponentPanel() {
		return this.transactionComponents.sideCalls;
	}

}
