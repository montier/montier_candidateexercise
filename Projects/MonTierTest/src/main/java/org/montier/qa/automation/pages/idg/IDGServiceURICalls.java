package org.montier.qa.automation.pages.idg;

import java.util.Arrays;

import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.PagesFactory;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.montier.qa.automation.base.utils.Table.TableRow;
import org.montier.qa.automation.pages.AlertAddPage;
import org.openqa.selenium.WebElement;

public class IDGServiceURICalls extends PageObject {

	public static class SrvcURICols extends TableCols {
		public static final Col DOMAIN = new Col("Domain");
		public static final Col URI = new Col("URI");
		public static final Col SERVICE = new Col("Service Name");
		public static final Col AVG_MS = new Col("Avg (ms.)");
		public static final Col MAX_MS = new Col("Max (ms.)");
		public static final Col MIN_MS = new Col("Min (ms.)");
		public static final Col CALLS = new Col("Calls");
		public static final Col PERCENTILE90 = new Col("90% Percentile");
		public static final Col PERCENTILE95 = new Col("95% Percentile");
		public static final Col PERCENTILE99 = new Col("99% Percentile");

		@Override
		public void setColsIndices() {
			this.colsIndices.addAll(Arrays.asList(DOMAIN, URI, SERVICE, AVG_MS, MAX_MS, MIN_MS, CALLS, PERCENTILE90, PERCENTILE95, PERCENTILE99));
		}
	}

	public class URICallsFilters extends PageFilters {
		public FilterUtils TIME = new FilterUtils("Time");
		public FilterUtils DEVICE = new FilterUtils("Device");
		public FilterUtils DOMAIN = new FilterUtils("Domain");
		public FilterUtils SERVICE = new FilterUtils("Service");
		public FilterUtils INURI = new FilterUtils("In URI");
	}

	@Find(by = FBy.xpath, value = "//table")
	WebElement table;
	@Find(by = FBy.xpath, value = "//a[contains(@href, 'alertAdd')]")
	ByPath add_alert;

	public Table serviceURICallsTable;

	public IDGServiceURICalls() {
		super();

	}

	@Override
	public void initPageElement() {
		this.serviceURICallsTable = new Table(table, new SrvcURICols());
	}

	@Override
	public URICallsFilters getPageFilters() {
		if (this.pageFilters == null)
			this.pageFilters = new URICallsFilters();
		return (URICallsFilters) this.pageFilters;
	}

	public AlertAddPage clickAddAlert(TableRow row, Col col) {
		String hover_elem_xpath = add_alert.path;
		this.serviceURICallsTable.MouseHoverAndClick(row, col, hover_elem_xpath);
		PagesFactory.getInstance();
		// driver.switchToSubWindow();
		this.switchToSubWindow(PagesFactory.getPages().getAlertAddPage());
		return PagesFactory.getPages().getAlertAddPage();
	}
}
