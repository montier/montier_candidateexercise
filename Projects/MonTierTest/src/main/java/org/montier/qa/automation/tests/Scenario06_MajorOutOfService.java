package org.montier.qa.automation.tests;

import java.util.Date;
import java.util.TreeMap;

import org.montier.qa.automation.base.FunctionalTest;
import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.utils.Table.TableRow;
import org.montier.qa.automation.elements.ChartLine;
import org.montier.qa.automation.elements.SideMenu.MenuSelect;
import org.montier.qa.automation.pages.RestartDataPage.RestartsCols;
import org.montier.qa.automation.pages.idg.IDGSystemOverviewPage.SystemErrorsCols;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.internal.collections.Pair;

public class Scenario06_MajorOutOfService extends FunctionalTest {

	String caues = "Domain Restart";
	String domain = "GovernmentA_Domain";
	TreeMap<String, ChartLine> lines = null;
	Pair<Date, Float> val = null;
	Pair<Date, Float> value = null;
	Date start_date = null;
	Date end_date = null;
	TableRow row = null;
	String error = "System battery failed.";
	String error2 = "Internal cooling fan has stopped.";
	String error3 = "System power supply #1 has failed";
	String error4 = "Power supply failure.";

	@Test
	public void MajorOutOfService() {
		Log.description("Test goal - Troubleshooting a major out-of-service issue." + "\n"
				+ "Test shows investigation of domain reload when seeing many services did not respond for a while, and after that everything went back to normal.");

		this.step("Check device level health in System Overview page", () -> {
			Log.instep("Open System Overview page and filter Time by Last 15 Minutes.");
			MenuSelect.Dashboards_Overview_SystemOverview.getPage();
			PageObject.getFilterElem().resetFilters(false, true);
			pages.idg.sysOverview.getPageFilters().TIME.applyFilter("Last 15 Minutes");
			pages.idg.sysOverview.refresh();
//			TODO: Need to fix the chart logics before uncomment this section
//			Log.instep("Assert SystemActivity chart has steady behavior (value always > 0)."); // TODO
//			pages.idg.sysOverview.SystemActivityChart.parseChartLines();
//			start_date = pages.idg.sysOverview.SystemActivityChart.x_start_date;
//			end_date = pages.idg.sysOverview.SystemActivityChart.x_end_date;
//			value = pages.idg.sysOverview.SystemActivityChart.getLowestValue("Transactions", start_date, end_date);
//			Assert.assertTrue(value.second() > 0,
//					"Expected for Transactions line in systemactivity chart to be always more than 0, but found " + value.second());

//			Log.instep("Assert DeviceCPU chart has steady behavior and values always less than 80%");
//			pages.idg.sysOverview.DeviceResourcesCpuChart.parseChartLines();
//			lines = pages.idg.sysOverview.DeviceResourcesCpuChart.lines;
//			for (String line : lines.keySet()) {
//				val = pages.idg.sysOverview.DeviceResourcesCpuChart.getHighestValue(line);
//				Assert.assertTrue(val.second() < 90);// there are more than 80%
//			}

//			Log.instep("Assert DeviceMemory chart has steady behavior and values always less then 70%");
			// pages.idg.sysOverview.DeviceResourcesMemoryChart.parseChartLines();
			// lines = pages.idg.sysOverview.DeviceResourcesMemoryChart.lines;
			// for (String line : lines.keySet()) {
			// val = pages.idg.sysOverview.DeviceResourcesMemoryChart.getHighestValue(line);
			// Assert.assertTrue(val.second() < 70);
			// }

			Log.instep("Assert System Errors not real errors*");
			if (pages.idg.sysOverview.SystemErrorsTable.getNumRows() != 0) {
				row = pages.idg.sysOverview.SystemErrorsTable.getRowValueEqual(SystemErrorsCols.MESSAGE, error);
				Assert.assertTrue(row.getCeil(SystemErrorsCols.MESSAGE).getText().equals(error));
				row = pages.idg.sysOverview.SystemErrorsTable.getRowValueEqual(SystemErrorsCols.MESSAGE, error2);
				Assert.assertTrue(row.getCeil(SystemErrorsCols.MESSAGE).getText().equals(error2));
				row = pages.idg.sysOverview.SystemErrorsTable.getRowValueEqual(SystemErrorsCols.MESSAGE, error3);
				Assert.assertTrue(row.getCeil(SystemErrorsCols.MESSAGE).getText().equals(error3));
				row = pages.idg.sysOverview.SystemErrorsTable.getRowValueEqual(SystemErrorsCols.MESSAGE, error4);
				Assert.assertTrue(row.getCeil(SystemErrorsCols.MESSAGE).getText().equals(error4));
			}
			pages.idg.sysOverview.getPageFilters().TIME.applyFilter("Last Hour");
			pages.idg.sysOverview.refresh();
		});

//		this.step("Check domain level health in RestartDashboards.", () -> {
//			Log.instep("Open Analytics → RestartDashboards page.");
		MenuSelect.Dashboards_Analytics_Restarts.getPage();
//			Log.instep("Assert there was a restart in RestartTimeline chart.");
//			pages.idg.restartData.TimelineChart.parseChartLines();
//			lines = pages.idg.restartData.TimelineChart.lines;
//			for (String line : lines.keySet()) {
//				val = pages.idg.restartData.TimelineChart.getLastValueHigherThan(line, (float) 0);
//				Assert.assertTrue(val.second() > 0);
//			}
//		});

		this.step("See details of restart Device and Domain Restarts table.", () -> {
			Log.instep("Assert restart Cause is Domain restart");
			TableRow row = pages.idg.restartData.RestartsTable.getRow(0);
			Assert.assertTrue(row.getCeil(RestartsCols.CAUSE).getText().equals(caues));
			Log.instep("Assert Domain name is GovernmentA_Domain.");
			Assert.assertTrue(row.getCeil(RestartsCols.DOMAIN).getText().equals(domain));
		});

		this.step("Determine restart cause", () -> {
			Log.instep("In Device and Domain Restarts table mouse hover on cause col");
			TableRow row = pages.idg.restartData.RestartsTable.getRow(0);
			String[] contect = pages.idg.restartData.mouseHoverGetText(row, RestartsCols.CAUSE).split("\n");
			Log.instep("Assert User Id is admin");
			Assert.assertEquals(contect[0], "User ID: admin");
			Log.instep("Assert User IP 172.77.77.5");
			Assert.assertEquals(contect[1], "User IP: 172.77.77.5");
			Log.instep("Assert Facility is saml-artificat");
			Assert.assertEquals(contect[2], "Facility: saml-artifact");

		});
	}
}
