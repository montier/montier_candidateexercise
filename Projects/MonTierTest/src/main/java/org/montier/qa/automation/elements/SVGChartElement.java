package org.montier.qa.automation.elements;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import org.montier.qa.automation.base.Element;
import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.RunTestException;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.internal.collections.Pair;

public abstract class SVGChartElement extends Element {
	@Find(by = FBy.xpath, value = ".//div[contains(@class, 'panel-body')]//span[contains(@class, 'gwt-InlineHTML')]//*[contains(@class,'fa-minus') or contains(@class, 'fa-square')]//following-sibling::span", init = true)
	ByPath chart_legend_names;
	@Find(by = FBy.xpath, value = "//div[contains(@class, 'panel-body')]//i[contains(@class, 'fa-minus')or contains(@class, 'fa-square')]//following-sibling::span[text()='$LEGENDNAME$: ']/parent::div/parent::div")
	ByPath hidden_chart_point_value;
	@Find(by = FBy.xpath, value = ".//*[@class='ytick'][$NUM$]/*")
	ByPath ytick;
	// @Find(by = FBy.tagName, value = "rect", init = true)
//	@Find(by = FBy.xpath, value = "//*[@class='subplot xy']//*[name()= 'rect']", init = true)
//	ByPath x_axis;
	@Find(by = FBy.className, value = "xtick", init = true)
	ByPath xtick;
	@Find(by = FBy.xpath, value = "//*[@class='NoDataMessage' or text()='No data to display'] ")
	ByPath no_data_to_display_msg;

	public TreeMap<String, ChartLine> lines;
	public String chartName;
	public WebElement chartElement;
	private WebElement xAxis;
	public TreeMap<String, WebElement> legend_names;
	public int chartKeys;
	public int width;
	public int height;
	protected Float y_unit_differece;
	public Float y0_height;
	protected Float x_unit_seconds_differece;
	public Date x_start_date;
	public Date x_end_date;

	public SVGChartElement(WebElement chart_elem) {
		super(chart_elem);
		if (isTheChartEmpty()) {
			Log.info("Chart has no data, no data to displat message appears");
			return;
		}
		this.chartName = chart_elem.getAttribute("class");
		Log.debug("init Chart " + chart_elem.getAttribute("class"));
		this.chartElement = chart_elem;
//		this.xAxis = this.findElement(x_axis);
//		this.width = this.xAxis.getSize().width;
//		this.height = this.xAxis.getSize().height;
	}

	public boolean isTheChartEmpty() {
		return this.findElementIfExists(no_data_to_display_msg).size() != 0;
	}

	public void parseChartLines() {
		this.initChartLegend();
		if (thereAreYValues()) {
			this.y0_height = this.ytickValues(1).second();
			this.y_unit_differece = this.calcYUnitDelta();
		}

		this.lines = new TreeMap<String, ChartLine>();
		for (String line_name : legend_names.keySet()) {
			ChartLine line = new ChartLine(line_name, this.chartElement);
			this.lines.put(line_name, line);

		}
		if (thereAreXValues())
			calulateX();
	}

	public ArrayList<Pair<Date, Float>> getLinePoints(String line_name) {
		ArrayList<Pair<Date, Float>> points = new ArrayList<Pair<Date, Float>>();
		ChartLine line = this.lines.get(line_name);
		Pair<Date, Float> point;
		for (Float x : line.getLinePoints().keySet()) {
			point = new Pair<Date, Float>(this.fromXLengthToDate(x),
					this.fromYHeightToValue(line.getLinePoints().get(x)));
			points.add(point);
		}
		return points;
	}

	private void initChartLegend() {
		this.legend_names = new TreeMap<String, WebElement>();
		List<WebElement> chart_legend_names = this.findElementIfExists(this.chart_legend_names);
		if (chart_legend_names.isEmpty())
			this.legend_names.put("general", null);
		for (WebElement legend : chart_legend_names) {
			this.legend_names.put(legend.getText(), legend);
		}
	}

	private boolean thereAreYValues() {
		return this.findElementIfExists(this.ytick, String.valueOf(1)).size() > 0;
	}

	private boolean thereAreXValues() {
		return this.findElementIfExists(this.xtick, String.valueOf(1)).size() > 0;
	}

	protected Float calcYUnitDelta() {
		Pair<Float, Float> tick1 = ytickValues(1);
		Pair<Float, Float> tick2 = ytickValues(2);
		Float value_diff = tick2.first() - tick1.first();
		Float pix_diff = tick1.second() - tick2.second();
		return value_diff / pix_diff;
	}

	public Float fromYHeightToValue(Float y_height) {
		return (this.y0_height - y_height) * this.y_unit_differece;
	}

	public Float fromYValueToHeight(Float y_value) {
		float max_y_value = this.y_unit_differece * this.y0_height;
		if (y_value >= max_y_value)
			throw new RunTestException(
					"Y value is bigger than y height max height is " + max_y_value + " and asking for " + y_value);
		return this.y0_height - (y_value / this.y_unit_differece);
	}

	private Pair<Float, Float> ytickValues(int tick_num) {
		WebElement ytick = this.findElement(this.ytick, String.valueOf(tick_num));
		Float value = Float.parseFloat(ytick.getText().replace("k", "000"));
		Float height = Float.parseFloat(ytick.getAttribute("transform").replaceAll("[^\\d.^\\,]", "").split(",")[1]);
		return new Pair<Float, Float>(value, height);
	}

	/*
	 * private Pair<Float, Float> getRangeX(Date x_start_date, long tunit_seconds) {
	 * long tunit_x = tunit_seconds / this.x_unit_seconds_differece; int x_point =
	 * this.clacXDelta(x_start_date); long end_range = Math.min(x_point+tunit_x,
	 * this.width); long start_range = Math.max(x_point+tunit_x, this.width); return
	 * new Pair(start_range, end_range); }
	 * 
	 * private int getStartRangeX(Date x_end_date, long tunit_seconds) { int tunit_x
	 * = Math.toIntExact(tunit_seconds / this.x_unit_seconds_differece); return
	 * this.clacXDelta(x_start_date) + tunit_x; }
	 */
	private void calulateX() {
		float line_end_x = this.lines.lastEntry().getValue().getLinePoints().lastKey();
		Date line_end_date = this.readValue(line_end_x, null).first();
		float line_start_x = this.lines.firstEntry().getValue().getLinePoints().firstKey();
		Date line_start_date = this.readValue(line_start_x, null).first();

		// Date line_start_x = this.readValue(, null).first();
		// Date line_end_x =
		// this.readValue(this.lines.lastEntry().getValue().linePoints.lastKey(),
		// null).first();
		this.x_unit_seconds_differece = this.calcSecondsDelta(line_start_date, line_end_date,
				line_end_x - line_start_x);
		Assert.assertTrue(this.x_unit_seconds_differece > 0, "X difference must have bigget than 0");
		x_start_date = new Date(line_start_date.getTime() - (long) (line_start_x * x_unit_seconds_differece * 1000));
		// x_start_date.setTime(line_start_date.getTime() - (long) (line_start_x *
		// x_unit_seconds_differece * 1000));
		// this.x_start_date = this.readValue(1, null).first();
		x_end_date = new Date(line_start_date.getTime() + (long) ((this.width - 1) * x_unit_seconds_differece * 1000));
		// this.x_end_date = this.readValue(this.width - 1, null).first();
		// this.x_unit_seconds_differece = this.calcSecondseDelta(this.width - 1);
	}

	protected Date fromXLengthToDate(Float x) {
		Date date = new Date();
		date.setTime(x_start_date.getTime() + (long) (x * x_unit_seconds_differece * 1000));
		return date;
	}

	protected Float fromDateToXLength(Date date) {
		long date_difference = date.getTime() - this.x_start_date.getTime();
		if (date_difference == 0)
			return (float) date_difference;
		float dif = ((date_difference / 1000) / x_unit_seconds_differece);
		return dif;
	}

	protected float clacXDelta(Date x_end_date) {
		long second_distance = (x_end_date.getTime() - this.x_start_date.getTime()) / 1000;
		return second_distance / this.x_unit_seconds_differece;
	}

	protected float calcSecondseDelta(int end_x) {
		Date x_end_date = this.readValue(end_x, null).first();
		float second_distance = (x_end_date.getTime() - this.x_start_date.getTime()) / 1000;
		float delta = second_distance / (this.width - 2);
		return delta;
	}

	protected float calcSecondsDelta(Date start_date, Date end_date, float length) {
		float second_distance = (end_date.getTime() - start_date.getTime()) / 1000;
		float delta = second_distance / (length);
		return delta;
	}

	protected Pair<Date, Float> pointFromPixToValues(Pair<Float, Float> point) {
		Date date = this.fromXLengthToDate(point.first());
		Float y = this.fromYHeightToValue(point.second());
		return new Pair<Date, Float>(date, y);
	}

	protected Pair<Date, Float> pointFromPixToValues(Point point) {
		return pointFromPixToValues(new Pair<Float, Float>((float) point.x, (float) point.y));
	}

	public Pair<Date, Float> readValue(float delta_x, String line_name) {
		String[] values = mouseHoverAndReadValues(delta_x);
		Date x_value = null;
		boolean values_exists = true;
		try {
			x_value = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(values[0]);
		} catch (ParseException e) {
			Log.debug("No date in values, try to mouse hover again. values are " + values);
			values_exists = false;
		}
		if (!values_exists) {
			values = mouseHoverAndReadValues(delta_x);
			try {
				x_value = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(values[0]);
			} catch (ParseException e) {
				Log.debug("No date in values values are " + values);
				e.printStackTrace();
				throw new RunTestException(e.getMessage());
			}
		}
		Float y_value = null;

		if (line_name != null) {
			for (String value : values)
				if (value.startsWith(line_name))
					y_value = Float.parseFloat(value.split(":")[1].replace(",", "").trim());
		}
		return new Pair<Date, Float>(x_value, y_value);
	}

	private String[] mouseHoverAndReadValues(float delta_x) {
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		this.mouseHoverX((int) delta_x, this.height);
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		WebElement points_data = null;
		points_data = this.findElement(this.hidden_chart_point_value, this.legend_names.firstKey());
		Assert.assertTrue(points_data != null && !points_data.getText().isEmpty(),
				"Unexpected empty point data when trying to read details tooltip of chart point");
		// Log.info("Mouse hover may no works");
		return points_data == null ? null : points_data.getText().split("\n");
	}

	public void mouseHoverX(int delta_x, int delta_y) {
		Log.debug("mouse hover on point " + delta_x + ", " + delta_y / 2 + ". xAxis length = " + this.width);
		// driver.moveToElement(this.xAxis, delta_x + 1, 100);
		driver.moveToElement(this.xAxis, delta_x, delta_y / 2);
		// TODO mouse Hover in external page (Driver Page??)
		/*
		 * Actions builder = new Actions(driver); builder.moveToElement(this.xAxis, x,
		 * 0); builder.perform();
		 */
	}

	public Pair<Date, Float> getHighestValue(String line_name) {
		// int end_x = this.clacXDelta(x_end_date);
		// Timestamp timestamp = new Timestamp(x_end_date.getTime());
		Point p = lines.get(line_name).getHighestPoint(1.0f, 0.0f);
		return this.readValue(p.getX(), line_name);
	}

	// public Pair<Date, Float> getHigherValueTimeRange(String line_name, Float
	// value, Date starting_date, long tunit_seconds){
	// Pair<Float, Float> range = this.getRangeX(starting_date, tunit_seconds);
	// lines.get(line_name).getHigherValueTimeRange(range, value);
	// }
}