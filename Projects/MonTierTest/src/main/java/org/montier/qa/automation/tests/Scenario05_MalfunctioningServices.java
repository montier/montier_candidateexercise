package org.montier.qa.automation.tests;

import org.montier.qa.automation.base.FunctionalTest;
import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.utils.Table.TableRow;
import org.montier.qa.automation.elements.SideMenu.MenuSelect;
import org.montier.qa.automation.pages.base.IDGAbstractTransactionPage.TransRawVal;
import org.montier.qa.automation.pages.idg.IDGCertificatesExpirationPage.ExpCertifCols;
import org.montier.qa.automation.pages.idg.IDGEarlyFailedPage.EarlyFailCols;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Scenario05_MalfunctioningServices extends FunctionalTest {
	String domain = "EarlyFailedTrans_Domain";
	String time = "Last Hour";
	String device = "BankB_Domain";
	int services_num;
	int num_rows;

	@Test
	public void MalfunctioningServices() {
		Log.description("Test goal - Troubleshooting malfunction service" + "\n"
				+ "Somatimes services seem to be completely out-of-service, and return errors only." + "\n"
				+ "As a DataPower administrator, you recive a complaint that a new service is malfunctioning and always returns errors.");

		this.step("Open Service Activity page and see there are transactions of that service with domain" + domain,
				() -> {
					MenuSelect.Dashboards_Analytics_ServiceActivity.getPage();
					Log.instep("Filter page for domain" + domain + " of last Hour");
					pages.idg.srvcActivityPage.getPageFilters().TIME.applyFilter(time);
					pages.idg.srvcActivityPage.getPageFilters().DOMAIN.applyFilter(domain);
//			pages.idg.srvcActivityPage.refresh();
				});

		this.step("Show that there are no transaction of that service.", () -> {
//			pages.idg.srvcActivityPage.transCountChart.parseChartLines();
//			services_num = pages.idg.srvcActivityPage.transCountChart.lines.keySet().size();
//			Assert.assertTrue(services_num == 2, "service number is not as expected, service number = " + services_num);

		});

		this.step("Ensure there are no failed object for " + domain, () -> {
			Log.instep("Open Failed Opjects Page");
			MenuSelect.Explore.getPage();
			pages.idg.srvcsConf.exploreTabs.selectTab("Failed Objects");
			num_rows = pages.idg.failObjectPage.downObjectsTable.getNumRows();
			Log.instep("Assert that domain doesn't have any failed objects");
			Assert.assertTrue(num_rows == 0, "Current domain have failed object not as expected");
			PageObject.getFilterElem().clearFilter("Domain");
			pages.idg.failObjectPage.refresh();
			Log.instep("Clear filter domain and assert there are some failed objects for others domains");
			num_rows = pages.idg.failObjectPage.downObjectsTable.getNumRows();
			Assert.assertTrue(num_rows != 0, "others domain doesn't have failed object as expected");

		});

		this.step("Ensure certificates doesn't expired for the domain", () -> {
			Log.instep("Open Expried Certificates Page");
			MenuSelect.Dashboards_Security_ExpiredCertificates.getPage();
			Log.instep("Assert certificate for domain " + domain + " are not about to expire");
			TableRow row_Expired = pages.idg.expiredCertifPage.expriedTable.getRowValueContains(ExpCertifCols.DETAILS,
					"EarlyFailedTrans");
			Assert.assertTrue(row_Expired == null,
					"In table Expired Certificates have an expired with domain : " + domain);

		});

		this.step("Check domain had early failing transactions.", () -> {
			Log.instep("Open Early Failing Page");
			MenuSelect.Investigate.getPage();
			pages.idg.trans.tabsElem.selectTab("Early Failing");
			Log.instep("Filter by domain" + domain);
			pages.idg.earlyFailedPage.getPageFilters().DOMAIN.applyFilter(domain);
			pages.idg.earlyFailedPage.refresh();
			Log.instep("Assert that have more then 10 failing request");
			Assert.assertTrue(pages.idg.earlyFailedPage.earlyFailTable.getNumRows() > 10,
					"Not enough transactions to work with");

		});

		this.step("Drill down to request EarlySSLFailures.HTTP-FSH", () -> {
			Log.instep("Click on Trans. ID");
			TableRow row = pages.idg.earlyFailedPage.earlyFailTable.getRowValueEqual(EarlyFailCols.FSH_PROXY,
					"EarlySSLFailures.HTTP-FSH");
			pages.idg.earlyFailedPage.clickTransID(row);
			Log.instep("Assert Status is ERROR");
			String status = pages.idg.transRawMessages.transDetails.getValueOf(TransRawVal.STATUS);
			Assert.assertEquals(pages.idg.transRawMessages.transDetails.getValueOf(TransRawVal.STATUS).trim(), "",
					"expected for status to be empty but found " + status);
			Log.instep("Assert Error Analysis");
			String error = pages.idg.transRawMessages.transactionAnalysisPanel.getErrorAnalysis();
			Assert.assertTrue(error.equals("No WS-Proxy service endpoints matched request."),
					"Error Analysis not equals as expected, Error is: " + error + " but expected for "
							+ "No WS-Proxy service endpoints matched request.");

		});

		this.step("Drill down to request EarlySSLFailures.SSL-PP", () -> {
			Log.instep("Return to Early Failing page and clear Trans. ID Filter");
			pages.idg.transRawMessages.tabsElem.selectTab("Early Failing");
			PageObject.getFilterElem().clearFilter("Trans. ID");
			pages.idg.earlyFailedPage.refresh();
			Log.instep("Click on Trans. ID");
			TableRow row = pages.idg.earlyFailedPage.earlyFailTable.getRowValueEqual(EarlyFailCols.FSH_PROXY,
					"EarlySSLFailures.SSL-PP");
			pages.idg.earlyFailedPage.clickTransID(row);
			String status = pages.idg.transRawMessages.transDetails.getValueOf(TransRawVal.STATUS);
			Assert.assertEquals(pages.idg.transRawMessages.transDetails.getValueOf(TransRawVal.STATUS).trim(), "",
					"Expected for status ERROR but found " + status);
			Log.instep("Assert Error Analysis");
			String error = pages.idg.transRawMessages.transactionAnalysisPanel.getErrorAnalysis();
			Assert.assertEquals(error,
					"Client Sends HTTP Request but HTTPS expected or Client did not send a certificate.");

		});

		this.step("Drill down to request WrongSSLVerFromClient.SSL-PP", () -> {
			Log.instep("Return to Early Failing page and clear Trans. ID Filter");
			pages.idg.transRawMessages.tabsElem.selectTab("Early Failing");
			PageObject.getFilterElem().clearFilter("Trans. ID");
			pages.idg.earlyFailedPage.refresh();
			Log.instep("Click on Trans. ID");
			TableRow row = pages.idg.earlyFailedPage.earlyFailTable.getRowValueEqual(EarlyFailCols.FSH_PROXY,
					"WrongSSLVerFromClient.SSL-PP");
			pages.idg.earlyFailedPage.clickTransID(row);
			Log.instep("Assert Error Analysis");
			String error = pages.idg.transRawMessages.transactionAnalysisPanel.getErrorAnalysis();
			Assert.assertTrue(error.equals("TLS 1.0 when only 1.1/1.2 allowed"),
					"Error Analysis not equals as expected, Error =" + error);
		});

	}

}
