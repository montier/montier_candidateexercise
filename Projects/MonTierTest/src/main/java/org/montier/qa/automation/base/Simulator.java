package org.montier.qa.automation.base;

import java.util.TreeMap;

import org.montier.qa.automation.base.testutils.connections.SSHConnect;
import org.montier.qa.automation.base.utils.FilesUtils;
import org.montier.qa.automation.base.utils.RunTestException;
import org.montier.qa.automation.elements.SideMenu.MenuSelect;
import org.montier.qa.automation.pages.MonitoredDevicesPage.GatewaysCols;
import org.testng.Assert;

import com.jcraft.jsch.JSchException;

public class Simulator {
	private static Simulator INSTANCE = null;
	SSHConnect sshConn;
	private TreeMap<String, String> envConf;
	public static String[] devicesList;

	private Simulator() {
		super();
		this.initEnvConf();
		sshConn = new SSHConnect(envConf.get("host"), Integer.parseInt(envConf.get("sshport")), envConf.get("user"), envConf.get("password"));
	}

	private void initEnvConf() {
		envConf = new TreeMap<String, String>();
		if (System.getProperty("env.conf_file") != null) {
			TreeMap<String, String> key_value = FilesUtils.readKeyValueFile(System.getProperty("env.conf_file"), false);
			envConf.putAll(key_value);
		} else {
			envConf.put("user", System.getProperty("env.user"));
			envConf.put("password", System.getProperty("env.pass"));
			envConf.put("host", System.getProperty("env.host"));
			envConf.put("sshport", System.getProperty("env.sshport"));
			envConf.put("uiport", System.getProperty("env.uiport"));
			envConf.put("uiuser", System.getProperty("env.uiuser"));
			envConf.put("uipassword", System.getProperty("env.uipassword"));
			envConf.put("numdevices", System.getProperty("env.numdevices"));
		}
		Log.info("Environment configuration details:");
		for (String cnf : envConf.keySet()) {
			Log.info("\t" + cnf + " = " + envConf.get(cnf));
		}
	}

	public static Simulator getInstance() {
		if (INSTANCE == null)
			INSTANCE = new Simulator();
		return INSTANCE;
	}

	public void initSimulator() throws JSchException {
		Log.debug("Init simulator by stop clean and rebuild");
		boolean exit_code;
		exit_code = this.stop();
		if (!exit_code) {
			Log.error("stop command failed");
			System.exit(1);
		}
		exit_code = this.clean();
		if (!exit_code) {
			Log.error("clean command failed");
			System.exit(1);
		}
		exit_code = this.build();
		if (!exit_code) {
			Log.error("build command failed");
			System.exit(1);
		}
		try {
			Thread.sleep(1000 * 60 * 10);
		} catch (Exception ee) {
			Log.error("Error while waiting after environment build");
			throw new RunTestException(ee);
		}
	}

	public String getConf(String key) {
		return envConf.get(key);
	}

	public boolean build() throws JSchException {
		int num_devices = Integer.parseInt(System.getProperty("numdevices"));
		String build_cmd = "sudo ~/msc-dpod-simulator/src/msc-dpod-simulator/dpodSimulator.sh -a build -r"
				+ (num_devices > 1 ? "-i " + num_devices : "");
		Log.debug("Build simulator by cmd - " + build_cmd);
		return sshConn.run(build_cmd);
	}

	public boolean stop() throws JSchException {
		String stop_cmd = "sudo ~/msc-dpod-simulator/src/msc-dpod-simulator/dpodSimulator.sh -a stop";
		Log.debug("Stop simulator by cmd - " + stop_cmd);
		return sshConn.run(stop_cmd);
	}

	public boolean clean() throws JSchException {
		String clean_cmd = "sudo ~/msc-dpod-simulator/src/msc-dpod-simulator/dpodSimulator.sh -a clean";
		Log.debug("Clean simulator by cmd - " + clean_cmd);
		return sshConn.run(clean_cmd);
	}

	public String[] getDevices() {
		int num_devices = Integer.parseInt(envConf.get("numdevices"));
		String[] devices = new String[num_devices];
		Log.debug("Open devices gateways page and check there are " + num_devices + " devices");
		MenuSelect.Manage_Devices_Gateways.getPage();
		PagesFactory.getInstance();
		Assert.assertTrue(PagesFactory.getPages().getGatewaysPage().wdpDevicesTable.getNumRows() == num_devices,
				"expected for num devices " + num_devices + ".");
		for (int i = 0; i < num_devices; i++)
			devices[i] = PagesFactory.getPages().getGatewaysPage().wdpDevicesTable.getValueOf(i, GatewaysCols.NAME);
		devicesList = devices;
		return devices;
	}
}
