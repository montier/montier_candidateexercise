package org.montier.qa.automation.pages.base;

import java.util.ArrayList;

import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.UsersRoles;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.elements.TabsPanelElement;
import org.montier.qa.automation.elements.TabsPanelElement.Tab;
import org.montier.qa.automation.pages.RoleViewPage.Permissions;
import org.montier.qa.automation.pages.idg.IDGEarlyFailedPage;
import org.montier.qa.automation.pages.idg.IDGTransactionsPage;
import org.openqa.selenium.WebElement;

public abstract class IDGAbstractInvestigatePage extends PageObject {

	// @Find(by = FBy.className, value = "InvestiagteMenu")
	@Find(by = FBy.xpath, value = "//div[contains(@class, 'InvestiagteMenu')]")
	WebElement investiagteMenu;

	public TabsPanelElement tabsElem;

	public IDGAbstractInvestigatePage() {
		super();
	}

	@Override
	public void initPageElement() {
		this.tabsElem = new TabsPanelElement(investiagteMenu, getTabs());
	}

	private Tab[] getTabs() {
		ArrayList<Tab> tabs = new ArrayList<Tab>();
		tabs.add(new Tab("Raw Messages", PageObject.class, !UsersRoles.getPermissionOfCurrentUser(Permissions.AllowRawMessages)));
		tabs.add(new Tab("Transactions", IDGTransactionsPage.class));
		tabs.add(new Tab("Early Failing", IDGEarlyFailedPage.class));
		tabs.add(new Tab("Extended Transactions", PageObject.class));
		tabs.add(new Tab("Payload Capture", PageObject.class, !UsersRoles.getPermissionOfCurrentUser(Permissions.managePayloadCapture)));
		Tab[] arr = new Tab[tabs.size()];
		return tabs.toArray(arr);

	}

}

// TODO: Need to add here a CONSTANTS of URL and PAGE_NAME and implement it in all the inheritors (see example in IDGTransactionsPage)
