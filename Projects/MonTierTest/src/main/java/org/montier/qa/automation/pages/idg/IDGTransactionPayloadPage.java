package org.montier.qa.automation.pages.idg;

import org.montier.qa.automation.pages.base.IDGAbstractTransactionPage;
import org.montier.qa.automation.toolkit.elements.TransactionComponents.Payload;

public class IDGTransactionPayloadPage extends IDGAbstractTransactionPage {

	public IDGTransactionPayloadPage() {
		super();
	}

	@Override
	public void initPageElement() {
		super.initPageElement();
		this.transactionComponents.payload = (Payload) this.transactionComponents.getTabContent(Payload.class);
	}

	@Override
	public Payload getComponentPanel() {
		return this.transactionComponents.payload;
	}
}
