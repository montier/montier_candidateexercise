package org.montier.qa.automation.pages;

import java.util.Arrays;

import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.PagesFactory;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.montier.qa.automation.base.utils.Table.TableRow;

public class UserViewPage extends PageObject {
	public static class RolesOfUserCols extends TableCols {
		public static final Col NAME = new Col("Name");
		public static final Col REMOVE = new Col("");

		public RolesOfUserCols() {
			super();
		}

		@Override
		public void setColsIndices() {
			colsIndices.addAll(Arrays.asList(NAME, REMOVE));
		}
	}

	@Find(by = FBy.className, value = "RolesOfUserTable")
	public ByPath user_roles_table;
	@Find(by = FBy.className, value = "GroupsOfUserTable")
	public ByPath user_groups_table;

	public String userName;

	public Table userRolesTable;
	public Table userGroupsTable;

	public UserViewPage() {
		super();
	}

	@Override
	public PageFilters getPageFilters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void initPageElement() {
		this.userRolesTable = new Table(this.findElement(user_roles_table), new RolesOfUserCols());
		this.userGroupsTable = new Table(this.findElement(user_groups_table), new RolesOfUserCols());
	}

	public String findUserRole() {
		return userRolesTable.getRow(0).getValue(RolesOfUserCols.NAME);
	}

	public String findUserGroups() {
		return userGroupsTable.getRow(0).getValue(RolesOfUserCols.NAME);
	}

	public RoleViewPage clickOnRole(String role) {
		TableRow row = userRolesTable.getRowValueContains(RolesOfUserCols.NAME, role);
		row.click(RolesOfUserCols.NAME);
		PagesFactory.getPages().roleViewPage.openPage();
		return PagesFactory.getPages().roleViewPage;
	}
}
