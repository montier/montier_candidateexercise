package org.montier.qa.automation.base;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Log {
	public static Logger log = null;// = LogManager.getLogger(Log.class);

	public Log() {

	}

	public static void newLogFile(String log_file_name) {
		if (log == null)
			System.setProperty("suiteName", log_file_name);
		else
			LogManager.shutdown();
		System.setProperty("logFileName", log_file_name);
		System.setProperty("log4j.configurationFile", "log4j2-2.xml");
		log = LogManager.getLogger(Log.class);
		Log.info("log file name = " + System.getProperty("logFileName"));
	}

	public static void startTestCase(String sTestCaseName) {
		log.info("\n\n************************************************                " + sTestCaseName
				+ "                ************************************************\n\n");
	}

	public static void endTestCase(String sTestCaseName) {
		Log.info("Test " + sTestCaseName + " Ended");
		log.info("\n\n==============================================            " + "-E---N---D-"
				+ "             ==============================================\n\n\n");
	}

	public static void failTest(String test_name, String log_file, long duration, String exception) {// , String notifications) {
		log.error(
				"\n\n\n*****************************************************************************************************************************\n"
						+ "*                                           Test Failed                                   \n" + "* Test: " + test_name
						+ "\n" + "* Duration: " + duration + "\n" + "* Log file: " + System.getProperty("logsDirectory") + "/"
						+ System.getProperty("suiteName") + "/" + log_file + "\n" + "* Screenshot file: " + System.getProperty("logsDirectory")
						+ "/screenshots/" + System.getProperty("logFileName") + ".png\n"
						+ "*****************************************************************************************************************************\n"
						// + "* Notifications: \n" + notifications + "\n"
						+ "=============================================================================================================================\n"
						+ "* Exception: \n" + exception + "\n"
						+ "=============================================================================================================================");
	}

	public static void info(String message) {
		log.info("\t\t\t" + message);
	}

	public static void step(String message) {
		log.info(message);
	}

	public static void instep(String message) {
		log.info("\t\t *" + message);
	}

	public static void description(String message) {
		log.info(message);
	}

	public static void warn(String message) {
		log.warn(message);
	}

	public static void error(String message) {
		log.error("\t\t\t" + message);
	}

	public static void fatal(String message) {
		log.fatal(message);
	}

	public static void debug(String message) {
		log.info("\t\t\t\t" + message);
	}
}