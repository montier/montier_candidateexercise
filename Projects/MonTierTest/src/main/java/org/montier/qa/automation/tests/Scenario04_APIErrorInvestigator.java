package org.montier.qa.automation.tests;

import java.util.Arrays;

import org.montier.qa.automation.base.FunctionalTest;
import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.UsersRoles;
import org.montier.qa.automation.base.UsersRoles.UserRole;
import org.montier.qa.automation.elements.SideMenu.MenuSelect;
import org.montier.qa.automation.elements.TopPanel;
import org.montier.qa.automation.elements.TopPanel.ProductType;
import org.montier.qa.automation.pages.RoleViewPage.Permissions;
import org.montier.qa.automation.pages.base.APICAbstractTransactionPage;
import org.montier.qa.automation.pages.base.IDGAbstractTransactionPage;
import org.montier.qa.automation.pages.idg.IDGServicePerDevicePage.DeviceTransCols;
import org.montier.qa.automation.toolkit.actions.Login;
import org.montier.qa.automation.toolkit.elements.TransactionsTable.TransCols;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Scenario04_APIErrorInvestigator extends FunctionalTest {

	String inves_user_name = "BankInvestigator";
	String device_name;
	String service_name;
	IDGAbstractTransactionPage idg_trans_page;
	APICAbstractTransactionPage apic_trans_page;

	@BeforeMethod
	public void before() {
		if (UsersRoles.getInstance().getUserRole(inves_user_name) == null)
			loadAndVerifyUserRole();
		this.step("Switch to user " + inves_user_name, () -> {
			if (UsersRoles.getCurrentUserName().equals(inves_user_name))
				Log.info("Already logged in");
			else
				Login.switchUser(inves_user_name, inves_user_name + "1@");
		});
	}

	private void loadAndVerifyUserRole() {
		this.step("Load roles of user " + inves_user_name, () -> {
			Log.instep("Go into Users page and load " + inves_user_name + " user role");
			UsersRoles.getInstance().addRole(inves_user_name);
			UserRole inve_role = UsersRoles.getInstance().getUserRole(inves_user_name);
			Log.instep("Verify user has no ability to see payload and raw Messages");
			Assert.assertFalse(inve_role.getPermission(Permissions.AllowPayload),
					"Expected for AllowPayload premission to be NO");
			Assert.assertFalse(inve_role.getPermission(Permissions.AllowRawMessages),
					"Expected for AllowRawMessages premission to be NO");
			Assert.assertFalse(inve_role.getPermission(Permissions.managePayloadCapture),
					"Expected for managePayloadCapture premission to be NO");
			Assert.assertFalse(inve_role.getPermission(Permissions.allowManageAPICPolicyVariablesCaptures),
					"Expected for allowManageAPICPolicyVariablesCaptures premission to be NO");
			Log.instep("Verify user allowed resources");

			Assert.assertNotNull(inve_role.getAllowedResources().get("Service"),
					"Service resources is unexpected null");
			Assert.assertTrue(Arrays.asList(inve_role.getAllowedResources().get("Service")).stream().allMatch(
					srvc -> "CreateAccount_MHXE.MPGW,CreateAccount_WHSE.WSP,AccountStatus_MHJV.MPGW,AccountStatus_WHSW.WSP"
							.contains(srvc)),
					"unexpected allowed service");
			Assert.assertNotNull(inve_role.getAllowedResources().get("Domain"), "Domain resources is unexpected null");
			Assert.assertTrue(Arrays.asList(inve_role.getAllowedResources().get("Domain")).stream()
					.allMatch(srvc -> "APIMgmt_A72F3635F3,BankA_Domain".contains(srvc)), "unexpected allowed domain");
//			Assert.assertTrue(Arrays.asList(inve_role.getAllowedResources().get("Catalog")).stream()
//					.allMatch(srvc -> "CatalogA,montiercatalog".contains(srvc)), "unexpected allowed service");

		});

	}

//Unexists: bankcatalog
	@Test
	public void IDG() {
		Log.description("Test goal - DPOD supports API transactions processed by API Connect within DataPower Gateway. "
				+ "DPOD is very useful for non-production environments by providing limited access transactional information for developers and service consumers. "
				+ "As a DataPower administrator, you may offload some of the investigation process to developers.");

		this.step("Check recent activity page", () -> {
			Log.instep("Change product to IDG");
			TopPanel.getInstance().selectProduct(ProductType.IDG);
			Log.instep("Assert user landing page is recentActiviy");
			pages.idg.recentActivity.openPage();
			Log.instep("Validate user DOMAIN filter contains just allowed services");
			Assert.assertTrue(pages.idg.recentActivity.validateFilterOptionsForUser());
			Log.instep("Validate recentActivity tablescontains allowed resources only");
			Assert.assertTrue(pages.idg.recentActivity.validateContentByUserPermission());
		});

		this.step("Enter Service Per Device page and check content", () -> {
			MenuSelect.Dashboards_Analytics_Service_perDevice.getPage();
			Log.instep("Filter Time by Last Hour");
			pages.idg.srvcPerDevice.getPageFilters().TIME.applyFilter("Last Hour");
			pages.idg.srvcPerDevice.refresh();
			Log.instep("Validate device transactions table contains row for each pair of device with allowed service");
			Assert.assertTrue(pages.idg.srvcPerDevice.validateServiceColContent());
		});
		this.step("Open error transactions on one of the devices", () -> {
			Log.instep("Filter Status by Error");
			pages.idg.srvcPerDevice.getPageFilters().STATUS.applyFilter("ERROR");
			pages.idg.srvcPerDevice.refresh();
			for (int i = 0; i < pages.idg.srvcPerDevice.deviceTransTable.getNumRows(); i++)
				Assert.assertTrue("	CreateAccount_MHXE.MPGW,CreateAccount_WHSE.WSP".contains(
						pages.idg.srvcPerDevice.deviceTransTable.getRow(i).getValue(DeviceTransCols.SERVICE)));
			Log.instep("In service per device page click on one of the services");
			pages.idg.srvcPerDevice.clickOnRow(0);
			// String device_service = pages.idg.srvcPerDevice.clickOnRow(0); // TODO:
			// change the name of the function? device_name =
			// device_service.split(",")[0]; service_name =
			// device_service.split(",")[1];
		});

		this.step("Validate transaction page according to ApiDeveloper user permission", () -> {
			Log.instep(
					"Validate that transactions page not contains RawMessages, PayloadCapture and ContextVariableCapture ans denied in user permission");
			Assert.assertNull(pages.idg.trans.tabsElem.selectTab("Raw Messages"), "Raw Messages tab unexpected exists");
			Assert.assertNull(pages.idg.trans.tabsElem.selectTab("Payload Capture"),
					"Raw Messages tab unexpected exists");
			Log.instep("Validate transactions table also not contains payload column");
			Assert.assertFalse(
					pages.idg.trans.transTable.table.tableElmnt.colNames.contains(TransCols.PAYLOAD.toString()));
		});

		this.step("Drill down to investigate failed transaction", () -> {
			Log.instep("click one of the transactions");
			pages.idg.trans.transTable.clickTransID(pages.idg.trans.transTable.table.getRow(0));
			idg_trans_page = (IDGAbstractTransactionPage) pages.currentPage;
			Log.instep("Validate transaction details"); // Status = ERROR Device = as we choosed step before Service =
														// as we choose step before
														// Operation = (question) clientIP = (question)
														// Log.instep("Validate there are no payload and
														// rawMessage tabs");
			Assert.assertNull(idg_trans_page.tabsElem.selectTab("Raw Messages"), "Raw Messages tab unexpected exists");
			Assert.assertNull(idg_trans_page.tabsElem.selectTab("Payload"), "Raw Messages tab unexpected exists");
			Log.instep("Validate ExtendedLatenct exists");
			idg_trans_page.tabsPanelElem.selectTab("Extended Latency");
		});

		this.step("extract failure reason from the limited transaction details", () -> {
			Log.instep("Assert error analysis contains HTTP.....");
			Assert.assertTrue(pages.idg.extendLatePage.transactionAnalysisPanel.getErrorAnalysis()
					.equals("HTTP 500 FAIL returned from backend"));
		});
		this.step("Check transactions table filters", () -> {
			PageObject.getFilterElem()
					.removeFilterFromCurrent(pages.idg.transRawMessages.getPageFilters().TIME.toString()); //
			// PageObject.getFilterElem().resetFilters(true, false);
			idg_trans_page.tabsElem.selectTab("Transactions");
			PageObject.getFilterElem().resetFilters(false, true);
			pages.idg.trans.getPageFilters().SERVICE.applyFilter("MakeAppointment_WSS.WSP");
			pages.idg.trans.refresh();
			Assert.assertTrue(pages.idg.trans.transTable.table.getNumRows() == 0,
					"unexpected rows for service MakeAppointment_WSS.WSP thats not allowed for user");
		});

	}

	@Test
	public void APIC() {
		Log.description("Test goal - DPOD supports API transactions processed by API Connect within DataPower Gateway. "
				+ "DPOD is very useful for non-production environments by providing limited access transactional information for developers and service consumers. "
				+ "As a DataPower administrator, you may offload some of the investigation process to developers.");

		this.step("Check recent activity page", () -> {
			Log.instep("Change product to APIC");
			TopPanel.getInstance().selectProduct(ProductType.APIC);

			Log.instep("Assert user landing page is recentActiviy");
			pages.apic.recentActivity.openPage();
			Log.instep("Validate user DOMAIN filter contains just allowed services");
			Assert.assertTrue(pages.apic.recentActivity.validateFilterOptionsForUser());
			Log.instep("Validate recentActivity tablescontains allowed resources only");
			Assert.assertTrue(pages.apic.recentActivity.validateContentByUserPermission());
		});

		this.step("Open transaction page and validate", () -> {
			MenuSelect.Investigate.getPage();
			Log.instep(
					"Validate that transactions page not contains RawMessages, PayloadCapture and ContextVariableCapture ans denied in user permission");
			Assert.assertNull(pages.apic.trans.tabsElem.selectTab("Raw Messages"),
					"Raw Messages tab unexpected exists");
			Assert.assertNull(pages.apic.trans.tabsElem.selectTab("Payload Capture"),
					"Raw Messages tab unexpected exists");
			Log.instep("Validate transactions table also not contains payload column");
			Assert.assertFalse(
					pages.apic.trans.transTable.table.tableElmnt.colNames.contains(TransCols.PAYLOAD.toString()));
			Log.instep("Validate transactions table content according to user permissions");
			pages.apic.trans.getPageFilters().CATALOG.applyFilter("bankcatalog");
			pages.apic.trans.refresh();
			Assert.assertTrue(pages.apic.trans.transTable.table.getNumRows() == 0,
					"unexpected rows for service bankcatalog thats not allowed for user");
			PageObject.getFilterElem().clearFilter(pages.apic.trans.getPageFilters().CATALOG.toString());
			pages.apic.trans.getPageFilters().DOMAIN.applyFilter("BankB_Domain");
			pages.apic.trans.refresh();
			Assert.assertTrue(pages.apic.trans.transTable.table.getNumRows() == 0,
					"unexpected rows for service bankcatalog thats not allowed for user");
			PageObject.getFilterElem().clearFilter(pages.apic.trans.getPageFilters().DOMAIN.toString());
		});
		this.step("Open error transactions", () -> {
			Log.instep("Filter Time by Last Hour and API name by accounts and status by ERROR");
			pages.apic.trans.getPageFilters().TIME.applyFilter("Last Hour");
			pages.apic.trans.getPageFilters().STATUS.applyFilter("ERROR");
			pages.apic.trans.getPageFilters().API_NAME.applyFilter("accounts");
			Log.instep("click one of the transactions");
			pages.apic.trans.refresh();
			Assert.assertNotEquals(pages.apic.trans.transTable.table.getNumRows(), 0, "Table has no rows");
			pages.apic.trans.transTable.clickTransID(pages.apic.trans.transTable.table.getRow(0));
		});

		this.step("Drill down to investigate failed transaction", () -> {
			apic_trans_page = (APICAbstractTransactionPage) pages.currentPage;
			Log.instep("Validate transaction details"); // Status = ERROR Device = as we choosed step before Service =
														// as we choose step
														// beforeOperation = (question) clientIP = (question)
														// Log.instep("Validate there are no
														// payload and rawMessage tabs");
			Assert.assertNull(apic_trans_page.tabsElem.selectTab("Raw Messages"), "Raw Messages tab unexpected exists");
			Assert.assertNull(apic_trans_page.tabsElem.selectTab("Payload"), "Raw Messages tab unexpected exists");
			Log.instep("Validate ExtendedLatenct exists");
			apic_trans_page.tabsPanelElem.selectTab("Extended Latency");
		});

		this.step("extract failure reason from the limited transaction details", () -> {
			Log.instep("Assert error analysis contains HTTP.....");
			Assert.assertTrue(pages.apic.extendLatePage.transactionAnalysisPanel.getErrorAnalysis()
					.contains("Rejected by filter; SOAP fault sent"));
			// .contains("HTTP/1.1 500 Internal Server Error: Request is missing a mandatory
			// field"));
		});
	}
}