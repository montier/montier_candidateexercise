package org.montier.qa.automation.base.utils;

public class RunTestException extends RuntimeException {
	private static final long serialVersionUID = -4344124741964202004L;

	public RunTestException() {
		// TODO Auto-generated constructor stub
	}

	public RunTestException(String message) {
		super(message);
	}

	public RunTestException(Throwable cause) {
		super(cause);
		cause.printStackTrace();
	}

	public RunTestException(String message, Throwable cause) {
		super(message, cause);
		cause.printStackTrace();
	}

	public RunTestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
