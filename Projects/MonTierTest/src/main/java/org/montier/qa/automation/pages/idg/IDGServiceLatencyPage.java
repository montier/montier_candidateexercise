package org.montier.qa.automation.pages.idg;

import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.elements.SVGLineChartElement;
import org.openqa.selenium.WebElement;

public class IDGServiceLatencyPage extends PageObject {
	public class LatencyFilters extends PageFilters {
		public FilterUtils TIME = new FilterUtils("Time");
		public FilterUtils DEVICE = new FilterUtils("Device");
		public FilterUtils DOMAIN = new FilterUtils("Domain");
		public FilterUtils SERVICE = new FilterUtils("Service");
	}

	@Find(by = FBy.className, value = "ServiceTotalLatencyChart", init = true)
	WebElement serviceTotalLatencyChart;
	@Find(by = FBy.className, value = "ServiceBELatencyChart", init = true)
	WebElement serviceBELatencyChart;
	@Find(by = FBy.className, value = "ServiceDPRequestLatencyChart", init = true)
	WebElement serviceDPRequestLatencyChart;

	public SVGLineChartElement totalLatencyChart;
	public SVGLineChartElement BELatencyChart;
	public SVGLineChartElement requestLatencyChart;

	public IDGServiceLatencyPage() {
		super();
	}

	@Override
	public void refresh() {
		super.refresh();
		this.initCharts();
	}

	@Override
	public void initPageElement() {//
	}

	public void initCharts() {
		this.totalLatencyChart = new SVGLineChartElement(serviceTotalLatencyChart);
		this.BELatencyChart = new SVGLineChartElement(serviceBELatencyChart);
		this.requestLatencyChart = new SVGLineChartElement(serviceDPRequestLatencyChart);
	}

	@Override
	public LatencyFilters getPageFilters() {
		if (this.pageFilters == null)
			this.pageFilters = new LatencyFilters();
		return (LatencyFilters) this.pageFilters;
	}
}
