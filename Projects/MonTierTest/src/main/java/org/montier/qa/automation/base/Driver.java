package org.montier.qa.automation.base;

import static io.github.bonigarcia.wdm.DriverManagerType.CHROME;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.RunTestException;
import org.montier.qa.automation.elements.TopPanel;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Driver {
	private static Driver INSTANCE;
	private WebDriver driver;
	private String parentWindowHandler;
	private Actions action;
	private JavascriptExecutor jse;

	private Driver(String browser_type) {
		driver = initDriver(browser_type);
		parentWindowHandler = driver.getWindowHandle();
		action = new Actions(driver);
		jse = (JavascriptExecutor) driver;
	}

	public static Driver getInstance(String browser_type) {
		if (INSTANCE == null)
			INSTANCE = new Driver(browser_type);
		return INSTANCE;
	}

	public static Driver getInstance() {
		if (INSTANCE == null)
			throw new RunTestException("Cannot call driver instance at first time without browser_type");
		return INSTANCE;
	}

	public WebDriver getDriver() {
		return driver;
	}

	private WebDriver initDriver(String browser_type) {
		if (driver != null)
			return driver;
		ChromeOptions options = null;

		switch (browser_type) {
		case "chrome": {
			WebDriverManager.getInstance(CHROME).setup();
			options = new ChromeOptions();
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-gpu");
			// Remove comment to run with no UI
//			options.addArguments("--headless");
			options.addArguments("--disable-dev-shm-usage");
			options.addArguments("--ignore-certificate-errors");

			if (System.getProperty("is_dev_mode") != null && !System.getProperty("is_dev_mode").equals("true")) {
				options.addArguments("--headless");
				options.addArguments("--disable-dev-shm-usage");
			}
		}
		}
		driver = new ChromeDriver(options);

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		return driver;
	}

	public void openUrl(String url) {
		driver.get(url);
	}

	public void moveToElement(WebElement element, int x, int y) {
		if (element == null)
			throw new RunTestException("element unexpected null");
		Log.info("mouse hover on point " + x + ", " + y);
		Actions builder = new Actions(driver);

		builder.moveToElement(element, x, y).perform();

	}

	public void switchToSubWindow() {
		Set<String> handles = driver.getWindowHandles(); // get all window handles
		String sub_hndl = handles.stream().filter(hndl -> !hndl.equals(parentWindowHandler)).findFirst().get();
		driver.switchTo().window(sub_hndl); // switch to popup window
	}

	public void switchToParentHandler() {
		if (driver.getWindowHandle().equals(parentWindowHandler))
			return;
		Log.debug("Close current window and switch to parent window handler");
		driver.close();
		driver.switchTo().window(parentWindowHandler);
	}

	public File takeScreenshot(String target_file) {
		File src = null;
		// Take screenshot and store as a file format
		src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		// now copy the screenshot to desired location using copyFile //method
		File trgt = new File(target_file);
		trgt.setWritable(true);
		try {
			FileUtils.copyFile(src, trgt);
		} catch (IOException e) {
			Log.error("Failed while copy screenshot");
			e.printStackTrace();
		}
		return trgt;
	}

	public List<WebElement> findElements(ByPath by_path, SearchContext search) {
		List<WebElement> elements = null;
		try {
			Method m = By.class.getDeclaredMethod(by_path.byMethodName, String.class);
			elements = search.findElements((By) m.invoke(search, by_path.path));
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			Log.error("ERROR: Can't invoke BY method " + by_path.byMethodName + "for path " + by_path.path);
			throw new RunTestException(e);
		} catch (StaleElementReferenceException e) {
			Log.error(
					"EXCEPTION while find element, base element or driver not attached to the page.\n possible problems: \n - you access thru page that is no current page\n - page not refreshed when need (infrastracture problem)");
			// e.printStackTrace();
			throw new RunTestException(e);
		}
		return elements;
	}

	public void scrollPageEnd() {
		jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	public void scrollPageStart() {
		jse.executeScript("window.scrollTo(0, -document.body.scrollHeight);");
	}

	public void jsonChartActivte() {
		jse.executeScript("org.montier.common.client.standard.nativejs.QaHelper.setQaMode(true);");

		TopPanel top_panel = new TopPanel();
		top_panel.reloadPage();
	}

}
