package org.montier.qa.automation.toolkit.actions;

import java.util.List;

import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PagesFactory;
import org.montier.qa.automation.base.utils.RunTestException;
import org.montier.qa.automation.base.utils.Table.TableRow;
import org.montier.qa.automation.elements.FadedMessageElement.Message;
import org.montier.qa.automation.elements.FadedMessageElement.MessageType;
import org.montier.qa.automation.elements.SideMenu.MenuSelect;
import org.montier.qa.automation.pages.AlertAddPage;
import org.montier.qa.automation.pages.AlertsSetupPage;
import org.montier.qa.automation.pages.AlertsSetupPage.AlertCols;
import org.testng.Assert;

public class AlertActions {
	public static void AddAlert(AlertAddPage addAlertPage, String alert_message_name) {
		// addAlertPage.addAlert();
		// Message alert_message = addAlertPage.fadedMessages.getMessage(MessageType.SUCCESS);
		addAlertPage.addAlert();
		List<Message> alert_messages = addAlertPage.fadedMessages.getAllMessages();
		Assert.assertEquals(alert_messages.size(), 1, "Expected for one success message but found more than 1");
		Message alert_message = alert_messages.get(0);
		Assert.assertEquals(alert_message.getType(), MessageType.SUCCESS, "Alert unexpected didnt added with message " + alert_message.getText());

		Assert.assertTrue(alert_message.getText().contains("Alert added successfully, alert's name is " + alert_message_name),
				"Alert message is " + alert_message.getText() + " instead of " + alert_message_name);
		String alert_name = alert_message.getText().split("name is")[1].trim();
		Log.info("Alert added with name " + alert_name);
	}

	public static void deleteAlert(String alert_name, boolean fail_no_exists) {
		AlertsSetupPage alertsSetup = (AlertsSetupPage) MenuSelect.Reports_Alerts_Alerts.getPage();
		Log.instep("Check alert exists for service");
		TableRow row_alert = alertsSetup.alertsSetupTable.getRowValueContains(AlertCols.NAME, alert_name);
		try {
			String name = row_alert.getValue(AlertCols.NAME);
			row_alert.getCeil(AlertCols.NAME).click();
			Log.info("Found alert " + name);

		} catch (Exception e) {
			if (fail_no_exists)
				throw new RunTestException("No alert matching for the service");
			else
				Log.debug("No alert name " + alert_name);
			return;
		}
		PagesFactory.getPages().alertViewPage.openPage();
		Log.instep("Delete alert");
		PagesFactory.getPages().alertViewPage.deleteAlert();
	}
}
