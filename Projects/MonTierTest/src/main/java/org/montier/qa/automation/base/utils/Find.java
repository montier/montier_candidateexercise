package org.montier.qa.automation.base.utils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.TYPE })
public @interface Find {

	public enum FBy {
		id, linkText, partialLinkText, xpath, name, tagName, className, cssSelector
	}

	FBy by();

	String value();

	boolean init() default false;

	boolean from_root() default false;
}
