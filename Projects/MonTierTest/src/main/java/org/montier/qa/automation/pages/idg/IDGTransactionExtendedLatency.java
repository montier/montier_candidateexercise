package org.montier.qa.automation.pages.idg;

import org.montier.qa.automation.pages.base.IDGAbstractTransactionPage;
import org.montier.qa.automation.toolkit.elements.TransactionComponents.ExtendedLatency;

public class IDGTransactionExtendedLatency extends IDGAbstractTransactionPage {

	public IDGTransactionExtendedLatency() {
		super();
	}

	@Override
	public void initPageElement() {
		super.initPageElement();
		this.transactionComponents.extendedLatency = (ExtendedLatency) this.transactionComponents.getTabContent(ExtendedLatency.class);
	}

	@Override
	public ExtendedLatency getComponentPanel() {
		return this.transactionComponents.extendedLatency;
	}
}
