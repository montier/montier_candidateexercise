package org.montier.qa.automation.elements;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.Assert;
import org.montier.qa.automation.base.Element;
import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.PagesFactory;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.pages.idg.IDGTransactionsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import lombok.AllArgsConstructor;
import lombok.Setter;

public class TabsPanelElement extends Element {
	@AllArgsConstructor
	public static class Tab {
		String tabText;
		Class<?> targetPage;
		Boolean hide;

		public Tab(String tabText, Class<?> targetPage) {
			this.tabText = tabText;
			this.targetPage = targetPage;
			hide = false;
		}
	}

	@Find(by = FBy.xpath, value = "//*[text()='$TABNAME$']")
	ByPath tab;
	@Find(by = FBy.xpath, value = "//*[@style='display: none;']")
	ByPath not_desplayed1;
	@Find(by = FBy.xpath, value = "//*[@style='display: none;']//*[text()]")
	ByPath not_desplayed2;

	@Setter
	String pageArgs;

	List<Tab> tabs = new ArrayList<Tab>();

	public TabsPanelElement(WebElement panel_elem, Tab... tabs) {
		super(panel_elem);
		String not_desplayed_text = getNotDesplayedText();
		for (Tab tab : tabs) {
			if (tab.hide) {
				Assert.assertTrue("Tab " + tab.tabText + " unexpected displayed",
						not_desplayed_text.contains(tab.tabText));
			} else
				this.tabs.add(tab);
		}
	}

	private String getNotDesplayedText() {
		String text = "";
		for (WebElement elem : this.findElementIfExists(not_desplayed1))
			text += " " + elem.getAttribute("text");

		for (WebElement elem : this.findElementIfExists(not_desplayed2))
			text += " " + elem.getAttribute("text");

		return text;
	}

	public PageObject selectTab(String tab_text) {
		Log.info("Select tab " + tab_text);
		Tab selected = null;
		try {
			selected = this.tabs.stream().filter(tab -> tab.tabText.equals(tab_text)).findFirst().get();
		} catch (NoSuchElementException e) {
			Log.info(tab_text + " is not a valid tab name");
			return null;
		}
		this.findElement(tab, selected.tabText).click();
		PagesFactory.getPages().openPage(selected.targetPage.getSimpleName(), pageArgs);
		return PagesFactory.getInstance().currentPage;
	}

	public void moveToTab(String tab_text) {
		Log.info("Select tab " + tab_text);
		driver.getDriver().findElement(By.linkText(tab_text)).click();
		if (!driver.getDriver().getCurrentUrl().contains(IDGTransactionsPage.URL))
			Log.info("The navigation to " + tab_text + " didn't succeed");
	}

}
