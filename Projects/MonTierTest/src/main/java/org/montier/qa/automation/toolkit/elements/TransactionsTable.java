package org.montier.qa.automation.toolkit.elements;

import org.montier.qa.automation.base.Driver;
import org.montier.qa.automation.base.Element;
import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PagesFactory;
import org.montier.qa.automation.base.UsersRoles;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.IConditions;
import org.montier.qa.automation.base.utils.IConditions.BiCondition;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.montier.qa.automation.base.utils.Table.TableRow;
import org.montier.qa.automation.pages.RoleViewPage.Permissions;
import org.openqa.selenium.WebElement;

public class TransactionsTable extends Element {
	public abstract static class TransCols extends TableCols {
		public static final Col TIME = new Col("Time");
		public static final Col DEVICE = new Col("Device");
		public static final Col STATUS = new Col("Status");
		public static final Col TRANS_ID = new Col("Trans. ID");
		public static final Col CLIENT_IP = new Col("Client IP");
		public static final Col GL_TRANS_ID = new Col("Gl. Trans. ID");
		public static final Col ELAPSED = new Col("Elapsed (ms)");
		public static final Col PAYLOAD = new Col("Payload");

		public TransCols() {
			super();
		}
	}

	@Find(by = FBy.className, value = "TransactionPayloadLink")
	ByPath payload_alem;
	@Find(by = FBy.xpath, value = "//div[contains(@class, 'TransactionsGrid')]")
	protected WebElement transactions_grid;
	public Table table;

	public TransactionsTable(TableCols table_cols) {
		super();
		if (!UsersRoles.getPermissionOfCurrentUser(Permissions.AllowPayload))
			table_cols.removeCols(TransCols.PAYLOAD);
		this.table = new Table(transactions_grid, table_cols);
	}

	public void clickTransID(TableRow row) {
		String trans_id_text = row.getCeil(TransCols.TRANS_ID).getText();
		Log.info("Click on Trnas.ID " + trans_id_text);
		row.getCeil(TransCols.TRANS_ID).click();
		if (UsersRoles.getPermissionOfCurrentUser(Permissions.AllowRawMessages))
			PagesFactory.getPages().getTransRawMessagesPage().openPage(trans_id_text);
		else {
			String current_url = Driver.getInstance().getDriver().getCurrentUrl();
			if (current_url.toLowerCase().contains("extendedlatency"))
				PagesFactory.getPages().getExtendLatePage().openPage(trans_id_text);
			else if (current_url.toLowerCase().contains("sidecall"))
				PagesFactory.getPages().getSideCallsPage().openPage(trans_id_text);
			else if (UsersRoles.getPermissionOfCurrentUser(Permissions.AllowPayload) && current_url.toLowerCase().contains("payload"))
				PagesFactory.getPages().getTransPayload().openPage(trans_id_text);

		}
	}

	public TableRow findRowWithPayload(BiCondition... conditions) {
		BiCondition[] conds = new BiCondition[conditions.length + 1];
		for (int i = 0; i < conditions.length; i++)
			conds[i] = conditions[i];
		// conds[conditions.length] = IConditions.isElementContainsElem(TransCols.PAYLOAD, payload_alem);
		conds[conditions.length] = IConditions.getWebElementConstainsAttributeValue(TransCols.PAYLOAD, "class", payload_alem.path);
		return this.table.findRowSatisfies("find_first", true, conds);
	}

	public ByPath getPayloadPath() {
		return payload_alem;
	}
}
