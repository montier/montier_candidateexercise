package org.montier.qa.automation.elements;

import java.util.ArrayList;
import java.util.Date;

import org.openqa.selenium.WebElement;
import org.testng.internal.collections.Pair;

public class SVGTimeLineChartElement extends SVGChartElement {
	/**
	 * This chart relates to charts with x as time line and Y is true or flase (no Y ticks). For Example - Restarts chart
	 */
	public SVGTimeLineChartElement(WebElement chart_elem) {
		super(chart_elem);
		this.y0_height = (float) (this.height);
		this.y_unit_differece = 1 / y0_height;
	}

	public Pair<Date, Float> getLastValueHigherThan(String line_name, Float value) {
		Float y_pix = this.fromYValueToHeight(value);
		Pair<Float, Float> higher_point = this.lines.get(line_name).getLastValueHigherThan(y_pix);
		return pointFromPixToValues(higher_point);
	}

	public ArrayList<Pair<Date, Float>> getTickPoints(String line_name) {
		return this.getLinePoints(line_name);
	}

	@Override
	public void parseChartLines() {
		super.parseChartLines();
		this.y0_height += this.lines.firstEntry().getValue().getLinePoints().firstEntry().getValue();
	}

}
