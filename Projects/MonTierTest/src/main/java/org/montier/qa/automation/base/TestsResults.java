package org.montier.qa.automation.base;

import java.util.HashMap;

import org.testng.ITestResult;

import lombok.Getter;

public class TestsResults {
	public static class TestResult {
		ITestResult testResult;
		String testName;
		String failMethod;
		String endStatus;
		String exceptionType;
		String logName;
		String screenshotName;

		public TestResult(String test_name) {
			testName = test_name;
		}

	}

	private static TestsResults INSTANCE;
	private HashMap<String, TestResult> results;
	private @Getter TestResult currentTest;

	private TestsResults() {
	}

	public static TestsResults getInstance() {
		if (INSTANCE == null)
			return new TestsResults();
		return INSTANCE;
	}

	public void addTestName(String test_name) {
		this.results.put(test_name, new TestResult(test_name));
	}

}
