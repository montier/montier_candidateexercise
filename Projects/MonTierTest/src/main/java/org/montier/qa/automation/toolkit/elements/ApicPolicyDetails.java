package org.montier.qa.automation.toolkit.elements;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeMap;

import org.montier.qa.automation.base.Element;
import org.montier.qa.automation.base.testutils.TestUtils.StringUtils;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.montier.qa.automation.base.utils.Table.TableRow;
import org.montier.qa.automation.elements.KeyValuePanelElement;
import org.montier.qa.automation.toolkit.elements.RequestFlowElement.ApicPolicy;
import org.montier.qa.automation.toolkit.elements.RequestFlowElement.PoliciesList;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class ApicPolicyDetails extends Element {
	@AllArgsConstructor
	public static enum PolicyKeys {
		POLICY_TITLE("Policy Title"), POLICY_NAME("Policy Name"), POLICY_TYPE("Policy Type"),
		EXECUTION_TIME_MS("Execution Time (ms.)"), SIDE_CALLS("Side Calls"), POLICY_VARIABLES("Policy Variables");

		private final @Getter String stringValue;

		@Override
		public String toString() {
			return stringValue;
		}
	}

	public static class SideCallsPolicyCols extends TableCols {
		public static Col TYPE = new Col("Type");
		public static Col STATUS = new Col("Status");
		public static Col ELAPSED_MS = new Col("Elapsed (ms.)");
		public static Col DETAILS = new Col("Details");

		public SideCallsPolicyCols() {
			super();
		}

		@Override
		public void setColsIndices() {
			colsIndices.addAll(Arrays.asList(TYPE, STATUS, ELAPSED_MS, DETAILS));
		}
	}

	public static class ContextPolicyCols extends TableCols {
		public static Col NAME = new Col("Name");
		public static Col VALUE = new Col("Value");

		public ContextPolicyCols() {
			super();
		}

		@Override
		public void setColsIndices() {
			colsIndices.addAll(Arrays.asList(NAME, VALUE));
		}
	}

	@Find(by = FBy.xpath, value = "//div[contains(@class, 'modal') and @role='dialog']//div[contains(@class,'modal-body')]/*[1]")
	ByPath policies_list;
	@Find(by = FBy.xpath, value = "//table")
	ByPath sidecalls_table;
	@Find(by = FBy.xpath, value = "//table")
	ByPath context_table;
	@Find(by = FBy.xpath, value = "//div[contains(@class,'modal-body')]//child::label[text()='$KEY$']//following-sibling::div")
	ByPath details_panel_content;
	@Find(by = FBy.xpath, value = "//button[@class='close']")
	ByPath close_button;
	@Find(by = FBy.xpath, value = ".//div[contains(text(),'No Data')]")
	ByPath no_data;

	private KeyValuePanelElement<PolicyKeys> currentPolicyDetails;
	private @Getter ApicPolicy currentPolicy;
	private PoliciesList policies;

	public ApicPolicyDetails(WebElement base_elem) {
		super(base_elem);
		this.policies = null;
		this.currentPolicyDetails = null;
		// this.callsTable = new Table(this.findElement(sidecalls_table), new
		// SideCallsPolicyCols());//if exist
		// this.contextTable = new Table(this.findElement(context_table), new
		// ContextPolicyCols());
	}

	public void open(String policy_name, PoliciesList flow_policies) {
		setKeyValues();
		Assert.assertEquals(policy_name, currentPolicyDetails.getValueOf(PolicyKeys.POLICY_NAME));
		if (this.policies == null)
			this.policies = new PoliciesList(this.findElement(policies_list), flow_policies);
		currentPolicy = this.policies.getUserPolicies().get(policy_name);
		fillCurrentPolicyDetails(this.getCurrentPolicy());
	}

	public void close() {
		this.findElement(close_button).click();
	}

	public void refresh() {
		this.setKeyValues();
	}

	public void validateTransactionPolicies(LinkedHashMap<String, ApicPolicy> validate_list,
			String... last_policy_name) {
		String current_policy_name = this.currentPolicyDetails.getValueOf(PolicyKeys.POLICY_NAME);
		Iterator<String> current_policy_iter = this.policies.getUserPolicies().keySet().iterator();
		Iterator<String> validate_policy_iter = validate_list.keySet().iterator();
		ApicPolicy current = this.policies.getUserPolicies().get(current_policy_iter.next());
		ApicPolicy validate = validate_list.get(validate_policy_iter.next());

		while (current_policy_iter.hasNext()) {
			current = this.policies.getUserPolicies().get(current_policy_iter.next());
			validate = validate_list.get(validate_policy_iter.next());
			this.clickOnPolicy(current, true);
			Assert.assertTrue(current.validateData(validate),
					"Expected for " + validate.toString() + " but found " + current.toString());
		}
		if (validate_policy_iter.hasNext())
			Assert.assertTrue(last_policy_name.length > 0 && validate.getName().equals(last_policy_name[0]),
					"Expected for transaction to end with apic name " + last_policy_name[0]
							+ " but actually finished in " + validate.getName());

		this.policies.getUserPolicies().get(current_policy_name).getPolicyElement().baseElement.click();
	}

	public void clickOnPolicy(ApicPolicy current, boolean add_details) {
		current.getPolicyElement().click();
		this.refresh();
		String currentMS = String.valueOf(StringUtils.parseMS(current.getPolicyElement().getMs()));

		if (this.currentPolicyDetails.getValueOf(PolicyKeys.EXECUTION_TIME_MS).contains("estimate"))
			currentMS = currentMS + " (estimate)";
		Assert.assertEquals(this.currentPolicyDetails.getValueOf(PolicyKeys.POLICY_NAME), current.getName());
		Assert.assertEquals(this.currentPolicyDetails.getValueOf(PolicyKeys.EXECUTION_TIME_MS), currentMS);
		if (add_details) {
			fillCurrentPolicyDetails(current);
		}
	}

//	private void fillCurrentPolicyDetails(ApicPolicy current) {
//		current.setDirection(this.currentPolicyDetails.getValueOf(PolicyKeys.DIRECTION));
//		current.setType(this.currentPolicyDetails.getValueOf(PolicyKeys.POLICY_TYPE));
//		this.setSideCalls(current, this.currentPolicyDetails.getSubElem(PolicyKeys.SIDE_CALLS));
//		this.setContextVariables(current, this.currentPolicyDetails.getSubElem(PolicyKeys.CONTEXT_VARIABLES));
//	}

	private void fillCurrentPolicyDetails(ApicPolicy current) {
		current.setType(this.currentPolicyDetails.getValueOf(PolicyKeys.POLICY_TYPE));
		String policyname = this.currentPolicyDetails.getValueOf(PolicyKeys.POLICY_NAME);
		String executiontime = this.currentPolicyDetails.getValueOf(PolicyKeys.EXECUTION_TIME_MS);
		String sidecalls = this.currentPolicyDetails.getValueOf(PolicyKeys.SIDE_CALLS);
		if (!sidecalls.contains("No Data"))
			this.setSideCalls(current, this.currentPolicyDetails.getSubElem(PolicyKeys.SIDE_CALLS));
		this.setContextVariables(current, this.currentPolicyDetails.getSubElem(PolicyKeys.POLICY_VARIABLES));
	}

	public void setSideCalls(ApicPolicy current, WebElement side_calls_table_elem) {
		WebElement elem = side_calls_table_elem.findElement(By.xpath(no_data.path));
		if (elem.getAttribute("style").contains("display: block;"))
			return;
		Table side_calls_table = new Table(side_calls_table_elem, new SideCallsPolicyCols());
		TableRow row;
		for (int i = 0; i < side_calls_table.getNumRows(); i++) {
			row = side_calls_table.getRow(i);
			current.addSideCall(row.getValue(SideCallsPolicyCols.TYPE), row.getValue(SideCallsPolicyCols.STATUS),
					row.getValue(SideCallsPolicyCols.ELAPSED_MS), row.getValue(SideCallsPolicyCols.DETAILS));
		}
	}

	public void setContextVariables(ApicPolicy current, WebElement context_variables_table_elem) {
		WebElement elem = context_variables_table_elem.findElement(By.xpath(no_data.path));
		if (!elem.getAttribute("style").contains("display: none;"))
			return;
		Table side_calls_table = new Table(context_variables_table_elem, new ContextPolicyCols());
		TableRow row;
		for (int i = 0; i < side_calls_table.getNumRows(); i++) {
			row = side_calls_table.getRow(i);
			current.addContextVariable(row.getValue(ContextPolicyCols.NAME), row.getValue(ContextPolicyCols.VALUE));
		}
	}

	public HashMap<String, List<TreeMap<String, String>>> getAllSideCalls() {
		HashMap<String, List<TreeMap<String, String>>> sc_map = new HashMap<String, List<TreeMap<String, String>>>();
		ApicPolicy policy;
		for (String policy_name : policies.getUserPolicies().keySet()) {
			policy = policies.getUserPolicies().get(policy_name);
			if (policy.getType() == null)
				if (policy_name.equals("end-block"))
					policy.setName("$$end-block$$");
			clickOnPolicy(policy, true);
			sc_map.put(policy_name, policies.getUserPolicies().get(policy_name).getSideCalls());
		}
		return sc_map;
	}

	public void setKeyValues() {
		this.currentPolicyDetails = new KeyValuePanelElement<PolicyKeys>(this.baseElement, PolicyKeys.values(),
				details_panel_content);
	}

	/*
	 * public void validateTransactinsPolicies(ArrayList<ApicPolicy>
	 * expected_policies_of_transaction) { Object[] keys =
	 * policies.getUserPolicies().keySet().toArray(); for (int i = 0; i <
	 * expected_policies_of_transaction.size(); i++) {
	 * clickOnPolicy(expected_policies_of_transaction.get(i), true);
	 * Assert.assertTrue(expected_policies_of_transaction.get(i).validateData(
	 * policies.getUserPolicies().get(keys[i].toString())), "Expected for " +
	 * expected_policies_of_transaction.get(i).toString() + " but FOUND " +
	 * policies.getUserPolicies().get(keys[i].toString()).toString()); } }
	 */

}
