package org.montier.qa.automation.pages.idg;

import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.montier.qa.automation.elements.KeyValuePanelElement;
import org.openqa.selenium.WebElement;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class IDGServiceConfigurationPage extends PageObject {
	@AllArgsConstructor
	public static enum SrvcCngDtlVals {
		DEVICE("Device"), SERVICE("Service"), FS_HANDLER("FS Handler"), SERVICE_TYPE("Service Type"), SOAP_VERSION("SOAP VERSION"),;
		private final @Getter String stringValue;

		@Override
		public String toString() {
			return stringValue;
		}

	}

	@AllArgsConstructor
	public static class SrvcCngDtlCols extends TableCols {
		public static final Col TIME = new Col("Time Sampled");
		public static final Col CHANGE_OPERATION = new Col("Change Operation");
		public static final Col DESCRIPTION = new Col("Description");
		public static final Col LATENCY_CHANGE = new Col("Latency Change");
		public static final Col ERROR_CHANGE = new Col("Error Change");
	}

	@Find(by = FBy.xpath, value = "//table")
	WebElement table;
	@Find(by = FBy.xpath, value = "//div[@class='panel-heading' and contains(text(), 'Service Configuration')]")
	WebElement service_conf_panel;

	public Table changeAuditTable;
	public KeyValuePanelElement<SrvcCngDtlVals> serviceConfPanel;

	public IDGServiceConfigurationPage() {
		super();
	}

	@Override
	public void initPageElement() {
		this.changeAuditTable = new Table(table, new SrvcCngDtlCols());
		this.serviceConfPanel = new KeyValuePanelElement<SrvcCngDtlVals>(service_conf_panel, SrvcCngDtlVals.values());
	}

	@Override
	public PageFilters getPageFilters() {
		return null;
	}
}
