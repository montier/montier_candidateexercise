package org.montier.qa.automation.toolkit.elements;

import org.montier.qa.automation.base.Element;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.elements.ElapsedTimeImageElementAPIC;
import org.openqa.selenium.WebElement;

public class TransactionAnalysisPanelAPIC extends Element {

	@Find(by = FBy.xpath, value = ".//div[contains(@class, 'qaTransactionPageLatencyDrawing')]")
	ByPath elapsed_times_image;
	@Find(by = FBy.xpath, value = ".//div[@class='panel-body']//child::label[text()='$KEY$']//following-sibling::p")
	ByPath value_by_key;

	public ElapsedTimeImageElementAPIC elapsedTimeImage;

	public TransactionAnalysisPanelAPIC(WebElement base_elem, String status) {
		super(base_elem);
//		if (status.equals("ERROR")) {
//			// Assert.assertTrue(this.findElementIfExists(elapsed_times_image).isEmpty(), "Unexpected elapsed time diagram when API status is ERROR");
//			Assert.assertTrue(this.findElementIfExists(elapsed_times_image).get(0).getText().length() == 0,
//					"Unexpected elapsed time diagram when API status is ERROR");
//			elapsedTimeImage = null;
//		} else
		elapsedTimeImage = new ElapsedTimeImageElementAPIC(this.findElement(elapsed_times_image));
	}

	public String getElapsedTime() {
		return this.findElement(value_by_key, "Elapsed Time").getText();
	}

	public String getErrorAnalysis() {
		return this.findElement(value_by_key, "Error Analysis").getText();
	}

	public String getErrorMessage() {
		return this.findElement(value_by_key, "Error Message").getText();
	}

	public String getErrorReason() {
		return this.findElement(value_by_key, "Error Reason").getText();
	}

	public String getPayloadSize() {
		return this.findElement(value_by_key, "Payload Size").getText();
	}
}
