package org.montier.qa.automation.elements;

import java.util.HashMap;
import java.util.TreeMap;

import org.montier.qa.automation.base.Element;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class KeyValuePanelElement<T> extends Element {
	public T[] values;

	@Find(by = FBy.xpath, value = "//div[@class='panel-body']//child::div[@class='row']")
	ByPath rows;
	@Find(by = FBy.xpath, value = "//div[@class='panel-body']//child::label[text()='$KEY$']//following-sibling::p")
	ByPath value_by_key;

	public KeyValuePanelElement(WebElement base_elem, T[] keysAndValues, TreeMap<T, String> validate_values) {
		super(base_elem);
		values = keysAndValues;
		validateValues(validate_values);
	}

	public KeyValuePanelElement(WebElement base_elem, T[] keysAndValues) {
		super(base_elem);
		values = keysAndValues;
	}

	public KeyValuePanelElement(WebElement base_elem, T[] keysAndValues, ByPath non_defaule_value_by_key) {
		super(base_elem);
		values = keysAndValues;
		this.value_by_key = non_defaule_value_by_key;
	}

	public void validateValues(TreeMap<T, String> validate_values) {
		String actual;
		String expected;
		for (T key : values) {
			expected = validate_values.get(key);
			if (expected != null) {
				actual = getValueOf(key);
				Assert.assertEquals(actual, expected, "Failed for key " + key + ": Expected for " + expected + " but found " + actual);
			}
		}
	}

	public HashMap<T, String> getValues() {
		HashMap<T, String> all_values = new HashMap<T, String>();
		for (T key : values)
			all_values.put(key, this.getValueOf(key));
		return all_values;
	}

	public String getValueOf(T key) {
		WebElement val = this.findElement(value_by_key, key.toString());
		return val.getText();
	}

	public WebElement getSubElem(T key) {
		return this.findElement(value_by_key, key.toString());
	}
}
