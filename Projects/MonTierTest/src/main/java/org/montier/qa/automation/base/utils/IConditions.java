package org.montier.qa.automation.base.utils;

import java.util.function.BiFunction;

import org.montier.qa.automation.base.Driver;
import org.openqa.selenium.WebElement;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class IConditions {

	@AllArgsConstructor
	public static abstract class BiCondition {
		private @Getter Object obj;
		public Object compare_to_value;

		public abstract Boolean apply(Object value);
	}

	public static class IntegerBiCondition extends BiCondition {
		BiFunction<Integer, Integer, Boolean> fun;

		public IntegerBiCondition(Object obj, Object compare_to_value) {
			super(obj, compare_to_value);
		}

		@Override
		public Boolean apply(Object value) {
			String str_num = value.toString().replaceAll("\\D+", "");
			if (str_num.length() == 0)
				return false;
			return fun.apply(Integer.parseInt(str_num), (Integer) compare_to_value);
		}
	}

	public static class StringBiCondition extends BiCondition {
		BiFunction<String, String, Boolean> fun;

		public StringBiCondition(Object obj, String compare_to_value) {
			super(obj, compare_to_value);
		}

		@Override
		public Boolean apply(Object value) {
			return fun.apply((String) value, (String) compare_to_value);
		}
	}

	public static class BooleanBiCondition extends BiCondition {
		BiFunction<Boolean, Boolean, Boolean> fun;

		public BooleanBiCondition(Object obj, Boolean compare_to_value) {
			super(obj, compare_to_value);
		}

		@Override
		public Boolean apply(Object value) {
			return fun.apply((Boolean) value, (Boolean) compare_to_value);
		}
	}

	public static class WebElementAttributeBiCondition extends BiCondition {
		BiFunction<String, String, Boolean> fun;
		String elem_attribute;

		public WebElementAttributeBiCondition(Object obj, String attribute_name, String attribute_value) {
			super(obj, attribute_value);
			this.elem_attribute = attribute_name;
		}

		@Override
		public Boolean apply(Object elem) {
			return fun.apply(((WebElement) elem).getAttribute(this.elem_attribute), (String) compare_to_value);
		}
	}

	public static class ExistsWebElementCondition extends BooleanBiCondition {
		ByPath find_elem;

		public ExistsWebElementCondition(Object obj, ByPath elem_to_find) {
			super(obj, true);
			this.find_elem = elem_to_find;
		}

		@Override
		public Boolean apply(Object elem) {
			return fun.apply(!Driver.getInstance().findElements(find_elem, (WebElement) elem).isEmpty(), true);
		}
	}

	public static BiCondition getGreaterThanCondition(Object obj, Integer compare_to_value) {
		IntegerBiCondition cond = new IntegerBiCondition(obj, compare_to_value);
		cond.fun = (num1, num2) -> num1 > num2 ? true : false;
		return cond;
	}

	public static BiCondition getLowerThanCondition(Object obj, Integer compare_to_value) {
		IntegerBiCondition cond = new IntegerBiCondition(obj, compare_to_value);
		cond.fun = (num1, num2) -> num1 < num2 ? true : false;
		return cond;
	}

	public static BiCondition getContainsCondition(Object obj, String compare_to_value) {
		StringBiCondition cond = new StringBiCondition(obj, compare_to_value);
		cond.fun = (str, sub_str) -> str.contains(sub_str) ? true : false;
		return cond;
	}

	public static BiCondition getEqualsCondition(Object obj, String compare_to_value) {
		StringBiCondition cond = new StringBiCondition(obj, compare_to_value);
		cond.fun = (str, str2) -> str.equals(str2) ? true : false;
		return cond;
	}

	public static WebElementAttributeBiCondition getWebElementConstainsAttributeValue(Object obj, String compare_to_value,
			String elem_attribute_to_compare) {
		WebElementAttributeBiCondition cond = new WebElementAttributeBiCondition(obj, compare_to_value, elem_attribute_to_compare);
		cond.fun = (str, str2) -> str.contains(str2) ? true : false;
		return cond;
	}

	public static ExistsWebElementCondition isElementContainsElem(Object obj, ByPath sub_elem_to_find) {
		ExistsWebElementCondition cond = new ExistsWebElementCondition(obj, sub_elem_to_find);
		return cond;
	}
	/*
	 * public ICondition getGreaterThanCondition(Col col, String value) { Icondition cond = new ICondition() }
	 * 
	 * this.findFirstRowSatisfies(IConditions.getGreaterThanCondition(TableCols.ELAPSED, 1700), Iconditions.getHasElementClass(TableCols.PAYLOAD,
	 * "")); public TableRow findFirstRowSatisfies(ICondition... conditions) { int i = 0; WebElement ceil = null; Boolean res; while (i <=
	 * this.numRows) { i++; res = true; for(ICondition cond: conditions) { ceil = this.getRow(i).getCeil(conditions[0].col);
	 * if(!conditions[0].apply(ceil, value)) { res = false; break; } } if(res) break; } return res? this.rows.get(i): null;
	 * 
	 * }
	 */
}
