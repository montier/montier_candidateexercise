package org.montier.qa.automation.pages;

import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.openqa.selenium.WebElement;

import lombok.Setter;

public class SignInPage extends PageObject {
	@Find(by = FBy.name, value = "j_username", init = true)
	private @Setter WebElement userName;
	@Find(by = FBy.name, value = "j_password", init = true)
	private WebElement password;
	@Find(by = FBy.cssSelector, value = ".btn", init = true)
	private WebElement submitButton;

	public SignInPage() {
		super();
	}

	@Override
	public void initPageElement() {
		// TODO Auto-generated method stub

	}

	public void enterUserName(String userName) {
		this.userName.clear();
		this.userName.sendKeys(userName);
	}

	public void enterPassword(String password) {
		this.password.clear();
		this.password.sendKeys(password);
	}

	public void submit() {
		submitButton.click();
	}

	public void logIn(String userName, String password) {
		Log.info("Log in for user " + userName);
		this.enterUserName(userName);
		this.enterPassword(password);
		this.submit();
	}

	@Override
	public PageFilters getPageFilters() {
		return null;
	}

	@Override
	public void openPage(String... strings) {
		// Signin PAge is very effete page, no need all function
		this.loadPageWait();
		this.initGeneralPathes();
		this.checkHTTPErrors();
		this.initWebElements(this.getClass());
	}
}
