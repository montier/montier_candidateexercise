package org.montier.qa.automation.elements;

import java.util.List;
import java.util.NavigableMap;
import java.util.SortedMap;
import java.util.TreeMap;

import org.montier.qa.automation.base.Element;
import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.internal.collections.Pair;

import lombok.Getter;

public class ChartLine extends Element {
	@Find(by = FBy.xpath, value = ".//div[contains(@class, 'panel-body')]//span[contains(@class, 'gwt-InlineHTML')]//*[contains(@class,'fa-minus') or contains(@class, 'fa-square')]/following-sibling::span[text()='$NAME$']/parent::span/i")
	ByPath legend_element;
	@Find(by = FBy.xpath, value = ".//*[@class='subplot xy']//*[@class='js-line']")
	ByPath js_line;
	@Find(by = FBy.xpath, value = ".//*[@class='subplot xy']//*[@class='js-line' and contains(@style, '$STYLE$')]")
	ByPath js_line_style;
	@Find(by = FBy.xpath, value = ".//*[@class='subplot xy']//*[@class='trace bars']//*[contains(@style, '$STYLE$')]")
	ByPath trace_bar_points;

	public String name;
	public String type;
	public String style;
	private @Getter TreeMap<Float, Float> linePoints;
	public WebElement line;
	// protected Date xPixelDif;
	// protected Float yPixelDif;

	public ChartLine(String name, WebElement base_elem) {
		super(base_elem);
		this.name = name;
		this.linePoints = new TreeMap<Float, Float>();
		this.initChartLine(name);
	}

	protected void initChartLine(String name) {
		Log.debug("init chart line >> " + name);
		List<WebElement> chart_element = this.findElementIfExists(legend_element, name);
		String class_name;
		if (!chart_element.isEmpty()) {
			this.style = this.adapt_color_path(chart_element.get(0).getCssValue("color"));
			class_name = chart_element.get(0).getAttribute("class");
		} else {
			this.style = "";
			class_name = "minus";
		}

		if (class_name.contains("minus")) {
			this.type = "js-line";
			this.buildJsLine();
		} else {
			this.type = "trace bars";
			this.buildTraceBars();
		}
	}

	public void buildTraceBars() {
		List<WebElement> line_bars = this.findElements(this.trace_bar_points, this.style);
		if (line_bars.size() == 1)
			this.type = "count chart";
		String debug_line_string = "";
		Float[] point;
		for (WebElement bar : line_bars) {
			point = parseTracePoints(bar.getAttribute("d"));
			this.linePoints.put(point[0] + (point[1] - point[0]) / 2, point[2]);
			debug_line_string += point[0] + " " + point[2] + ", " + point[1] + " " + point[2] + ",";
		}
		Log.debug(debug_line_string);
	}

	private Float[] parseTracePoints(String point_text) {
		// Text points now looks like "M<x1>,<numberV><y>H<x2><numberZ>" for example"M606.25,150V106.55H724.55V150Z"
		String[] split_comma, split_H;
		Float x1, x2, y;
		split_comma = point_text.split(",");
		x1 = Float.parseFloat(split_comma[0].substring(1));
		split_H = split_comma[1].split("V")[1].split("H");
		x2 = Float.parseFloat(split_H[1]);
		y = Float.parseFloat(split_H[0]);
		return new Float[] { x1, x2, y };
	}

	private void buildJsLine() {
		this.line = this.findElement(this.js_line_style, this.style);
		Assert.assertNotNull(this.line, "Chart is empty and there is no line");
		String[] line_pixels = line.getAttribute("d").replace("M", "").replaceAll("[^\\d.,-]", " ").split(" ");
		String[] point;
		String debug_line_string = "";
		for (String pix : line_pixels) {
			debug_line_string += pix + " , ";
			point = pix.split(",");
			this.linePoints.put(Float.parseFloat(point[0]), Float.parseFloat(point[1]));
		}
		Log.debug(debug_line_string);

	}

	private String adapt_color_path(String color) {
		String adapted_color = color.replace("rgba", "rgb");
		adapted_color = adapted_color.substring(0, adapted_color.lastIndexOf(","));
		return adapted_color + ")";
	}

	public Point getHighestPoint(Float start_x, Float end_x) {
		start_x = this.linePoints.higherKey(start_x);
		end_x = (end_x <= start_x) ? this.linePoints.lastKey() : this.linePoints.lowerKey(end_x);
		Log.debug("Highest point in range (" + start_x + " ," + end_x + ")");
		SortedMap<Float, Float> sub_map = this.linePoints.subMap(start_x, end_x);
		Float high_y = sub_map.get(sub_map.firstKey());
		Float high_x = sub_map.firstKey();
		for (Float x : sub_map.keySet()) {
			if (sub_map.get(x) <= high_y) { // the y value of js-line stores as the distance from the top of the chart. so if for example
											// y=chart.height the y value is 0
				high_y = sub_map.get(x);
				high_x = x;
			}
		}
		Log.debug("Highest point: (" + high_x + " , " + high_y + ")");
		return new Point(Math.round(high_x), Math.round(high_y));
	}

	public Point getLowestPoint(Float start_x, Float end_x) {
		SortedMap<Float, Float> sub_map = this.linePoints.subMap(start_x, end_x);
		Float low_x = sub_map.firstKey();
		Float low_y = sub_map.get(low_x);
		for (Float x : sub_map.keySet()) {
			if (sub_map.get(x) >= low_y) { // the y pixels in js_lines stores in upside down, so if the current y higher than point, its actually
											// smaller
				low_y = sub_map.get(x);
				low_x = x;
			}
		}
		Log.debug("Lowest point: (" + low_x + " , " + low_y + ")");
		return new Point(Math.round(low_x), Math.round(low_y));
	}

	public Point getLowerPoint(Float start_x, Float end_x, Float y_pix) {
		SortedMap<Float, Float> sub_map = this.linePoints.subMap(start_x, end_x);
		Float low_y = y_pix;
		Float low_x = start_x;
		for (Float x : sub_map.keySet())
			if (low_y <= sub_map.get(x)) {
				low_y = sub_map.get(x);
				low_x = x;
				break;
			}
		return new Point(Math.round(low_x), Math.round(low_y));
	}

	public Float getAverageValue(Float start_x, Float end_x) {
		// TODO!
		SortedMap<Float, Float> sub_map = this.linePoints.subMap(start_x, end_x);
		Float sum_y = 0.0f;
		for (Float y : sub_map.values())
			sum_y += y;
		// new Point(Math.round(low_x), Math.round(low_y));
		return sum_y;
	}

	/*
	 * private Point over_sub_map(String mathod, Float start_x, Float end_x) { SortedMap<Float, Float> sub_map =
	 * this.linePoints.subMap(this.linePoints.higherKey(start_x), this.linePoints.lowerKey(end_x)); Float high_y = 0.0f; Float high_x = start_x;
	 * for(Float x: sub_map.keySet()) if (high_y < sub_map.get(x)) {
	 * 
	 * high_y = sub_map.get(x); high_x = x; } return new Point(Math.round(high_x), Math.round(high_y));
	 * 
	 * }
	 */

	public Pair<Float, Float> getLastValueHigherThan(Float value) {
		NavigableMap<Float, Float> desc_line = this.linePoints.descendingMap();
		Float higher_y = value;
		Float higher_x = null;
		for (Float x : desc_line.keySet())
			if (value > desc_line.get(x)) {
				higher_y = desc_line.get(x);
				higher_x = x;
				break;
			}
		Log.debug("getLastValueHigherThan " + value + " = (" + higher_x + "," + higher_y + ")");
		return new Pair<Float, Float>(higher_x, higher_y);
	}

	public Pair<Float, Float> getLastValueLowerThan(Float value, Float end_x) {
		NavigableMap<Float, Float> des = this.linePoints.descendingMap();
		NavigableMap<Float, Float> desc_line = des.subMap(end_x, true, this.linePoints.firstKey(), true);
		Float lower_y = value;
		Float lower_x = null;
		for (Float x : desc_line.keySet()) {
			if (x > end_x)
				break;
			if (value < desc_line.get(x)) {
				lower_y = desc_line.get(x);
				lower_x = x;
				break;
			}
		}
		if (lower_x == null) {
			Log.debug("There is no point with value lower than " + value + " in range [0, " + end_x + "]");
			return null;
		} else {
			Log.debug("getLastValueLowerThan " + value + "," + end_x + " = (" + lower_x + "," + lower_y + ")");
			return new Pair<Float, Float>(lower_x, lower_y);
		}
	}

	public Float xRangeWhenValueHigherThan(Float value) {
		Pair<Float, Float> last_higher = this.getLastValueHigherThan(value);
		Log.debug("Last Point higher than " + value + " = " + last_higher);
		Pair<Float, Float> first_higher = getLastValueLowerThan(value, last_higher.first());
		return last_higher.first() - first_higher.first();
	}
}
