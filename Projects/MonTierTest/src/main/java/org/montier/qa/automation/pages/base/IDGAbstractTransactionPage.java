package org.montier.qa.automation.pages.base;

import java.util.ArrayList;

import org.montier.qa.automation.base.UsersRoles;
import org.montier.qa.automation.base.testutils.TestUtils;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.FilterUtils.FilterStatus;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.elements.KeyValuePanelElement;
import org.montier.qa.automation.elements.TabsPanelElement;
import org.montier.qa.automation.elements.TabsPanelElement.Tab;
import org.montier.qa.automation.pages.RoleViewPage.Permissions;
import org.montier.qa.automation.pages.idg.IDGTransactionExtendedLatency;
import org.montier.qa.automation.pages.idg.IDGTransactionPayloadPage;
import org.montier.qa.automation.pages.idg.IDGTransactionRawMessagesPage;
import org.montier.qa.automation.pages.idg.IDGTransactionSideCallsPage;
import org.montier.qa.automation.toolkit.elements.TransactionAnalysisPanel;
import org.montier.qa.automation.toolkit.elements.TransactionComponents;
import org.openqa.selenium.WebElement;

import lombok.AllArgsConstructor;
import lombok.Getter;

public abstract class IDGAbstractTransactionPage extends IDGAbstractInvestigatePage {

	@AllArgsConstructor
	public static enum TransRawVal {
		TIME("Time"), STATUS("Status"), DEVICE("Device"), DOMAIN("Domain"), SERVICE("Service"), OPERATION("Operation"),
		CLIENT_IP("Client IP"), GL_TRANS_ID("Gl. Trans. ID");

		private final @Getter String stringValue;

		@Override
		public String toString() {
			return stringValue;
		}
	}

	public class TransRawFilters extends PageFilters {
		public FilterUtils TIME = new FilterUtils("Time", FilterStatus.URL);
		public FilterUtils DEVICE = new FilterUtils("Device", FilterStatus.URL);
		public FilterUtils DOMAIN = new FilterUtils("Domain", FilterStatus.URL);
		public FilterUtils TRANS_ID = new FilterUtils("Trans. ID", FilterStatus.URL);

		@Override
		public boolean allAreUrlFilters() {
			return true;
		}
	}

	@Find(by = FBy.xpath, value = ".//span[text()='Transaction $TITLE$']//ancestor::div[contains(@class, 'panel panel-default')]")
	ByPath trans_details_panel;
	@Find(by = FBy.xpath, value = "//div[text()=' Transaction Analysis ']/parent::div[contains(@class, 'panel')]")
	ByPath trans_analysis_panel;
	@Find(by = FBy.xpath, value = ".//div[contains(@class, 'qaTransactionPageLatencyDrawing')]")
	ByPath elapsed_times_image;
	@Find(by = FBy.xpath, value = ".//div[contains(@class, 'qaTransactionPageDetailsPanel')]")
	WebElement menuPanelElement;

	public String transID;
	public KeyValuePanelElement<TransRawVal> transDetails;
	public TransactionAnalysisPanel transactionAnalysisPanel;
	protected TransactionComponents transactionComponents;

	// public ElapsedTimeImageElement elapsedTimeImage;
	public TabsPanelElement tabsPanelElem;

	public IDGAbstractTransactionPage() {
		super();
	}

	@Override
	public void initPageElement() {
		super.initPageElement();
		this.transDetails = new KeyValuePanelElement<TransRawVal>(this.findElement(trans_details_panel, this.transID),
				TransRawVal.values());
		this.tabsPanelElem = new TabsPanelElement(menuPanelElement, getTabs());
		this.tabsPanelElem.setPageArgs(this.transID);
		this.getPageFilters().DEVICE.applyFilter(this.transDetails.getValueOf(TransRawVal.DEVICE));
		this.getPageFilters().DOMAIN.applyFilter(this.transDetails.getValueOf(TransRawVal.DOMAIN));
		this.getPageFilters().TRANS_ID.applyFilter(this.transID);

		this.initValidationValues();
		transactionAnalysisPanel = new TransactionAnalysisPanel(this.findElement(trans_analysis_panel),
				this.transDetails.getValueOf(TransRawVal.STATUS));
		transactionComponents = new TransactionComponents(menuPanelElement, IDGTransactionRawMessagesPage.class,
				IDGTransactionPayloadPage.class, IDGTransactionSideCallsPage.class,
				IDGTransactionExtendedLatency.class);
		// elapsedTimeImage = new
		// ElapsedTimeImageElement(this.findElement(elapsed_times_image));

	}

	private Tab[] getTabs() {
		ArrayList<Tab> tabs = new ArrayList<Tab>();
		tabs.add(new Tab("Raw Messages", IDGTransactionRawMessagesPage.class,
				!UsersRoles.getPermissionOfCurrentUser(Permissions.AllowRawMessages)));
		tabs.add(new Tab("Payload", IDGTransactionPayloadPage.class,
				!UsersRoles.getPermissionOfCurrentUser(Permissions.AllowPayload)));
		tabs.add(new Tab("Extended Latency", IDGTransactionExtendedLatency.class));
		tabs.add(new Tab("Side Calls", IDGTransactionSideCallsPage.class));
		Tab[] arr = new Tab[tabs.size()];
		return tabs.toArray(arr);
	}

	@Override
	public void openPage(String... transID) {
		this.transID = transID != null && transID.length > 0 ? transID[0] : null;
		super.openPage();
	}

	@Override
	public void refresh() {
		this.openPage(this.transID);
	}

	public abstract Object getComponentPanel();

	public int getElapsedVal() {
		return TestUtils.StringUtils.parseMS(this.transactionAnalysisPanel.getElapsedTime());
	}

	public String getServiceVal() {
		return this.transDetails.getValueOf(TransRawVal.SERVICE);
	}

	public String getDomainVal() {
		return this.transDetails.getValueOf(TransRawVal.DOMAIN);
	}

	public String getDevicenVal() {
		return this.transDetails.getValueOf(TransRawVal.DEVICE);
	}

	public String getErrors() {
		return this.transactionAnalysisPanel.getErrorAnalysis();
	}

	@Override
	protected void initValidationValues() {
//		this.addValidationField("service", this.transDetails.getValueOf(TransRawVal.SERVICE));
		this.addValidationField("time", this.transDetails.getValueOf(TransRawVal.TIME));
		this.addValidationField("domain", this.transDetails.getValueOf(TransRawVal.DOMAIN));
		this.addValidationField("device", this.transDetails.getValueOf(TransRawVal.DEVICE));
	}

	@Override
	public TransRawFilters getPageFilters() {
		if (this.pageFilters == null)
			this.pageFilters = new TransRawFilters();
		return (TransRawFilters) this.pageFilters;
	}

}
