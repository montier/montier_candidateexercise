package org.montier.qa.automation.pages.idg;

import java.util.Arrays;

import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.montier.qa.automation.pages.base.AbstractExplorePage;
import org.openqa.selenium.WebElement;

public class IDGFailedObjectsPage extends AbstractExplorePage {
	public class ActivityFilters extends PageFilters {
		public FilterUtils DEVICE = new FilterUtils("Device");
		public FilterUtils DOMAIN = new FilterUtils("Domain");
		public FilterUtils OPJECT_TYPE = new FilterUtils("Object Type");
	}

	public static class DownObjectsCols extends TableCols {
		public static final Col TIME_DETECTED = new Col("Time Detected");
		public static final Col DEVICE = new Col("Device");
		public static final Col DOMAIN = new Col("Domain");
		public static final Col OBJECT_NAME = new Col("Object Name");
		public static final Col OBJECT_TYPE = new Col("Object Type");
		public static final Col OPERATIONAL_STATE = new Col("Operational State");
		public static final Col ADMIN_STATE = new Col("Admin State");
		public static final Col EVENT_CODE = new Col("Event code");
		public static final Col ERROR_CODE = new Col("Error Code");

		public DownObjectsCols() {
			super();
		}

		@Override
		public void setColsIndices() {
			colsIndices.addAll(
					Arrays.asList(TIME_DETECTED, DEVICE, DOMAIN, OBJECT_NAME, OBJECT_TYPE, OPERATIONAL_STATE, ADMIN_STATE, EVENT_CODE, ERROR_CODE));
		}
	}

	@Find(by = FBy.className, value = "DownObjectsGrid")
	WebElement table;

	public Table downObjectsTable;

	public IDGFailedObjectsPage() {
		super();
	}

	@Override
	public void initPageElement() {
		super.initPageElement();
		this.downObjectsTable = new Table(table, new DownObjectsCols());

	}

	@Override
	public ActivityFilters getPageFilters() {
		if (this.pageFilters == null)
			this.pageFilters = new ActivityFilters();
		return (ActivityFilters) this.pageFilters;
	}

}
