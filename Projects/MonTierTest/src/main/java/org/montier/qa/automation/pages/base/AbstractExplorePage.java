package org.montier.qa.automation.pages.base;

import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.elements.TabsPanelElement;
import org.montier.qa.automation.elements.TabsPanelElement.Tab;
import org.montier.qa.automation.pages.idg.IDGFailedObjectsPage;
import org.montier.qa.automation.pages.idg.IDGServicesConfiguration;
import org.openqa.selenium.WebElement;

public abstract class AbstractExplorePage extends PageObject {

	@Find(by = FBy.className, value = "ExploreMenu")
	WebElement exploreMenu;

	public TabsPanelElement exploreTabs;

	public AbstractExplorePage() {
		super();
	}

	@Override
	public void initPageElement() {
		this.exploreTabs = new TabsPanelElement(exploreMenu, new Tab("Service Configuration", IDGServicesConfiguration.class),
				new Tab("Failed Objects", IDGFailedObjectsPage.class));
	}

}
