package org.montier.qa.automation.elements;

import static org.testng.Assert.assertEquals;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.montier.qa.automation.base.Driver;
import org.montier.qa.automation.base.Element;
import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PageObject.PageFilters;
import org.montier.qa.automation.base.PagesFactory;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.RunTestException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import lombok.Getter;

public class FiltersPanelElement extends Element {
	@Find(by = FBy.xpath, value = "//div[contains(@class, 'FiltersPanel')]/*[1]/*[not(contains(@class, 'hidden'))] | div[contains(@class, 'Filter')]")
	ByPath filters;
	@Find(by = FBy.xpath, value = "//input[@placeholder='Free Text']")
	ByPath freeText;
	@Find(by = FBy.xpath, value = "//span//button[@class='btn btn-default']")
	ByPath applyFreeText;
	@Find(by = FBy.xpath, value = "//div[@class='dropdown open']//input[@type='text']")
	ByPath textBox;
	@Find(by = FBy.xpath, value = "//div[@class='dropdown open']//button[text()='Apply']")
	ByPath applyButton;
	@Find(by = FBy.linkText, value = "$LINKNAME$")
	ByPath dropdown;
	@Find(by = FBy.className, value = "qaFilterDescription")
	ByPath readTextBox;
	@Find(by = FBy.xpath, value = "//button[span='$FILTERNAME$']//following-sibling::*//div[contains(@class, 'AutoCompleteListItem')]//child::span[text()!='']")
	ByPath listItem;
	@Find(by = FBy.xpath, value = "//div[@class='dropdown open']//button[text()='Clear']")
	ByPath ClearButton;
	@Find(by = FBy.xpath, value = "//div[contains(@class, 'FiltersPanel')]//div[text()=' Reset ']")
	ByPath resetFilters;
	@Find(by = FBy.xpath, value = "//div[contains(@class, 'FiltersPanel')]//span[@class=\"caret\"]/parent::div")
	ByPath showMoreLess;

	private @Getter boolean isShowMoreOpen;
	LinkedHashMap<String, WebElement> filters_elements;
	HashMap<String, String> current_filters;

	public FiltersPanelElement() {
		super();
		isShowMoreOpen = false;
		filters_elements = new LinkedHashMap<String, WebElement>();
		current_filters = getKeyValueFilteredTextBox();
		this.initFilters(true);
	}

	private LinkedHashMap<String, String> getKeyValueFilteredTextBox() {
		LinkedHashMap<String, String> filter_key_value = new LinkedHashMap<>();
		List<WebElement> read_textboxs = this.findElementIfExists(readTextBox);
		String[] key_value_text;
		String key;
		String value;
		for (WebElement text_box_elem : read_textboxs) {
			key_value_text = text_box_elem.getText().split("\\[|\\]");

			key = key_value_text[0].trim();
			value = key_value_text[1].trim();

			switch (key) {
			case ("Transaction ID"):
				key = "Trans. ID";
			case ("Global Transaction ID"):
				key = "Global Trans. ID";
			case ("Time"):
				value = value.substring(0, 1) + value.substring(1).toLowerCase();
			}

			filter_key_value.put(key, value);
		}

		return filter_key_value;
	}

	private void checkFilteredValues() {
		LinkedHashMap<String, String> page_filters = getKeyValueFilteredTextBox();
		if (current_filters.size() != page_filters.size()) {
			Assert.fail("EXPECTED " + current_filters + " BUT FOUND " + page_filters);
		}
		Log.debug("EXPECTED " + current_filters + " FOUND " + page_filters);
		String expected, actual;
		for (String filter_key : page_filters.keySet()) {
			expected = current_filters.get(filter_key);
			Assert.assertNotNull(expected, "Filter " + filter_key + " appears on page not as expected");
			actual = page_filters.get(filter_key).trim();
			assertEquals(actual, expected.trim());
		}
	}

	public void initFilters(boolean removeUnexisting) {
		String prev_path = null;
		if (Driver.getInstance().getDriver().getCurrentUrl().contains("#apicTransactions:")) {
			prev_path = filters.path;
			filters.setPath(
					"//div[contains(@class, 'FiltersPanel')]/*//div[contains(@class, 'Filter') and not(contains(@class, 'hidden')) and not(contains(@class, 'FavoriteFiltersModal'))and not(contains(@class, 'FilterPanelToolsWidget'))]");
		}

		filters_elements.clear();
		List<WebElement> filter_elements = this.findElementIfExists(filters);
		filter_elements.stream().forEach(element -> filters_elements.put(element.getText(), element));
		if (removeUnexisting)
			// this.checkFilteredValues();
			if (prev_path != null)
				filters.setPath(prev_path);
	}

	private List<WebElement> findFilterOptions(String filter_name) {
		initFilters(false);
		filters_elements.get(filter_name).click();
		List<WebElement> list_item = this.findElements(listItem, filter_name);

		return list_item;
	}

	public List<String> findFilterOptionsText(String filter_name) {
		List<WebElement> filter_options_elements = findFilterOptions(filter_name);
		return filter_options_elements.stream().map(WebElement::getText).collect(Collectors.toList());
	}

	public FiltersPanelElement selectOption(String filter_name, int index) {
		this.initFilters(false);
		List<WebElement> filter_options_elements = findFilterOptions(filter_name);
		if (index >= filter_options_elements.size())
			throw new RunTestException("index out of list");
		String value = filter_options_elements.get(index).getText();
		filter_options_elements.get(index).click();
		this.current_filters.put(filter_name, value);
		this.checkFilteredValues();
		return this;
	}

	public void selectOption(String filter_name, String value) {
		this.initFilters(false);
		List<WebElement> filter_options_elements = findFilterOptions(filter_name);
		boolean found = false;
		for (WebElement elem : filter_options_elements)
			if (value.equals(elem.getText())) {
				elem.click();
				found = true;
				this.current_filters.put(filter_name, value);
				this.checkFilteredValues();
				break;
			}
		if (!found)
			throw new RunTestException("Filter option " + value + " not exists");

	}

	private WebElement freeText(String value) {
		WebElement free_text = this.findElement(freeText);
		free_text.sendKeys(value);
		return this.findElement(applyFreeText);
	}

	private WebElement textBox(String value) {
		WebElement text_box = this.findElement(textBox);
		text_box.sendKeys(value);
		return this.findElement(applyButton);
	}

	private WebElement dropDown(String value) {
		return (this.findElement(dropdown, value));

	}

	private WebElement findFilterType(String filter_name, String value) {
		WebElement filter_input;
		if ("Apply".equals(filter_name)) {
			filter_input = this.freeText(value);

		}
		try {
			this.driver.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.MILLISECONDS);
			filter_input = this.textBox(value);

		} catch (Exception e) {
			filter_input = this.dropDown(value);
		}
		return filter_input;

	}

	public FiltersPanelElement clearFilter(String filter_name) {
		this.initFilters(false);
		filters_elements.get(filter_name).click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.current_filters.remove(filter_name);
		this.findElement(ClearButton).click();
		return this;
	}

	public FiltersPanelElement applyFilter(String filter_name, String value) {
		initFilters(false);
		filters_elements.get(filter_name).click();
		WebElement filter_input = this.findFilterType(filter_name, value);
		filter_input.click();
		if (filter_name == "Time")
			value = value.substring(0, 1) + value.substring(1).toLowerCase();
		this.current_filters.put(filter_name, value);
		this.checkFilteredValues();
		return this;
	}

	public void addFilterToCurrent(String filter_name, String value) {
		Log.debug("Add To Current: " + filter_name + ":" + value);
		this.current_filters.put(filter_name, value);
	}

	public void removeFilterFromCurrent(String filter_name) {
		this.current_filters.remove(filter_name);

	}

	public Set<String> getFilterNames() {
		return this.filters_elements.keySet();
	}

	public FiltersPanelElement resetFilters(boolean current_only, boolean refresh) {
		Log.debug("reset filters" + (current_only ? " current only " : ""));
		if (!current_only) {
			if (this.findElementIfExists(resetFilters).size() == 0) {
				if (PagesFactory.getInstance().currentPage.getPageFilters() == null)
					Log.debug("No filters to reset");
				else {
					Assert.assertTrue(PagesFactory.getInstance().currentPage.getPageFilters().allAreUrlFilters(),
							"There are filters on page, expected for reset filter button");
					Log.debug(
							"page has just URL filters and there is no reset  button as expected, skipping reset (just from current if needed)");
				}
			} else {
				Log.debug("click on reset filters link");
				this.findElement(resetFilters).click();
			}
		}
		Log.debug("Clear current filter list");
		this.current_filters.clear();
		Log.debug("puy filter Time as default value 'Last 24 hours'");
		this.current_filters.put("Time", "Last 24 hours");
		if (refresh) {
			Log.debug("refresh page after filters were reset");
			PagesFactory.getInstance().currentPage.refresh();
		}
		this.isShowMoreOpen = false;
		return this;
	}

	public boolean ensureAllFiltersExists(PageFilters page_object_filters) {
		this.initFilters(false);
		String expected_fil_name, actual_fil_name;
		List<String> po_filters = page_object_filters.getFilters(false);
		Log.debug("Check all filters exists as expected: " + po_filters);
		Set<String> filt_name = this.getFilterNames();
		filt_name.remove("Show More");
		filt_name.remove("Show Less");

		Iterator<String> po_filters_itr = po_filters.iterator();
		Iterator<String> fil_name_itr = filt_name.iterator();
		// Assert.assertTrue(filt_name.size() == filters.size(),
		// "there are " + filt_name.size() + "filters in page are " + filt_name + " but
		// expected for " + po_filters.size() + "filters" +
		// page_object_filters);
		while (fil_name_itr.hasNext() && po_filters_itr.hasNext()) {
			actual_fil_name = fil_name_itr.next();
			if (actual_fil_name.trim().length() == 0)
				continue;
			expected_fil_name = po_filters_itr.next();
			Assert.assertTrue(expected_fil_name.equals(actual_fil_name),
					"expected for filter name " + expected_fil_name + " but found " + actual_fil_name);
			if (expected_fil_name.equals("Time") && current_filters.get(expected_fil_name) == null)
				current_filters.put("Time", "Last 24 hours");
		}
		if (fil_name_itr.hasNext() || po_filters_itr.hasNext()) {
			String next = fil_name_itr.hasNext() ? fil_name_itr.next() : po_filters_itr.next();
			if (next.trim().length() > 0)
				Assert.fail("there are " + filt_name.size() + "filters in page are " + filt_name + " but expected for "
						+ po_filters.size() + "filters" + po_filters);
		}

		/*
		 * for (String fld : filters) { try { check = fil_name_itr.next();
		 * Assert.assertTrue(check.equals(fld)); } catch (IllegalArgumentException e) {
		 * throw new RunTestException(e); } if (check.equals("Time") &&
		 * current_filters.get(check) == null) current_filters.put("Time",
		 * "Last 24 hours"); }
		 */
		return true;
	}

	public void updateCurrentFilters(PageFilters page_filters) {
		List<String> filters = page_filters.getFilters(true);
		Iterator<String> current = new HashSet<String>(current_filters.keySet()).iterator();
		while (current.hasNext()) {
			String fil = current.next();
			if (fil.contains("Transaction"))
				fil.replaceFirst("Transaction", "Trans.");
			if (fil.equals("Time") && filters.contains("Downtime Started")) {
				fil = "Downtime Started";
				current_filters.put("Downtime Started", current_filters.get("Time"));
				current_filters.remove("Time");
			}
			if (fil.equals("Downtime Started") && filters.contains("Time")) {
				fil = "Time";
				current_filters.put("Time", current_filters.get("Downtime Started"));
				current_filters.remove("Downtime Started");
			}
			try {
				if (filters.contains(fil))
					Log.debug("filter page contains this filter " + fil);
				else {
					current_filters.remove(fil);
					Log.debug("DELETE filter. page not contains this filter " + fil);
				}
			} catch (IllegalArgumentException e) {
				throw new RunTestException(e);
			}
		}
	}

	public String getFilteredValue(String filter_name) {
		return current_filters.get(filter_name);
	}

	public void clickShowMoreLess() {
		WebElement elem = this.findElement(this.showMoreLess);
		elem.click();
		this.isShowMoreOpen = !this.isShowMoreOpen;
	}
}
