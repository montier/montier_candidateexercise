package org.montier.qa.automation.elements;

import org.montier.qa.automation.base.Element;
import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.testutils.TestUtils;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.openqa.selenium.WebElement;

public class ElapsedTimeImageElementAPIC extends Element {
	@Find(by = FBy.cssSelector, value = "div[title='Front-End Request (Network)']")
	WebElement front_end_request;
	@Find(by = FBy.cssSelector, value = "div[title='Gateway Processing']")
	WebElement gateway_processing;
//	@Find(by = FBy.cssSelector, value = "div[title='Back-End Request (Network)']")
//	WebElement back_end_request;
	@Find(by = FBy.cssSelector, value = "div[title='Back-End Processing']")
	WebElement back_end_processing;
	@Find(by = FBy.cssSelector, value = "div[title='Front-End Response (Network)']")
	WebElement back_end_response;
//	@Find(by = FBy.cssSelector, value = "div[title='DataPower Response Processing']")
//	WebElement dataPower_response_processing;
//	@Find(by = FBy.cssSelector, value = "div[title='Back-End Response (Network)']")
//	WebElement front_end_response;
	@Find(by = FBy.xpath, value = ".//div[contains(@title, 'Front-End Request (Network)')]/parent::div//div[@title!='']")
	ByPath elapsed_times;

	public ElapsedTimeImageElementAPIC(WebElement base_elem) {
		super(base_elem);

	}

	public int frontEndRequestVal() {
		return TestUtils.StringUtils.parseMS(front_end_request.getText());
	}

	public int dataPowerRequestProcessingVal() {
		return TestUtils.StringUtils.parseMS(gateway_processing.getText());
	}

//	public int backEndRequestVal() {
//		return TestUtils.StringUtils.parseMS(back_end_request.getText());
//	}

	public int backProcessingVal() {
		return TestUtils.StringUtils.parseMS(back_end_processing.getText());
	}

	public int backEndResponseVal() {
		return TestUtils.StringUtils.parseMS(back_end_response.getText());
	}

//	public int dataPowerResponeseProcessingVal() {
//		return TestUtils.StringUtils.parseMS(dataPower_response_processing.getText());
//	}

//	public int frontEndResponseVal() {
//		return TestUtils.StringUtils.parseMS(front_end_response.getText());
//	}

	public Integer getSumOfElapsedTime() {
		Integer elapsed_time = 0;
		Integer time;
		for (WebElement elem : this.findElements(elapsed_times)) {
			time = TestUtils.StringUtils.parseMS(elem.getText());
			Log.info(elem.getAttribute("title") + " = " + time);
			elapsed_time += time;
		}
		return elapsed_time;
	}
}
