package org.montier.qa.automation.pages.apic;

import org.montier.qa.automation.base.UsersRoles;
import org.montier.qa.automation.pages.RoleViewPage.Permissions;
import org.montier.qa.automation.pages.base.APICAbstractTransactionPage;
import org.montier.qa.automation.toolkit.elements.TransactionComponents.RawMessages;

public class APICTransactionRawMessagesPage extends APICAbstractTransactionPage {
	public APICTransactionRawMessagesPage() {
		super();
	}

	@Override
	public void initPageElement() {
		super.initPageElement();
		if (UsersRoles.getPermissionOfCurrentUser(Permissions.AllowRawMessages))
			this.transactionComponents.rawMessages = (RawMessages) this.transactionComponents.getTabContent(RawMessages.class);
	}

	@Override
	public RawMessages getComponentPanel() {
		return this.transactionComponents.rawMessages;
	}
}
