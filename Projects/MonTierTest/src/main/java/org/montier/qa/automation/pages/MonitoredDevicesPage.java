package org.montier.qa.automation.pages;

import java.util.Arrays;

import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;

public class MonitoredDevicesPage extends PageObject {
	public static class GatewaysCols extends TableCols {
		public static Col NAME = new Col("Name");
		public static Col HOST = new Col("Host");
		public static Col SOMA_PORT = new Col("SOMA Port");
		public static Col LOG_TARGET_SOURCE = new Col("Log Target Source");
		public static Col DEVICE_RESOURCES_MONITORING = new Col("Device Resources Monitoring");
		public static Col SERVICE_RESOURCES_MONITORING = new Col("Service Resources Monitoring");

		public GatewaysCols() {
			super();
		}

		@Override
		public void setColsIndices() {
			colsIndices.addAll(Arrays.asList(NAME, HOST, SOMA_PORT, LOG_TARGET_SOURCE, DEVICE_RESOURCES_MONITORING, SERVICE_RESOURCES_MONITORING));
		}
	}

	@Find(by = FBy.className, value = "WdpDevicesTable", init = true)
	private ByPath wdp_devices_table;

	public Table wdpDevicesTable;

	public MonitoredDevicesPage() {
		super();
	}

	@Override
	public PageFilters getPageFilters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void initPageElement() {
		this.wdpDevicesTable = new Table(this.findElement(wdp_devices_table), new GatewaysCols());
	}

}
