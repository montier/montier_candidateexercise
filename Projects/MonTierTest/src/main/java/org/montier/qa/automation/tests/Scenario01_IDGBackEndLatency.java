package org.montier.qa.automation.tests;

import org.montier.qa.automation.base.Driver;
import org.montier.qa.automation.base.FunctionalTest;
import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.testutils.TimeUtils;
import org.montier.qa.automation.base.utils.IConditions;
import org.montier.qa.automation.base.utils.Table.TableRow;
import org.montier.qa.automation.elements.SideMenu.MenuSelect;
import org.montier.qa.automation.pages.AlertAddPage;
import org.montier.qa.automation.pages.base.IDGAbstractTransactionPage.TransRawVal;
import org.montier.qa.automation.pages.idg.IDGServiceConfigurationPage;
import org.montier.qa.automation.pages.idg.IDGServiceConfigurationPage.SrvcCngDtlCols;
import org.montier.qa.automation.pages.idg.IDGServiceLatencyPage;
import org.montier.qa.automation.pages.idg.IDGServiceURICalls;
import org.montier.qa.automation.pages.idg.IDGServiceURICalls.SrvcURICols;
import org.montier.qa.automation.pages.idg.IDGServicesConfiguration;
import org.montier.qa.automation.pages.idg.IDGServicesConfiguration.SrvCnfCols;
import org.montier.qa.automation.pages.idg.IDGTransactionsPage;
import org.montier.qa.automation.pages.idg.IDGTransactionsPage.IDGTransCols;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Scenario01_IDGBackEndLatency extends FunctionalTest {
//	@Override
//	protected void addValidations() {
//		validationValues.add("domain", "BankB_Domain");
//		validationValues.add("service", "MakeAppointment_WSS.WSP");
//	}

	IDGServiceLatencyPage srvc_latency_page;
	int trans_lat;
	IDGServiceConfigurationPage detailsPage;
	String trans_time;
	int latency = 1700;
	String service_name = "MakeAppointment_WSS.WSP";

	@BeforeMethod
	public void deleteExistingAlerts() {
		// AlertActions.deleteAlert(service_name, false);
		Driver.getInstance().jsonChartActivte();
	}

	@Test
	public void IDGBackEndLatency() {
		Log.description("Test goal: Troubleshooting a back-end latency issue");

		this.step("Open service latency page", () -> {
			srvc_latency_page = (IDGServiceLatencyPage) MenuSelect.Dashboards_Analytics_ServiceLatency.getPage();
			Log.instep("Filter page for service " + service_name + " of last hour");
			srvc_latency_page.getPageFilters().TIME.applyFilter("Last Hour");
			srvc_latency_page.getPageFilters().DEVICE.select(0);
			srvc_latency_page.getPageFilters().DOMAIN.applyFilter("BankB_Domain");
			srvc_latency_page.getPageFilters().SERVICE.applyFilter("MakeAppointment_WSS.WSP");
			// srvc_latency_page.getPageFilters().SERVICE.applyFilter("BackendForHTTPServices.XMLFW");
			srvc_latency_page.refresh();
		});

//		this.step("Check Total Latency of service " + service_name + " higher than " + latency + " for EVERY EXECUTION at least 5 minutes", () -> {
//			srvc_latency_page.totalLatencyChart.parseChartLines();
//			int seconds_range = srvc_latency_page.totalLatencyChart.timeRangeValueHigherThan(service_name, (float) latency);
//			Log.instep("range length is " + seconds_range / 60 + " Minutes");
//			Assert.assertTrue(seconds_range > 5 * 60, "Found range of " + seconds_range / 60 + " minutes, less than 5 minutes as expected");
//		});
//
//		this.step("Check that problem caused by Backend and latency higher than " + latency, () -> {
//			srvc_latency_page.BELatencyChart.parseChartLines();
//			Log.instep("Assert the highest point in Backend Latency Chart higher than " + latency);
//			Pair<Date, Float> highest_point = srvc_latency_page.BELatencyChart.getHighestValue(service_name);
//			Log.info("Highest point value is " + highest_point);
//			Assert.assertTrue(highest_point.second() > latency, "highest point of BackEnd chart lower than latency");
//		});

		this.step("Drill down to investigate transactions of service " + validationValues.get("service") + " with high latency", () -> {
			Log.instep("Open Transactions page and look for transaction with high latency");
			IDGTransactionsPage trnsPage = (IDGTransactionsPage) MenuSelect.Investigate.getPage();
			Assert.assertTrue(trnsPage.transTable.table.getNumRows() >= 10, "Not enough transactions to work with");
			// TableRow row =
			// trnsPage.transTable.getRowValueGreaterThan(IDGTransCols.ELAPSED, latency);
			TableRow row = trnsPage.transTable.table.findRowSatisfies("find_first", true,
					IConditions.getGreaterThanCondition(IDGTransCols.ELAPSED, latency));
			trans_lat = Integer.parseInt(row.getValue(IDGTransCols.ELAPSED));
			Assert.assertTrue(trans_lat > latency, "Error row found, latency is " + trans_lat + " lower than " + latency);
			Log.info("Found transaction id " + row.getValue(IDGTransCols.TRANS_ID) + " with latency " + trans_lat);

			Log.instep("`Open transaction of service " + service_name + " with latency > " + latency);
			trnsPage.transTable.clickTransID(row);
		});

		this.step("Check transaction details", () -> {
			Log.instep("Validate no error in the transaction Error Analysis section");
			trans_time = pages.idg.transRawMessages.transDetails.getValueOf(TransRawVal.TIME);
			Assert.assertEquals(pages.idg.transRawMessages.transactionAnalysisPanel.getErrorAnalysis(), "No errors detected.");

			Log.instep("Ensure sum elapsed time of transaction same as transaction latency " + trans_lat);
			Assert.assertTrue(pages.idg.transRawMessages.transactionAnalysisPanel.elapsedTimeImage.getSumOfElapsedTime() == pages.idg.transRawMessages
					.getElapsedVal(), "sum elapsed time is not same as transaction latency");
		});

		this.step("Check service type", () -> {
			Log.instep("Open Service Configuration page");
			IDGServicesConfiguration srvcCnfgPage = (IDGServicesConfiguration) MenuSelect.Explore.getPage();
			srvcCnfgPage.getPageFilters().SERVICE.applyFilter("MakeAppointment_WSS.WSP");

			Log.instep("Check service type is WSGateway (soap-11) and WSGateway (soap-12)");
			srvcCnfgPage.refresh();
			Assert.assertTrue(srvcCnfgPage.srvcCnfgTable.getNumRows() == 2, "There are no 2 types of services");
			detailsPage = this.checkServiceTypeAndClick(srvcCnfgPage);
		});

		this.step("Look for changes in configuration of service " + service_name, () -> {
			int conf_update_interval = 100;
			Log.instep("check that remote uri was changed about " + conf_update_interval + " minutes before transaction time");
			TableRow uri_changed_row = detailsPage.changeAuditTable.getRowValueEqual(SrvcCngDtlCols.DESCRIPTION,
					"Remote URI was changed from /MakeAppointment_WSS/Service.asmx to /MakeAppointment_WSS_V2/Service.asmx.");
			Assert.assertNotNull(uri_changed_row);

			Assert.assertTrue(TimeUtils.checkTimeInRange(uri_changed_row.getValue(SrvcCngDtlCols.TIME), conf_update_interval * 60, trans_time),
					"URI changes at " + uri_changed_row.getValue(SrvcCngDtlCols.TIME) + " but expected for up to hour after " + trans_time);

			Log.instep("check that remote port was changed about " + conf_update_interval + " minutes before transaction time");
			TableRow port_changed_row = detailsPage.changeAuditTable.getRow(uri_changed_row.index + 1);
			Assert.assertEquals(port_changed_row.getValue(SrvcCngDtlCols.DESCRIPTION), "Remote Port was changed from 2552 to 2560.");
			Assert.assertTrue(TimeUtils.checkTimeInRange(uri_changed_row.getValue(SrvcCngDtlCols.TIME), conf_update_interval * 60, trans_time));
		});

		this.step("Add Alert on service when average latency is too high", () -> {
			IDGServiceURICalls serviceURICalls = (IDGServiceURICalls) MenuSelect.Dashboards_Analytics_Service_URI_Calls.getPage();
			serviceURICalls.getPageFilters().TIME.applyFilter("Last Hour");
			serviceURICalls.getPageFilters().SERVICE.applyFilter("MakeAppointment_WSS.WSP");
			serviceURICalls.refresh();

			Log.instep("Click on Add alert on average latency column of service");
			TableRow srvrow = serviceURICalls.serviceURICallsTable.getRow(0);
			int avg_ms = Integer.valueOf(srvrow.getValue(SrvcURICols.AVG_MS));
			Log.info("Click on Average ms = " + avg_ms);
			AlertAddPage addAlert = serviceURICalls.clickAddAlert(srvrow, SrvcURICols.AVG_MS);
			addAlert.openPage();
			Log.instep("Verify alert automatic fields");
			Assert.assertTrue(addAlert.isValueContains("Name", service_name));
			Assert.assertTrue(addAlert.isValueContains("Description", "Number of transactions with latency greater than " + avg_ms + " ms"));
			Assert.assertTrue(addAlert.isValueContains("Service Field Override", "WDPObjectName"));
			addAlert.clickDetails();
			Assert.assertTrue(addAlert.getQueryDetails().contains("\"gte\":" + avg_ms));

			// TODO: Error on adding alert
//			Log.instep("Add Alert and check existing on alerts list");
//			AlertActions.AddAlert(addAlert, "Service URI Latency MakeAppointment_WSS.WSP");
//			addAlert.switchToParentWindow();
			// Driver.getInstance().switchToParentHandler();
		});
	}

	public IDGServiceConfigurationPage checkServiceTypeAndClick(IDGServicesConfiguration srvcCnfgPage) {
		String soap_format = "WSGateway (soap-%d)";
		TableRow service12_row = srvcCnfgPage.srvcCnfgTable.getRowValueEqual(SrvCnfCols.SERVICE_TYPE, String.format(soap_format, 12));
		TableRow service11_row = srvcCnfgPage.srvcCnfgTable.getRow(service12_row.index == 0 ? 1 : 0);
		Log.info("Check 2 kinds of soap services exists " + String.format(soap_format, 12) + "and " + String.format(soap_format, 11));
		Assert.assertEquals(service11_row.getValue(SrvCnfCols.SERVICE_TYPE), String.format(soap_format, 11));
		Log.instep("Choose service type " + String.format(soap_format, 12));
		service12_row.click();
		pages.idg.confDetails.openPage();
		return pages.idg.confDetails;
	}
}
