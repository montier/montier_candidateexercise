package org.montier.qa.automation.pages;

import java.util.Arrays;

import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.openqa.selenium.WebElement;

public class AlertsSetupPage extends PageObject {
	public static class AlertCols extends TableCols {
		public static final Col ENABLED = new Col("Enabled");
		public static final Col SYSTEM_HEALTH = new Col("System Health Metric");
		public static final Col NAME = new Col("Name");
		public static final Col DESCRIPTION = new Col("Description");
		public static final Col SCHEDULE = new Col("Schedule");
		public static final Col RECIPIENTS = new Col("Recipients");

		@Override
		public void setColsIndices() {
			this.colsIndices.addAll(Arrays.asList(ENABLED, SYSTEM_HEALTH, NAME, DESCRIPTION, SCHEDULE, RECIPIENTS));
		}
	}

	@Find(by = FBy.xpath, value = "//table")
	WebElement table;
	public Table alertsSetupTable;

	public AlertsSetupPage() {
		super();
	}

	@Override
	public void initPageElement() {
		this.alertsSetupTable = new Table(table, new AlertCols());
	}

	@Override
	public PageFilters getPageFilters() {
		return null;
	}

}
