package org.montier.qa.automation.pages.idg;

import java.util.Arrays;

import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.pages.base.IDGAbstractInvestigatePage;
import org.montier.qa.automation.toolkit.elements.TransactionsTable;
import org.montier.qa.automation.toolkit.elements.TransactionsTable.TransCols;

/*

public abstract class BaseTransactions extends AbstractInvestigatePage {
	public abstract static class TransCols extends TableCols{
		public static final Col TIME = new Col("Time");
		public static final Col DEVICE = new Col("Device");
		public static final Col STATUS = new Col("Status");
		public static final Col TRANS_ID = new Col("Trans. ID");
		public static final Col CLIENT_IP = new Col("Client IP");
		public static final Col GL_TRANS_ID = new Col("Gl. Trans. ID");
		public static final Col ELAPSED = new Col("Elapsed (ms)");
		public static final Col PAYLOAD = new Col("Payload");


		public TransCols(){
			super();
		}
	}

	@Find(by = FBy.xpath, value = "//table")
	protected WebElement transactions_table;
	public Table transTable;

	public BaseTransactions() {
		super();
	}

	public PageObject clickTransID(TableRow row) {
		String trans_id_text = row.getCeil(TransCols.TRANS_ID).getText();
		Log.info("Click on Trnas.ID " + trans_id_text);
		row.getCeil(TransCols.TRANS_ID).click();
		PagesFactory.getPages().getTransRawMessagesPage().openPage(trans_id_text);
		return PagesFactory.getPages().getTransRawMessagesPage();
	}
}
*/
public class IDGTransactionsPage extends IDGAbstractInvestigatePage {
	public static final String PAGE_NAME = "Transactions";
	public static final String URL = "idgTransactions:-productView";

	public static class IDGTransCols extends TransCols {
		public static final Col SERVICE = new Col("Service Name");
		public static final Col OPERATION_URI = new Col("Operation/URI");
		public static final Col DOMAIN = new Col("Domain");

		public IDGTransCols() {
			super();
		}

		@Override
		public void setColsIndices() {
			colsIndices
					.addAll(Arrays.asList(SERVICE, OPERATION_URI, TIME, DEVICE, DOMAIN, STATUS, TRANS_ID, CLIENT_IP, GL_TRANS_ID, ELAPSED, PAYLOAD));
		}
	}

	public static class IDGTransactionsFilters extends PageFilters {
		public FilterUtils FREETEXT = new FilterUtils("Apply");
		public FilterUtils TIME = new FilterUtils("Time");
		public FilterUtils DEVICE = new FilterUtils("Device");
		public FilterUtils DOMAIN = new FilterUtils("Domain");
		public FilterUtils SERVICE = new FilterUtils("Service");
		public FilterUtils STATUS = new FilterUtils("Status");
		public FilterUtils TRANS_ID = new FilterUtils("Trans. ID");
		public FilterUtils MIN_REQUEST_SIZE = new FilterUtils("Min Request Size");
		public FilterUtils MIN_RESPONSE_SIZE = new FilterUtils("Min Response Size");
		public FilterUtils CLIENT_IP = new FilterUtils("Client IP");
		public FilterUtils PAYLOAD = new FilterUtils("Payload");
	}

	public TransactionsTable transTable;

	public IDGTransactionsPage() {
		super();
	}

	@Override
	public void initPageElement() {
		super.initPageElement();
		this.transTable = new TransactionsTable(new IDGTransCols());
	}

	@Override
	public IDGTransactionsFilters getPageFilters() {
		if (this.pageFilters == null)
			this.pageFilters = new IDGTransactionsFilters();
		return (IDGTransactionsFilters) this.pageFilters;
	}
}
