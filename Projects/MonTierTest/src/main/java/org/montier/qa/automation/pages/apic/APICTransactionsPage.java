package org.montier.qa.automation.pages.apic;

import java.util.Arrays;

import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.FilterUtils.FilterStatus;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.pages.base.APICAbstractInvestigatePage;
import org.montier.qa.automation.toolkit.elements.TransactionsTable;
import org.montier.qa.automation.toolkit.elements.TransactionsTable.TransCols;

public class APICTransactionsPage extends APICAbstractInvestigatePage {

	public static class APICTransCols extends TransCols {
		public static Col CATALOG_SPACE = new Col("Catalog/Space");
		public static Col PRODUCT = new Col("Product");
		public static Col PLAN = new Col("Plan");
		public static Col API = new Col("API");
		public static Col APP_NAME = new Col("App Name");
		public static Col URI = new Col("URI");
		public static Col METHOD = new Col("Method");

		public APICTransCols() {
			super();
		}

		@Override
		public void setColsIndices() {
			colsIndices.addAll(Arrays.asList(TIME, DEVICE, CATALOG_SPACE, PRODUCT, PLAN, API, APP_NAME, METHOD, URI,
					STATUS, TRANS_ID, CLIENT_IP, ELAPSED, PAYLOAD));
		}
	}

	public class APICTransactionsFilters extends PageFilters {
		public FilterUtils FREETEXT = new FilterUtils("Apply");
		public FilterUtils TIME = new FilterUtils("Time");
		public FilterUtils DEVICE = new FilterUtils("Device");
		public FilterUtils DOMAIN = new FilterUtils("Domain");
		public FilterUtils CATALOG = new FilterUtils("Catalog");
		public FilterUtils SPACE = new FilterUtils("Space");
		public FilterUtils PRODUCT = new FilterUtils("Product");
		public FilterUtils PLAN = new FilterUtils("Plan");
		public FilterUtils API_NAME = new FilterUtils("API Name");
		public FilterUtils API_VERSION = new FilterUtils("API Version");
		public FilterUtils APP_NAME = new FilterUtils("App Name");
		public FilterUtils GATEWAY_TYPE = new FilterUtils("Gateway Type");
		public FilterUtils STATUS = new FilterUtils("Status");
		public FilterUtils TRANS_ID = new FilterUtils("Trans. ID");
		public FilterUtils GLOBAL_TRANS_ID = new FilterUtils("Global Trans. ID");
		public FilterUtils HTTP_METHOD = new FilterUtils("HTTP Method", FilterStatus.LESS);
		public FilterUtils IN_URL = new FilterUtils("In URL", FilterStatus.LESS);
		public FilterUtils IN_URI = new FilterUtils("In URI", FilterStatus.LESS);
		public FilterUtils FE_HTTP_RES_CODE = new FilterUtils("FE HTTP Res. Code", FilterStatus.LESS);
//		public FilterUtils BE_HTTP_RES_CODE = new FilterUtils("BE HTTP Res. Code", FilterStatus.LESS);
		public FilterUtils CONSUMER_ORG_NAME = new FilterUtils("Consumer Org Name", FilterStatus.LESS);
		public FilterUtils CLIENT_ID = new FilterUtils("Client ID", FilterStatus.LESS);
		public FilterUtils CLIENT_IP = new FilterUtils("Client IP", FilterStatus.LESS);
		public FilterUtils OAUTH_sCOPE = new FilterUtils("OAuth Scope", FilterStatus.LESS);
		public FilterUtils OAUTH_RESOURCE_OWNER = new FilterUtils("OAuth Resource Owner", FilterStatus.LESS);
		public FilterUtils OAUTH_TOKEN_VALID_FROM = new FilterUtils("OAuth Token Valid From", FilterStatus.LESS);
		public FilterUtils OAUTH_TOKEN_VALID_UNTIL = new FilterUtils("OAuth Token Valid Until", FilterStatus.LESS);
		public FilterUtils MIN_REQUEST_LATENCY = new FilterUtils("Min Request Latency", FilterStatus.LESS);
		public FilterUtils MIN_RESPONSE_LATENCY = new FilterUtils("Min Response Latency", FilterStatus.LESS);
		public FilterUtils MIN_BACKEND_LATENCY = new FilterUtils("Min Back-End Latency", FilterStatus.LESS);
		public FilterUtils MIN_TOTAL_LATENCY = new FilterUtils("Min Total Latency", FilterStatus.LESS);
		public FilterUtils MIN_REQUEST_SIZE = new FilterUtils("Min Request Size", FilterStatus.LESS);
		public FilterUtils MIN_RESPONSE_SIZE = new FilterUtils("Min Response Size", FilterStatus.LESS);
		public FilterUtils ERROR_REASON = new FilterUtils("Error Reason", FilterStatus.LESS);
		public FilterUtils ERROR_MESSAGE = new FilterUtils("Error Message", FilterStatus.LESS);
	}

	public TransactionsTable transTable;

	public APICTransactionsPage() {
		super();
	}

	@Override
	public void initPageElement() {
		super.initPageElement();
		this.transTable = new TransactionsTable(new APICTransCols());
	}

	@Override
	public APICTransactionsFilters getPageFilters() {
		if (this.pageFilters == null)
			this.pageFilters = new APICTransactionsFilters();
		return (APICTransactionsFilters) this.pageFilters;
	}

}
