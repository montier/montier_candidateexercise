package org.montier.qa.automation.base;

import java.util.ArrayList;
import java.util.List;

import org.testng.IConfigurationListener;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

import lombok.Getter;

public class IListener {
	public static enum ResultStatus {
		FAIL, SKIP, SUCCESS;
	}

	public static class TestResultRow {
		String testName;
		ResultStatus status;
		String failureMessage;
	}

	List<TestResultRow> resultRows = new ArrayList<TestResultRow>();

	public static class MyMethodListener implements IInvokedMethodListener {
		private static @Getter Throwable methodThrowable = null;

		@Override
		public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {//
			if (Log.log != null)
				Log.debug("Start " + testResult.getMethod().getMethodName());
		}

		@Override
		public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
			Log.debug("End " + testResult.getMethod().getMethodName());
		}
	}

	public static class MyConfigMethodListener implements IConfigurationListener {
		private static @Getter Throwable configurationThrowable = null;

		@Override
		public void onConfigurationSuccess(ITestResult itr) {//

		}

		@Override
		public void onConfigurationFailure(ITestResult itr) {
			// itr.getInstanceName()
			Log.info("Failed while configuration method " + itr.getMethod().getMethodName());
			configurationThrowable = itr.getThrowable();
			Log.info(FunctionalTest.getFailureMessage(itr));
			FunctionalTest.saveScreenshot();
		}

		@Override
		public void onConfigurationSkip(ITestResult itr) {
			Log.info("Skip " + itr.getMethod().getMethodName());
		}
	}
}
