package org.montier.qa.automation.base;

import org.montier.qa.automation.elements.TopPanel.ProductType;

import lombok.Getter;

public class PagesFactory {
	public APICPages apic;
	public IDGPages idg;

	private static PagesFactory INSTANCE = null;
	private @Getter ProductType product = ProductType.IDG;
	public PageObject currentPage;

	PagesFactory() {
		apic = new APICPages();
		idg = new IDGPages();
		currentPage = idg.getProductDefaultPage(product);
		product = ProductType.IDG;
	}

	public static PagesFactory getInstance() {
		if (INSTANCE == null)
			INSTANCE = new PagesFactory();
		return INSTANCE;
	}

	public static PagesContainer getPages() {
		switch (INSTANCE.product) {
		case APIC:
			return INSTANCE.apic;
		case IDG:
			return INSTANCE.idg;
		}
		return INSTANCE.idg;
	}

	public static void changeProduct(ProductType product_type) {
		Log.debug("Change product to " + product_type);
		INSTANCE.product = product_type;
		getPages().getProductDefaultPage(product_type).openPage();
		Log.debug("product changed to " + product_type);
	}
}
