package org.montier.qa.automation.pages.idg;

import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.UsersRoles;
import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.montier.qa.automation.elements.PercentileSummaryPanelElement;
import org.openqa.selenium.WebElement;

public class IDGRecentActivityPage extends PageObject {
	public class IDGRecentActivityFilters extends PageFilters {
		public FilterUtils TIME = new FilterUtils("Time");
		public FilterUtils DEVICE = new FilterUtils("Device");
		public FilterUtils DOMAIN = new FilterUtils("Domain");
		public FilterUtils CLIENT_IP = new FilterUtils("Client IP");
	}

	public static class DomainRequestSizeCols extends TableCols {
		public static final Col DOMAIN = new Col("Domain");
		public static final Col REQUEST_SIZE = new Col("Request Size");
	}

	public static class DomainActivityCols extends TableCols {
		public static final Col DOMAIN = new Col("Domain");
		public static final Col SUCCESSFUL_TRANSACTIONS = new Col("Successful Transactions");
	}

	public static class ServiceErrorsCols extends TableCols {
		public static final Col SERVICE = new Col("Service");
		public static final Col ERRORS = new Col("Errors");
	}

	@Find(by = FBy.className, value = "TrafficSummaryPanel")
	WebElement traffic_summary_panel;
	@Find(by = FBy.className, value = "SuccessfulTransactionsSummaryPanel")
	WebElement successful_transactions_summary_panel;
	@Find(by = FBy.className, value = "ErrorsSummaryPanel")
	WebElement errors_summary_panel;
	@Find(by = FBy.className, value = "DomainTrafficGrid")
	WebElement domain_request_table;
	@Find(by = FBy.className, value = "DomainActivityGrid")
	WebElement domain_activity_table;
	@Find(by = FBy.className, value = "ServiceErrorsGrid")
	WebElement service_errors_table;

	public PercentileSummaryPanelElement trafficSummaryPanel;
	public PercentileSummaryPanelElement successfulTransactionsSummaryPanel;
	public PercentileSummaryPanelElement errorsSummaryPanel;
	public Table domainRequestTable;
	public Table domainActivityTable;
	public Table serviceErrorsTable;

	public IDGRecentActivityPage() {
		super();
	}

	public boolean validateContentByUserPermission() {
		Boolean cond = validateTableContent(domainRequestTable, DomainRequestSizeCols.DOMAIN, "Domain")
				&& validateTableContent(domainActivityTable, DomainActivityCols.DOMAIN, "Domain")
				&& validateTableContent(serviceErrorsTable, ServiceErrorsCols.SERVICE, "Service");
		return cond;
	}

	private boolean validateTableContent(Table table, Col col, String resource) {
		for (int i = 0; i < table.getNumRows(); i++)
			if (!UsersRoles.isResourceAllowedForCurrentUser(resource, table.getValueOf(i, col))) {
				Log.error("Line number " + i + 1 + " contains " + resource + " " + table.getValueOf(i, col) + " and its not allowed by user");
				return false;
			}
		return true;
	}

	@Override
	public void initPageElement() {
		trafficSummaryPanel = new PercentileSummaryPanelElement(traffic_summary_panel);
		successfulTransactionsSummaryPanel = new PercentileSummaryPanelElement(successful_transactions_summary_panel);
		errorsSummaryPanel = new PercentileSummaryPanelElement(errors_summary_panel);
		this.domainRequestTable = new Table(domain_request_table, new DomainRequestSizeCols());
		this.domainActivityTable = new Table(domain_activity_table, new DomainActivityCols());
		this.serviceErrorsTable = new Table(service_errors_table, new ServiceErrorsCols());
	}

	@Override
	public IDGRecentActivityFilters getPageFilters() {
		if (this.pageFilters == null)
			this.pageFilters = new IDGRecentActivityFilters();
		return (IDGRecentActivityFilters) this.pageFilters;
	}

}
