package org.montier.qa.automation.pages.idg;

import java.util.Arrays;

import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.PagesFactory;
import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.montier.qa.automation.base.utils.Table.TableRow;
import org.montier.qa.automation.pages.base.IDGAbstractInvestigatePage;
import org.openqa.selenium.WebElement;

public class IDGEarlyFailedPage extends IDGAbstractInvestigatePage {
	public static class EarlyFailCols extends TableCols {
		public static final Col FSH_PROXY = new Col("FSH/Proxy");
		public static final Col OPERATION_URI = new Col("Operation/URI");
		public static final Col TIME = new Col("Time");
		public static final Col DEVICE = new Col("Device");
		public static final Col DOMAIN = new Col("Domain");
		public static final Col TRANS_ID = new Col("Trans. ID");
		public static final Col CLIENT_IP = new Col("Client IP");

		public EarlyFailCols() {
			super();
		}

		@Override
		public void setColsIndices() {
			colsIndices.addAll(Arrays.asList(FSH_PROXY, OPERATION_URI, TIME, DEVICE, DOMAIN, TRANS_ID, CLIENT_IP));
		}
	}

	public class EarlyFailingFilters extends PageFilters {
		public FilterUtils FREETEXT = new FilterUtils("Apply");
		public FilterUtils TIME = new FilterUtils("Time");
		public FilterUtils DEVICE = new FilterUtils("Device");
		public FilterUtils DOMAIN = new FilterUtils("Domain");
		public FilterUtils FSH_PROXY = new FilterUtils("FSH/Proxy");
		public FilterUtils TRANS_ID = new FilterUtils("Trans. ID");
		public FilterUtils CLIENT_IP = new FilterUtils("Client IP");

	}

	@Find(by = FBy.className, value = "EarlyFailedTransactionsGrid")
	WebElement earlyFailingRequests;

	public Table earlyFailTable;

	public IDGEarlyFailedPage() {
		super();

	}

	@Override
	public EarlyFailingFilters getPageFilters() {
		if (this.pageFilters == null)
			this.pageFilters = new EarlyFailingFilters();
		return (EarlyFailingFilters) this.pageFilters;
	}

	@Override
	public void initPageElement() {
		super.initPageElement();
		this.earlyFailTable = new Table(earlyFailingRequests, new EarlyFailCols());
	}

	public PageObject clickTransID(TableRow row) {
		String trans_id_text = row.getCeil(EarlyFailCols.TRANS_ID).getText();
		Log.info("Click on Trnas.ID " + trans_id_text);
		row.getCeil(EarlyFailCols.TRANS_ID).click();
		PagesFactory.getPages().getTransRawMessagesPage().openPage(trans_id_text);
		getFilterElem().addFilterToCurrent("Trans. ID", trans_id_text);
		return PagesFactory.getPages().getTransRawMessagesPage();
	}

}
