package org.montier.qa.automation.pages.apic;

import org.montier.qa.automation.pages.base.APICAbstractTransactionPage;
import org.montier.qa.automation.toolkit.elements.TransactionComponents.ExtendedLatency;

public class APICTransactionExtendedLatencyPage extends APICAbstractTransactionPage {
	public APICTransactionExtendedLatencyPage() {
		super();
	}

	@Override
	public void initPageElement() {
		super.initPageElement();
		this.transactionComponents.extendedLatency = (ExtendedLatency) this.transactionComponents.getTabContent(ExtendedLatency.class);
	}

	@Override
	public ExtendedLatency getComponentPanel() {
		return this.transactionComponents.extendedLatency;
	}
}
