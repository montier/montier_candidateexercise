package org.montier.qa.automation.tests;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeMap;

import org.montier.qa.automation.base.FunctionalTest;
import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.testutils.TestUtils;
import org.montier.qa.automation.base.utils.Table.TableRow;
import org.montier.qa.automation.elements.SideMenu;
import org.montier.qa.automation.elements.TopPanel;
import org.montier.qa.automation.elements.TopPanel.ProductType;
import org.montier.qa.automation.pages.apic.APICEarlyFailedPage.APICEarlyFailedTransCols;
import org.montier.qa.automation.pages.apic.APICTransactionsPage.APICTransCols;
import org.montier.qa.automation.pages.base.APICAbstractTransactionPage.APICTransRawVal;
import org.montier.qa.automation.toolkit.elements.RequestFlowElement.ApicPolicy;
import org.montier.qa.automation.toolkit.elements.TransactionComponents.SideCalls.APICSideCallsCols;
import org.montier.qa.automation.toolkit.elements.TransactionComponents.SideCalls.SideCallsCols;
import org.montier.qa.automation.toolkit.elements.TransactionsTable.TransCols;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Scenario10_APICConnectProblems extends FunctionalTest {

	TreeMap<APICTransRawVal, String> value = new TreeMap<>();
	LinkedHashMap<String, ApicPolicy> trans_policies_data = new LinkedHashMap<>();
	LinkedHashMap<String, ApicPolicy> respons_policies_data = new LinkedHashMap<>();
	String elapsed_time;
	HashMap<String, List<TreeMap<String, String>>> policy;
	String details_context_variable = null;
	List<TreeMap<String, String>> context_variable = null;
	int policies_ms;
	int image_ms;

	@BeforeClass
	public void enterAPIC() {
		defineAccountsTransactionPolicies();
		defineAccountsResponsePolicies();
		this.step("Select API-C product", () -> {
			TopPanel.getInstance().selectProduct(ProductType.APIC);

		});

	}

	@BeforeMethod
	public void openTransactionsAndClean() {
		PageObject.getFilterElem().resetFilters(true, false);
		SideMenu.MenuSelect.Investigate.getPage();

	}

	private void defineAccountsTransactionPolicies() {
		String[] names = { "activity-log", "validate", "getBalance: input", "getBalance: invoke", "getBalance: output",
				"GetcustomerBalance", "switch", "map" };// "oauth2-server"
		String[] types = { "activitylog", "validate", "map", "invoke", "map", "apim.setvariable", "switch", "map" };// "oauth2-server",

		for (int i = 0; i < types.length; i++) {
			trans_policies_data.put(names[i], new ApicPolicy(names[i], types[i], "REQUEST"));
		}

		trans_policies_data.get("getBalance: invoke").addSideCall("HTTP", "OK", null,
				"HTTP response code 200 for 'http://172.17.100.70:2055/services/AccountService'");
		for (int i = 5; i < names.length; i++) {
			trans_policies_data.get(names[i]).addContextVariable("CustomerBalance", "-99999");
		}
	}

	private void defineAccountsResponsePolicies() {
		String[] names = { "catch", "xslt-returnErrorToUser" };
		String[] types = { "catch", "xslt" };

		for (int i = 0; i < types.length; i++) {
			respons_policies_data.put(names[i], new ApicPolicy(names[i], types[i], "RESPONSE"));
		}
	}

	@Test(priority = 0)
	public void earlyFailingWrongURL_1() {

		value.put(APICTransRawVal.IN_URL,
				"https://172.17.100.70:443/montierproviderorg508/montiercatalog/accounts-service/balance");
		value.put(APICTransRawVal.API_NAME, "Unknown");
		value.put(APICTransRawVal.STATUS, "ERROR");
		value.put(APICTransRawVal.QUEARY_STRING, "customer_id=7777778");
		value.put(APICTransRawVal.PROVIDER_ORG, "montierproviderorg508");
		value.put(APICTransRawVal.API_VERSION, "Unknown");

		this.step("Open Early Failing page and filter url path " + value.get(APICTransRawVal.IN_URL), () -> {
			pages.apic.trans.tabsElem.selectTab("Early Failing");
			pages.apic.earlyFailedPage.getPageFilters().IN_URL.applyFilter(value.get(APICTransRawVal.IN_URL));
			pages.apic.earlyFailedPage.refresh();
		});
		this.step("Drill down to one of the transactions", () -> {
			TableRow row = pages.apic.earlyFailedPage.transTable.getRow(1);

			Log.step("Assert API Name is UnKnown");
			String name = row.getValue(APICEarlyFailedTransCols.API_NAME);
			Assert.assertTrue(name.equals(value.get(APICTransRawVal.API_NAME)));

			Log.info("Get all row values");
			String device = row.getValue(APICEarlyFailedTransCols.DEVICE);
			String domain = row.getValue(APICEarlyFailedTransCols.DOMAIN);
			String Transaction_id = row.getValue(APICEarlyFailedTransCols.TRANS_ID);
			String catalog_name = row.getValue(APICEarlyFailedTransCols.CATALOG_NAME);
			String client_ip = row.getValue(APICEarlyFailedTransCols.CLIENT_IP);

			Log.info("Found transaction id " + row.getValue(APICEarlyFailedTransCols.TRANS_ID));
			pages.apic.earlyFailedPage.clickTransID(row);

			Log.info("Assert all row values");
			String actual_device = pages.apic.transRawMessages.transDetails.getValueOf(APICTransRawVal.DEVICE);
			Assert.assertTrue(actual_device.equals(device), "actual_device dosn't equals " + device);
			String actual_domain = pages.apic.transRawMessages.transDetails.getValueOf(APICTransRawVal.DOMAIN);
			Assert.assertTrue(actual_domain.equals(domain), "actual_domain dosn't equals" + domain);
			String actual_trans_id = pages.apic.transRawMessages.transDetails
					.getValueOf(APICTransRawVal.TRANSACTION_ID);
			Assert.assertTrue(actual_trans_id.equals(Transaction_id),
					"actual_trans_id dosn't equals " + Transaction_id);
			String actual_catalog_name = pages.apic.transRawMessages.transDetails
					.getValueOf(APICTransRawVal.CATALOG_NAME);
			Assert.assertTrue(actual_catalog_name.equals(catalog_name),
					"actual_catalog_name dosn't equals " + catalog_name);
			String actual_client_ip = pages.apic.transRawMessages.transDetails.getValueOf(APICTransRawVal.CLIENT_IP);
			Assert.assertTrue(actual_client_ip.equals(client_ip), "actual_client_ip dosn't equals " + client_ip);

		});
		// TODO: In the transaction details page it is still Path URL and not In
		// URL/URI.
//		this.step("Assert fix transactions details", () -> {
//			pages.apic.transRawMessages.transDetails.validateValues(value);
//		});
		this.step("Check Error analysis details", () -> {
			String errorsAnalysis = pages.apic.transRawMessages.transactionAnalysisPanel.getErrorAnalysis();
			// String expected_error = "HTTP/1.1 404 Not Found: No resources match requested
			// URI";
			String expected_error = "Dynamic Execution Error";
			Assert.assertTrue(errorsAnalysis.contains(expected_error),
					"Error analysis dosn't contains " + expected_error);
			String errorsMessage = pages.apic.transRawMessages.transactionAnalysisPanel.getErrorMessage();
			String expectedMessage = "local:///isp/error_template.xsl:85: xsl:message terminate=yes value='HTTP/1.1 404 Not Found: No resources match requested URI'";
			Assert.assertTrue(errorsMessage.equals(expectedMessage), "Error Message dosn't equals " + expectedMessage);
		});
		this.step("Ensure diagrams not exists for early failed transaction with wrong url", () -> {

//			Log.instep("Assert Elapsed times diagram not exists");
//			Assert.assertTrue(pages.apic.transRawMessages.transactionAnalysisPanel.elapsedTimeImage == null);
			Log.instep("Assert no Request Flow");
			Assert.assertTrue(pages.apic.transRawMessages.requestFlowImage == null);
			Log.instep("Assert no Response Flow");
			Assert.assertTrue(pages.apic.transRawMessages.responseFlowImage == null);
		});

	}

	@Test(priority = 0)
	public void earlyFailingInvalidOAuthscope_2() {

		value.clear();
		value.put(APICTransRawVal.API_NAME, "oauth");
		value.put(APICTransRawVal.CLIENT_IP, "172.17.100.131");
		value.put(APICTransRawVal.STATUS, "ERROR");
		value.put(APICTransRawVal.API_VERSION, "1.0.0");
		value.put(APICTransRawVal.PROVIDER_ORG, "montierproviderorg508");
		value.put(APICTransRawVal.PRODUCT_ID, "montierproduct");
		value.put(APICTransRawVal.PRODUCT_NAME, "montierproduct");
		value.put(APICTransRawVal.PLAN_NAME, "default");
		value.put(APICTransRawVal.CONSUMER_ORG_ID, "5c20c011e4b0ad0671d5571d");
		value.put(APICTransRawVal.CONSUMER_ORG, "montierproviderorg508");
		value.put(APICTransRawVal.CONSUMER_APP, "montierapp");
		value.put(APICTransRawVal.CLIENT_ID, "da414b50-06a7-4009-9364-92ed0d1c4dca");

		this.step("Filter API Name " + value.get(APICTransRawVal.API_NAME) + " and Client IP "
				+ value.get(APICTransRawVal.CLIENT_IP), () -> {
					pages.apic.trans.getPageFilters().API_NAME.applyFilter(value.get(APICTransRawVal.API_NAME));
					pages.apic.trans.getPageFilters().CLIENT_IP.applyFilter(value.get(APICTransRawVal.CLIENT_IP));
					pages.apic.trans.refresh();
				});
		this.step("Drill down to investigate transactions details", () -> {
			TableRow row = pages.apic.trans.transTable.table.getRow(1);
			String device = row.getValue(APICTransCols.DEVICE);
			String catalog_space_name = row.getValue(APICTransCols.CATALOG_SPACE);
			String Trans_id = row.getValue(APICTransCols.TRANS_ID);

			pages.apic.earlyFailedPage.clickTransID(row);

			String actual_device = pages.apic.transRawMessages.transDetails.getValueOf(APICTransRawVal.DEVICE);
			Assert.assertTrue(actual_device.equals(device), actual_device + " Device dosn't equals " + device);
			String actual_catalog_space_name = pages.apic.transRawMessages.transDetails
					.getValueOf(APICTransRawVal.CATALOG_NAME) + "/"
					+ pages.apic.transRawMessages.transDetails.getValueOf(APICTransRawVal.SPACE_NAME);
			Assert.assertTrue(actual_catalog_space_name.equals(catalog_space_name),
					actual_catalog_space_name + " catalog/space_name dosn't equals " + catalog_space_name);
			String actual_Trans_id = pages.apic.transRawMessages.transDetails
					.getValueOf(APICTransRawVal.TRANSACTION_ID);
			Assert.assertTrue(actual_Trans_id.equals(Trans_id),
					actual_Trans_id + " Trans_id dosn't equals " + Trans_id);

		});
		this.step("Check transactions details", () -> {
			pages.apic.transRawMessages.transDetails.validateValues(value);
		});
		this.step("Check Error analysis details", () -> {
			String error_analysis = "*[da414b50-06a7-4009-9364-92ed0d1c4dca] Custom scope check encountered an error: scope \"detail\" not allowed*\n"
					+ "Rejected by filter; SOAP fault sent";// + "api-error-response multistep error\n"
			// + "HTTP/1.1 401 Unauthorized:";
			String actual_error_analysis = pages.apic.transRawMessages.transactionAnalysisPanel.getErrorAnalysis();
			Assert.assertTrue(actual_error_analysis.contains(error_analysis),
					actual_error_analysis + " error_analysis dosn't equals " + error_analysis);
			String error_reason = "Unauthorized";
			String actual_error_reason = pages.apic.transRawMessages.transactionAnalysisPanel.getErrorReason();
			Assert.assertTrue(actual_error_reason.equals(error_reason),
					actual_error_reason + " error_reason dosn't equals " + error_reason);
		});
//		this.step("Ensure elapsed time diagrams not exists", () -> {
//			Assert.assertTrue(pages.apic.transRawMessages.transactionAnalysisPanel.elapsedTimeImage == null);
//		});
		this.step("Ensure Request Flow APIC Policies exists and stopped in oauth2-server policy", () -> {
			pages.apic.transRawMessages.requestFlowImage.getPoliciesList().buildPoliciesList(true);
			pages.apic.transRawMessages.requestFlowImage.clickOnPolicy("oauth2-server");
			pages.apic.transRawMessages.requestFlowImage.getApicPoliciesDetailsPanel().close();

		});
	}

	@Test(priority = 0)
	public void earlyFailingScopeForbidden_3() {

		value.clear();
		value.put(APICTransRawVal.API_NAME, "accounts");
		value.put(APICTransRawVal.CLIENT_IP, "172.17.100.130");
		value.put(APICTransRawVal.STATUS, "ERROR");
		value.put(APICTransRawVal.API_VERSION, "1.0.1");
		value.put(APICTransRawVal.QUEARY_STRING, "customer_id=7777777");
		value.put(APICTransRawVal.PROVIDER_ORG, "montierproviderorg508");
		value.put(APICTransRawVal.PRODUCT_ID, "montierproduct");
		value.put(APICTransRawVal.PRODUCT_NAME, "montierproduct");
		value.put(APICTransRawVal.PLAN_NAME, "default");
		// value.put(APICTransRawVal.PLAN_VERSION, "1.0.0");
		value.put(APICTransRawVal.HTTP_METHOD, "GET");
		value.put(APICTransRawVal.CONSUMER_ORG_ID, "5c20c011e4b0ad0671d5571d");
		value.put(APICTransRawVal.CONSUMER_ORG, "montierproviderorg508");
		value.put(APICTransRawVal.CONSUMER_APP, "montierapp");
		value.put(APICTransRawVal.CLIENT_ID, "da414b50-06a7-4009-9364-92ed0d1c4dca");
		value.put(APICTransRawVal.IN_URL,
				"https://172.17.100.70:443/montierproviderorg508/montiercatalog/accounts-101/balance");

		String error_reason = "Forbidden";

		this.step("Filter Error Reason by " + error_reason, () -> {
			pages.apic.trans.getPageFilters().ERROR_REASON.applyFilter(error_reason);
			pages.apic.trans.refresh();
		});
		this.step("Drill down to investigate transactions details", () -> {

			TableRow row = pages.apic.trans.transTable.table.getRow(1);
			String device = row.getValue(APICTransCols.DEVICE);
			String catalog_space_name = row.getValue(APICTransCols.CATALOG_SPACE);
			String Trans_id = row.getValue(APICTransCols.TRANS_ID);

			pages.apic.earlyFailedPage.clickTransID(row);

			String actual_device = pages.apic.transRawMessages.transDetails.getValueOf(APICTransRawVal.DEVICE);
			Assert.assertTrue(actual_device.equals(device), actual_device + " Device dosn't equals " + device);
			String actual_catalog_space_name = pages.apic.transRawMessages.transDetails
					.getValueOf(APICTransRawVal.CATALOG_NAME) + "/"
					+ pages.apic.transRawMessages.transDetails.getValueOf(APICTransRawVal.SPACE_NAME);
			Assert.assertTrue(actual_catalog_space_name.equals(catalog_space_name),
					actual_catalog_space_name + " catalog/space_name dosn't equals " + catalog_space_name);
			String actual_Trans_id = pages.apic.transRawMessages.transDetails
					.getValueOf(APICTransRawVal.TRANSACTION_ID);
			Assert.assertTrue(actual_Trans_id.equals(Trans_id),
					actual_Trans_id + " Trans_id dosn't equals " + Trans_id);

			Log.instep("Assert FE Response Code is 403");
			String actual_FE = pages.apic.transRawMessages.transDetails.getValueOf(APICTransRawVal.FE_RESPONSE_CODE);
			Assert.assertEquals(actual_FE, "403");

		});

		// TODO: In the transaction details page it is still Path URL and not In
		// URL/URI.
//		this.step("Assert  Others fix transaction details:", () -> {
//			pages.apic.transRawMessages.transDetails.validateValues(value);
//
//		});

		this.step("Check Error analysis details", () -> {

			String error_analysis = "*[da414b50-06a7-4009-9364-92ed0d1c4dca] Custom scope check encountered an error: invalid_scope: did not match the requested resource: [\"Details\"]*\n"
					+ "Rejected by filter; SOAP fault sent"; // + "api-error-response multistep error\n"
			// + "HTTP/1.1 403 Forbidden: Internal Server Error\n"
			// + "oauth-scope-ck: policy: missing required scopes:";
			String actual_error_analysis = pages.apic.transRawMessages.transactionAnalysisPanel.getErrorAnalysis();
			Assert.assertTrue(actual_error_analysis.equals(error_analysis),
					actual_error_analysis + " error_analysis dosn't equals " + error_analysis);
			String error_message = "Internal Server Error";
			String actual_error_message = pages.apic.transRawMessages.transactionAnalysisPanel.getErrorMessage();
			Assert.assertTrue(actual_error_message.contains(error_message),
					actual_error_message + " error_message dosn't equals " + error_message);
			String actual_error_reason = pages.apic.transRawMessages.transactionAnalysisPanel.getErrorReason();
			Assert.assertTrue(actual_error_reason.contains(error_reason),
					actual_error_reason + " error_reason dosn't equals " + error_reason);
		});

//		this.step("Ensure elapsed time diagrams not exists", () -> {
//			Assert.assertTrue(pages.apic.transRawMessages.transactionAnalysisPanel.elapsedTimeImage == null);
//		});

		this.step("Ensure Requrst Flow APIC Policies exists and stopped in policy apim.security", () -> {
			Log.instep(
					"Ensure request flow contains apim.security internal type.(Internal → apim.security → internal)");
			ApicPolicy apim_security = pages.apic.transRawMessages.requestFlowImage.getPoliciesList()
					.getApimsecurityInternal();
			Assert.assertTrue(apim_security != null);
			Log.instep("validate apic policy details");

			Log.instep("Assert sum policies elapsed time is transaction elapsed time (for now probably fail)");
			policies_ms = pages.apic.transRawMessages.requestFlowImage.getSumPoliciesMS();
			image_ms = TestUtils.StringUtils
					.parseMS(pages.apic.transRawMessages.transactionAnalysisPanel.getElapsedTime());
			Assert.assertEquals(policies_ms, image_ms);

			Log.instep("Assert no Response Flow");
			Assert.assertTrue(pages.apic.transRawMessages.responseFlowImage == null);
		});
	}

	@Test(priority = 0)
	public void TransactionsMissingField_4() {

		value.clear();
		value.put(APICTransRawVal.API_NAME, "accounts");
		value.put(APICTransRawVal.CLIENT_IP, "172.17.100.130");
		value.put(APICTransRawVal.STATUS, "ERROR");
		value.put(APICTransRawVal.API_VERSION, "1.0.1");
		value.put(APICTransRawVal.QUEARY_STRING, "");// customer_id=7777777
		value.put(APICTransRawVal.PROVIDER_ORG, "montierproviderorg508");
		value.put(APICTransRawVal.PRODUCT_ID, "montierproduct");
		value.put(APICTransRawVal.PRODUCT_NAME, "montierproduct");
		value.put(APICTransRawVal.PLAN_NAME, "default");
		value.put(APICTransRawVal.HTTP_METHOD, "POST");
		value.put(APICTransRawVal.CONSUMER_ORG_ID, "5c20c011e4b0ad0671d5571d");
		value.put(APICTransRawVal.CONSUMER_ORG, "montierproviderorg508");
		value.put(APICTransRawVal.CONSUMER_APP, "montierapp");
		value.put(APICTransRawVal.CLIENT_ID, "da414b50-06a7-4009-9364-92ed0d1c4dca");
		value.put(APICTransRawVal.IN_URL,
				"https://172.17.100.70:443/montierproviderorg508/montiercatalog/accounts-101/withdraw");
		value.put(APICTransRawVal.OAUTH_TSCOPE, "Operations");
		value.put(APICTransRawVal.OAUTH_RES_OWNER, "da414b50-06a7-4009-9364-92ed0d1c4dca");

		String error_message = "Request is missing a mandatory field";

		this.step("Open Transactions page and filter Error Message by " + error_message, () -> {
			pages.apic.trans.getPageFilters().ERROR_MESSAGE.applyFilter(error_message);
			pages.apic.trans.refresh();
		});
		this.step("Drill down to investigate transactions details", () -> {
			TableRow row = pages.apic.trans.transTable.table.getRow(1);
			String device = row.getValue(APICTransCols.DEVICE);
			String Trans_id = row.getValue(APICTransCols.TRANS_ID);
			String status = row.getValue(APICTransCols.STATUS);

			pages.apic.trans.transTable.clickTransID(row);

			String actual_device = pages.apic.transRawMessages.transDetails.getValueOf(APICTransRawVal.DEVICE);
			Assert.assertTrue(actual_device.equals(device), actual_device + " Device dosn't equals " + device);
			String actual_Trans_id = pages.apic.transRawMessages.transDetails
					.getValueOf(APICTransRawVal.TRANSACTION_ID);
			Assert.assertTrue(actual_Trans_id.equals(Trans_id),
					actual_Trans_id + " Trans_id dosn't equals " + Trans_id);

			Log.instep("Assert FE Response Code is 500");
			String actual_FE = pages.apic.transRawMessages.transDetails.getValueOf(APICTransRawVal.FE_RESPONSE_CODE);
			Assert.assertEquals(actual_FE, "500");

		});
		// TODO: In the transaction details page it is still Path URL and not In
		// URL/URI.
//		this.step("Assert  Others fix transaction details:", () -> {
//			pages.apic.transRawMessages.transDetails.validateValues(value);
//
//		});

		this.step("Check Error analysis details", () -> {
			String error_analysis = "Rejected by filter; SOAP fault sent";
			String actual_error_analysis = pages.apic.transRawMessages.transactionAnalysisPanel.getErrorAnalysis();
			Assert.assertTrue(actual_error_analysis.contains(error_analysis),
					actual_error_analysis + " error_analysis dosn't equals " + error_analysis);
			String actual_error_message = pages.apic.transRawMessages.transactionAnalysisPanel.getErrorMessage();
			Assert.assertTrue(actual_error_message.contains(error_message),
					actual_error_message + " error_message dosn't equals " + error_message);
			String error_reason = "Internal Server Error";
			String actual_error_reason = pages.apic.transRawMessages.transactionAnalysisPanel.getErrorReason();
			Assert.assertTrue(actual_error_reason.contains(error_reason),
					actual_error_reason + " error_reason dosn't equals " + error_reason);
		});

		this.step("Ensure Request Flow Policies exists and stopped in validate policy", () -> {
			Log.instep(
					"Assert request flow contains apim.security internal type. Internal → apim.security → activity-log → validate → Internal");
			pages.apic.transRawMessages.requestFlowImage.clickOnPolicy("activity-log");
			Log.instep("validate each apic policy details");
			// TODO: validate field
//			pages.apic.transRawMessages.requestFlowImage.getApicPoliciesDetailsPanel()
//					.validateTransactionPolicies(trans_policies_data, "validate");
			pages.apic.transRawMessages.requestFlowImage.getApicPoliciesDetailsPanel().close();

		});
//
		// TODO: remove response flow - irrelevant. Check the elapsed time - problem
		// with the simulator / DPOD version ?
//		this.step("Ensure Response Flow Policies exists", () -> {
////			Log.instep(
////					"Assert response flow contains apim.security internal type.  Internal → catch → xslt-returnErrorToUser → Internal");
////			Assert.assertTrue(pages.apic.transRawMessages.responseFlowImage != null);
//
//			// Log.instep("validate apic policy details");
//
//			Log.instep("Assert sum policies elapsed time is transaction elapsed time (for now probably fail)");
//			policies_ms = pages.apic.transRawMessages.requestFlowImage.getSumPoliciesMS();
//			image_ms = TestUtils.StringUtils
//					.parseMS(pages.apic.transRawMessages.transactionAnalysisPanel.getElapsedTime());
//			Assert.assertEquals(policies_ms, image_ms);
//
//		});

		this.step("Ensure there is payload variable customer instead customer_id", () ->

		{
			Log.instep("Go into Payload section and choose Front-End Request tab");
			pages.apic.transRawMessages.tabsPanelElem.selectTab("Payload");
			pages.apic.transPayload.getComponentPanel().selectFrontEndRequest();
			Log.instep("Assert payload contains variable customer but not contains customer_id");
			String payloadValue = pages.apic.transPayload.getComponentPanel().getPayloadVal();
			Assert.assertTrue(payloadValue.contains("customer") && !payloadValue.contains("customer_id"),
					"Payload not contains as expected. Payload = "
							+ pages.apic.transPayload.getComponentPanel().getPayloadVal());
		});
	}

	@Test(priority = 0)
	public void TransactionsFaultyApplicativeReply5() {

		value.clear();
		value.put(APICTransRawVal.API_NAME, "accounts");
		value.put(APICTransRawVal.CLIENT_IP, "172.17.100.130");
		value.put(APICTransRawVal.STATUS, "OK");
		value.put(APICTransRawVal.API_VERSION, "1.0.3");
		value.put(APICTransRawVal.PROVIDER_ORG, "montierproviderorg508");
		value.put(APICTransRawVal.PRODUCT_ID, "montierproduct");
		value.put(APICTransRawVal.PRODUCT_NAME, "montierproduct");
		value.put(APICTransRawVal.PLAN_NAME, "default");
		value.put(APICTransRawVal.HTTP_METHOD, "POST");
		value.put(APICTransRawVal.CONSUMER_ORG_ID, "5c20c011e4b0ad0671d5571d");
		value.put(APICTransRawVal.CONSUMER_ORG, "montierproviderorg508");
		value.put(APICTransRawVal.CONSUMER_APP, "montierapp");
		value.put(APICTransRawVal.CLIENT_ID, "da414b50-06a7-4009-9364-92ed0d1c4dca");
		value.put(APICTransRawVal.IN_URL,
				"https://172.17.100.70:443/montierproviderorg508/montiercatalog/accounts-103/withdraw");
		value.put(APICTransRawVal.OAUTH_TSCOPE, "Operations");
		value.put(APICTransRawVal.OAUTH_RES_OWNER, "da414b50-06a7-4009-9364-92ed0d1c4dca");

		this.step("Open Transactions page and filter Api Name by " + value.get(APICTransRawVal.API_NAME)
				+ " and Api Version by " + value.get(APICTransRawVal.API_VERSION), () -> {
					pages.apic.trans.getPageFilters().API_NAME.applyFilter(value.get(APICTransRawVal.API_NAME));
					pages.apic.trans.getPageFilters().API_VERSION.applyFilter(value.get(APICTransRawVal.API_VERSION));
					pages.apic.trans.refresh();
				});
		this.step("Drill down to investigate transaction details", () -> {
			TableRow row = pages.apic.trans.transTable.table.getRow(1);
			String device = row.getValue(APICTransCols.DEVICE);
			String Trans_id = row.getValue(APICTransCols.TRANS_ID);
			elapsed_time = row.getValue(TransCols.ELAPSED);

			pages.apic.trans.transTable.clickTransID(row);

			String actual_device = pages.apic.transRawMessages.transDetails.getValueOf(APICTransRawVal.DEVICE);
			Assert.assertTrue(actual_device.equals(device), actual_device + " Device dosn't equals " + device);
			String actual_Trans_id = pages.apic.transRawMessages.transDetails
					.getValueOf(APICTransRawVal.TRANSACTION_ID);
			Assert.assertTrue(actual_Trans_id.equals(Trans_id),
					actual_Trans_id + " Trans_id dosn't equals " + Trans_id);

			Log.instep("Assert FE Response Code is 500");
			String actual_FE = pages.apic.transRawMessages.transDetails.getValueOf(APICTransRawVal.FE_RESPONSE_CODE);
			Assert.assertEquals(actual_FE, "200");

		});

//		this.step("Assert  Others fix transaction details:", () -> {
//			pages.apic.transRawMessages.transDetails.validateValues(value);
//
//		});

		this.step("Check Error analysis details", () ->

		{
			String error_analysis = "No errors detected";
			String actual_error_analysis = pages.apic.transRawMessages.transactionAnalysisPanel.getErrorAnalysis();
			Assert.assertTrue(actual_error_analysis.contains(error_analysis),
					actual_error_analysis + " error_analysis dosn't equals " + error_analysis);
		});

		this.step("Ensure elapsed time diagrams exists", () -> {
			Assert.assertTrue(pages.apic.transRawMessages.transactionAnalysisPanel.elapsedTimeImage != null);
			// Log.instep("Assert elapsed time of transaction same as Elapsed Time.");//TODO
			// BUG
			// Assert.assertTrue(elapsed_time.equals(pages.apic.transRawMessages.transactionAnalysisPanel.getElapsedTime()));//not
			// same elapsed
			// time
			// pages.apic.transRawMessages.validateElapsedTimeImageVsFlows();// FAIL.. BUG
			// IN RESPONSE

			// TODO: Time summary not counting frontEndBox - not receiving the element
//			Log.instep("Assert sum policies elapsed time is transaction elapsed time (for now probably fail)");
//			policies_ms = pages.apic.transRawMessages.requestFlowImage.getSumPoliciesMS();
//			image_ms = TestUtils.StringUtils
//					.parseMS(pages.apic.transRawMessages.transactionAnalysisPanel.getElapsedTime());
//			Assert.assertEquals(policies_ms, image_ms);

		});

		this.step("Ensure whole Request Flow Policies exists.", () -> {
			Log.instep("Assert request flow contains apim.security internal type");
			ApicPolicy apim_security = pages.apic.transRawMessages.requestFlowImage.getPoliciesList()
					.getApimsecurityInternal();
			Assert.assertTrue(apim_security != null);

		});
		this.step("Check invalid data of variable GetcustomerBalance", () -> {
			Log.instep("Click on GetcustomerBalance");
			pages.apic.transRawMessages.requestFlowImage.clickOnPolicy("GetcustomerBalance");
			Log.instep("Under Context variable assert CustomerBalance = -99999");
			context_variable = pages.apic.transRawMessages.requestFlowImage.getApicPoliciesDetailsPanel()
					.getCurrentPolicy().getContextVariables();

			details_context_variable = context_variable.get(0).get("Name");
			Assert.assertTrue(details_context_variable.equals("CustomerBalance"));

			details_context_variable = context_variable.get(0).get("Value");
			Assert.assertTrue(details_context_variable.equals("-99999"));
			pages.apic.transRawMessages.requestFlowImage.getApicPoliciesDetailsPanel().close();

		});

		this.step("Check all side calls of transaction exists in chart policies", () -> {
			pages.apic.transRawMessages.tabsPanelElem.selectTab("Side Calls");
			Log.instep("For each row when value of name is not internal: Assert there is a policy with same data");
			policy = pages.apic.sideCallsPage.requestFlowImage.getAllSideCalls();
			pages.apic.sideCallsPage.requestFlowImage.getApicPoliciesDetailsPanel().close();
			String policy_name;
			int num_rows = pages.apic.sideCallsPage.getComponentPanel().sideCallsTable.getNumRows();
			for (int i = 0; i < num_rows; i++) {
				TableRow row = pages.apic.sideCallsPage.getComponentPanel().sideCallsTable.getRow(i);
				policy_name = row.getCeil(APICSideCallsCols.POLICY_TITLE).getText();
				if (!policy_name.equals("Internal")) {
					Assert.assertEquals(policy.get(policy_name).get(0).get("Details"),
							row.getCeil(SideCallsCols.DETAILS).getText());
					Assert.assertEquals(policy.get(policy_name).get(0).get("Type"),
							row.getCeil(SideCallsCols.TYPE).getText());
					Assert.assertEquals(policy.get(policy_name).get(0).get("Status"),
							row.getCeil(SideCallsCols.STATUS).getText());
					Assert.assertEquals(policy.get(policy_name).get(0).get("Elapsed (ms.)"),
							row.getCeil(SideCallsCols.ELASPED).getText());
				}
			}

		});

	}
}
