package org.montier.qa.automation.pages;

import org.montier.qa.automation.base.PageObject;

public class ReportsPage extends PageObject {

	public ReportsPage() {
		super();
	}

	@Override
	protected void initValidationValues() {
		//
	}

	@Override
	public void initPageElement() {
		// empty override
	}

	@Override
	public PageFilters getPageFilters() {
		return null;
	}
}
