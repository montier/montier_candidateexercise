package org.montier.qa.automation.pages;

import java.util.Arrays;

import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.PagesFactory;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.montier.qa.automation.base.utils.Table.TableRow;
import org.testng.Assert;

public class UsersPage extends PageObject {
	public static class UserCols extends TableCols {
		public static final Col NAME = new Col("Name");
		public static final Col DESCRIPTION = new Col("Description");

		public UserCols() {
			super();
		}

		@Override
		public void setColsIndices() {
			colsIndices.addAll(Arrays.asList(NAME, DESCRIPTION));
		}
	}

	@Find(by = FBy.className, value = "UsersTable")
	public ByPath users_table;

	public Table usersTable;

	public UsersPage() {
		super();
	}

	@Override
	public PageFilters getPageFilters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void initPageElement() {
		this.usersTable = new Table(this.findElement(users_table), new UserCols());
	}

	public UserViewPage clickOnUser(String user_name) {
		Log.debug("Find user " + user_name + " in users table and click");
		TableRow row = usersTable.getRowValueEqual(UserCols.NAME, user_name);
		if (row == null) {
			Assert.fail("No user " + user_name + " as expected");
		} else
			row.click(UserCols.NAME);
		PagesFactory.getPages().userViewPage.openPage(user_name);
		return PagesFactory.getPages().userViewPage;
	}
}
