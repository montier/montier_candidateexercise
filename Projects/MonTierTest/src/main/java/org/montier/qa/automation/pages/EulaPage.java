package org.montier.qa.automation.pages;

import java.util.concurrent.TimeUnit;

import org.montier.qa.automation.base.Driver;
import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.utils.ByPath;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.toolkit.elements.WhatsNewModal;

public class EulaPage extends PageObject {
	@Find(by = FBy.xpath, value = "//button[contains(text(),'I Agree')]")
	public ByPath agreeButton;

	WhatsNewModal whatNewModal;

	public EulaPage() {
		super();
	}

	public void whatsNewCheckAndClick() {
		// TODO: change it to force_exists = true when we really sure we running in the first time, else it may be already closed even if it in eula
		// page
		whatNewModal.checkAndClick(false);
	}

	public void agreeLicense() {
		// agreeButton.click();
		Driver.getInstance().scrollPageEnd();
		try {
			// TODO: wait for faded message (time count of somethind) disappear
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		this.findElement(agreeButton).click();
	}

	@Override
	public PageFilters getPageFilters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void initPageElement() {
		whatNewModal = new WhatsNewModal(false);
	}
}
