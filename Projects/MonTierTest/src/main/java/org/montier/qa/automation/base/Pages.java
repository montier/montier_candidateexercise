package org.montier.qa.automation.base;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

import org.montier.qa.automation.base.utils.RunTestException;
import org.montier.qa.automation.elements.SideMenu;
import org.montier.qa.automation.elements.TopPanel.ProductType;

public abstract class Pages {

	protected Pages() {
		initPages();
	}

	private void initPages() {
		Class<?> clz = this.getClass();
		do {
			Field[] flds = clz.getDeclaredFields();
			for (Field fld : flds) {
				if (fld.getType().getName().startsWith("org.montier.qa.automation.pages.")) {
					Log.debug("init page " + fld.getName());
					Constructor<?> page_ctor = null;
					Object page_inst = null;
					try {
						page_ctor = fld.getType().getConstructor();
					} catch (NoSuchMethodException | SecurityException e) {
						e.printStackTrace();
						throw new RunTestException("Failed to Find constructor of " + fld.getName() + "\n Exception message: " + e.getMessage(), e);
					}
					try {
						page_inst = page_ctor.newInstance();
					} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						Log.error("Failed to initiate constructor of  " + fld.getName()
								+ " Check that class is not abstrct and method is public and arguments. \n Exception message: " + e.getMessage());
						e.printStackTrace();
						throw new RunTestException("Failed to initiate constructor of  " + fld.getName()
								+ " Check that class is not abstrct and method is public and arguments. \n Exception message: " + e.getMessage(), e);

					}

					try {
						fld.set(this, page_inst);
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
						throw new RunTestException(
								"Failed to set constructor instant to field " + fld.getName() + "\n Exception message: " + e.getMessage(), e);
					}
				}
			}
			clz = clz.getSuperclass();
		} while (clz != Pages.class);
	}

	public PageObject getProductDefaultPage(String menu_name, Boolean open_page, Boolean reset_filters) {
		if (reset_filters) {
			Log.debug("Reset filters");
			PageObject.getFilterElem().resetFilters(false, false);
		}
		PageObject page = this.implementProductDefaultPage(menu_name);
		if (open_page) {
			Log.debug("open product default page " + page.getClass().getSimpleName());
			SideMenu.openPage(page);
		}
		return page;
	}

	protected abstract PageObject getProductDefaultPage(ProductType product_type);

	protected abstract PageObject implementProductDefaultPage(String menu_name);

	public void refresh(Class<?> cls) {
		PageObject p = null;
		for (Field fld : this.getClass().getDeclaredFields())
			if (fld.getClass() == cls) {
				try {
					if (fld.get(this) == null)
						fld.set(this, cls.getConstructor().newInstance());
					p = (PageObject) fld.get(this);
				} catch (IllegalArgumentException | IllegalAccessException | InstantiationException | InvocationTargetException
						| NoSuchMethodException | SecurityException e) {
					throw new RunTestException(e.getMessage());
				}
				p.refresh();
				break;
			}
		PagesFactory.getInstance().currentPage = p;
	}

	public PageObject openPage(String page_name, String... args) {
		Log.debug("Open page " + page_name);
		ArrayList<Field> fields = getAllFields(this.getClass());
		Field fld = fields.stream().filter(field -> field.getType().getSimpleName().equals(page_name)).collect(Collectors.toList()).get(0);
		PageObject page = null;
		try {
			page = (PageObject) fld.get(this);
			page.openPage(args);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		PagesFactory.getInstance().currentPage = page;
		return page;
	}

	private ArrayList<Field> getAllFields(Class<?> cls) {
		ArrayList<Field> fields = new ArrayList<>();
		while (!cls.getName().equals(Pages.class.getName())) {
			fields.addAll(Arrays.asList(cls.getDeclaredFields()));
			cls = cls.getSuperclass();
		}
		return fields;
	}
}
