package org.montier.qa.automation.pages.idg;

import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.elements.SVGCountChartElement;
import org.montier.qa.automation.elements.SVGLineChartElement;
import org.openqa.selenium.WebElement;

public class IDGServiceActivityPage extends PageObject {
	public class ActivityFilters extends PageFilters {
		public FilterUtils TIME = new FilterUtils("Time");
		public FilterUtils DEVICE = new FilterUtils("Device");
		public FilterUtils DOMAIN = new FilterUtils("Domain");
		public FilterUtils SERVICE = new FilterUtils("Service");
		public FilterUtils STATUS = new FilterUtils("Status");
	}

	@Find(by = FBy.className, value = "ServiceTransactionsCountChart", init = true)
	WebElement serviceTransactionsCountChart;
	@Find(by = FBy.className, value = "ServiceTransactionsChart", init = true)
	WebElement serviceTransactionsChart;

	public SVGCountChartElement transCountChart;
	public SVGLineChartElement transChart;

	public IDGServiceActivityPage() {
		super();
	}

	@Override
	public void refresh() {
		super.refresh();
		this.initCharts();
	}

	@Override
	public void initPageElement() {
		// this.filter_Utils.checkFilters(page_filters);
	}

	public void initCharts() {
		this.transCountChart = new SVGCountChartElement(serviceTransactionsCountChart);
		this.transCountChart.parseChartLines();
		this.transChart = new SVGLineChartElement(serviceTransactionsChart);
		this.transChart.parseChartLines();
	}

	@Override
	public ActivityFilters getPageFilters() {
		if (this.pageFilters == null)
			this.pageFilters = new ActivityFilters();
		return (ActivityFilters) this.pageFilters;
	}
}
