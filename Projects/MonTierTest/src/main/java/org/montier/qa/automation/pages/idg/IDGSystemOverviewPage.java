package org.montier.qa.automation.pages.idg;

import java.util.Arrays;

import org.montier.qa.automation.base.PageObject;
import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.montier.qa.automation.elements.SVGLineChartElement;
import org.openqa.selenium.WebElement;

public class IDGSystemOverviewPage extends PageObject {
	public static class SystemErrorsCols extends TableCols {
		public static final Col DEVICE = new Col("Device");
		public static final Col CATEGORY = new Col("Category");
		public static final Col SEVERITY = new Col("Severity");
		public static final Col TIME = new Col("Time");
		public static final Col OBJECT_TYPE = new Col("Object Type");
		public static final Col OBJECT_NAME = new Col("Object Name");
		public static final Col MESSAGE = new Col("Message");

		public SystemErrorsCols() {
			super();
		}

		@Override
		public void setColsIndices() {
			colsIndices.addAll(Arrays.asList(DEVICE, CATEGORY, SEVERITY, TIME, OBJECT_TYPE, OBJECT_NAME, MESSAGE));
		}
	}

	@Find(by = FBy.className, value = "SystemErrorsGrid")
	WebElement systemErrorsGrid;
	@Find(by = FBy.className, value = "SystemActivityChart")
	WebElement systemActivityChart;
	@Find(by = FBy.className, value = "DeviceResourcesCpuChart")
	WebElement deviceResourcesCpuChart;
	@Find(by = FBy.className, value = "DeviceResourcesMemoryChart")
	WebElement deviceResourcesMemoryChart;

	public SVGLineChartElement SystemActivityChart;
	public SVGLineChartElement DeviceResourcesCpuChart;
	public SVGLineChartElement DeviceResourcesMemoryChart;
	public Table SystemErrorsTable;

	public class OverviewFilters extends PageFilters {

		public FilterUtils TIME = new FilterUtils("Time");
		public FilterUtils DEVICE = new FilterUtils("Device");
	}

	public IDGSystemOverviewPage() {
		super();
	}

	public IDGSystemOverviewPage(boolean init_element) {
		super();
		if (init_element)
			this.initPageElement();
	}

	@Override
	public void initPageElement() {
		this.SystemActivityChart = new SVGLineChartElement(systemActivityChart);
		this.DeviceResourcesCpuChart = new SVGLineChartElement(deviceResourcesCpuChart);
		this.DeviceResourcesMemoryChart = new SVGLineChartElement(deviceResourcesMemoryChart);
		this.SystemErrorsTable = new Table(systemErrorsGrid, new SystemErrorsCols());
	}

	@Override
	public OverviewFilters getPageFilters() {
		if (this.pageFilters == null)
			this.pageFilters = new OverviewFilters();
		return (OverviewFilters) this.pageFilters;
	}

}
