package org.montier.qa.automation.pages.idg;

import org.montier.qa.automation.base.UsersRoles;
import org.montier.qa.automation.pages.RoleViewPage.Permissions;
import org.montier.qa.automation.pages.base.IDGAbstractTransactionPage;
import org.montier.qa.automation.toolkit.elements.TransactionComponents.RawMessages;

public class IDGTransactionRawMessagesPage extends IDGAbstractTransactionPage {

	public IDGTransactionRawMessagesPage() {
		super();
	}

	@Override
	public void initPageElement() {
		super.initPageElement();
		if (UsersRoles.getPermissionOfCurrentUser(Permissions.AllowRawMessages))
			this.transactionComponents.rawMessages = (RawMessages) this.transactionComponents.getTabContent(RawMessages.class);
	}

	@Override
	public RawMessages getComponentPanel() {
		return this.transactionComponents.rawMessages;
	}
}
