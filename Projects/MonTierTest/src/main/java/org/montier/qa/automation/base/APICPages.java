package org.montier.qa.automation.base;

import org.montier.qa.automation.elements.TopPanel.ProductType;
import org.montier.qa.automation.pages.apic.APICApiLatencyPage;
import org.montier.qa.automation.pages.apic.APICApiPoliciesPage;
import org.montier.qa.automation.pages.apic.APICEarlyFailedPage;
import org.montier.qa.automation.pages.apic.APICRecentActivityPage;
import org.montier.qa.automation.pages.apic.APICServiceUrlCallsPage;
import org.montier.qa.automation.pages.apic.APICTransactionExtendedLatencyPage;
import org.montier.qa.automation.pages.apic.APICTransactionPayloadPage;
import org.montier.qa.automation.pages.apic.APICTransactionRawMessagesPage;
import org.montier.qa.automation.pages.apic.APICTransactionSideCallsPage;
import org.montier.qa.automation.pages.apic.APICTransactionsPage;

public class APICPages extends PagesContainer {
	public APICRecentActivityPage recentActivity;
	public APICTransactionsPage trans;
	public APICTransactionRawMessagesPage transRawMessages;
	public APICTransactionPayloadPage transPayload;
	public APICTransactionSideCallsPage sideCallsPage;
	public APICTransactionExtendedLatencyPage extendLatePage;
	public APICEarlyFailedPage earlyFailedPage;
	public APICApiLatencyPage apiLatencyPage;
	public APICApiPoliciesPage apiPoliciesPage;
	public APICServiceUrlCallsPage apiServiceUrl;

	public APICPages() {
		super();
	}

	@Override
	protected PageObject getProductDefaultPage(ProductType product_type) {
		return this.recentActivity;
	}

	@Override
	public APICTransactionRawMessagesPage getTransRawMessagesPage() {
		return this.transRawMessages;
	}

	@Override
	public APICTransactionPayloadPage getTransPayload() {
		return this.transPayload;
	}

	@Override
	public APICTransactionSideCallsPage getSideCallsPage() {
		return this.sideCallsPage;
	}

	@Override
	public APICTransactionExtendedLatencyPage getExtendLatePage() {
		return this.extendLatePage;
	}

	@Override
	public APICEarlyFailedPage getEarlyFailedPage() {
		return this.earlyFailedPage;
	}

	@Override
	public APICTransactionsPage getTransPage() {
		return this.trans;
	}

	@Override
	public PageObject implementProductDefaultPage(String menu_name) {
		switch (menu_name) {
		case "Dashboards":
			return this.recentActivity;
		case "Manage":
			return this.gatewaysPage;
		case "Reports":
			return this.reportsPage;
		}
		return null;
	}

}