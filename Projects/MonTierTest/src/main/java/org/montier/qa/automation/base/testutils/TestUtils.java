package org.montier.qa.automation.base.testutils;

import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.montier.qa.automation.base.Log;

public class TestUtils {
	public static class ValidationFields {
		private final String[] keysValidation = { "service", "domain", "trans_id", "device" };
		TreeMap<String, String> validationValues;

		public ValidationFields() {
			initValidationValues();
		}

		public void initValidationValues() {
			validationValues = new TreeMap<String, String>();
			for (String key : keysValidation)
				validationValues.put(key, null);
		}

		public void add(String key, String value) {
			key = key.trim().toLowerCase();
			if (validationValues.containsKey(key))
				validationValues.put(key, value);
			else
				Log.error("Not existing key " + key);

		}

		public String get(String key) {
			return validationValues.get(key);
		}

		public boolean equals(ValidationFields currentValidation) {
			for (String key : currentValidation.validationValues.keySet())
				if (currentValidation.validationValues.get(key) != null)
					if (!currentValidation.validationValues.get(key).equals(validationValues.get(key))) {
						Log.error("Failed validation, expected value of " + key + ": " + validationValues.get(key) + "but found: "
								+ currentValidation.validationValues.get(key));
						return false;
					}
			return true;
		}

		public void cleanAll() {
			initValidationValues();
		}
	}

	public static class StringUtils {
		public static boolean compareRegex(String regex_pattern, String str) {
			Pattern r = Pattern.compile(regex_pattern);
			Matcher m = r.matcher(str);
			return m.find();

		}

		public static int parseMS(String ms_text) {
			String number = ms_text.replaceAll("\\D+", "");
			return Integer.parseInt(number);
		}

	}
}
