package org.montier.qa.automation.pages.idg;

import java.util.Arrays;

import org.montier.qa.automation.base.testutils.TestUtils.StringUtils;
import org.montier.qa.automation.base.utils.FilterUtils;
import org.montier.qa.automation.base.utils.Find;
import org.montier.qa.automation.base.utils.Find.FBy;
import org.montier.qa.automation.base.utils.Table;
import org.montier.qa.automation.base.utils.Table.Col;
import org.montier.qa.automation.base.utils.Table.TableCols;
import org.montier.qa.automation.base.utils.Table.TableRow;
import org.montier.qa.automation.pages.base.AbstractExplorePage;
import org.openqa.selenium.WebElement;

public class IDGServicesConfiguration extends AbstractExplorePage {
	public static class SrvCnfCols extends TableCols {
		public static final Col DEVICE = new Col("Device");
		public static final Col DOMAIN = new Col("Domain");
		public static final Col SERVICE = new Col("Service Name"); /* FRONT_SIDE("Front-Side"), */
		public static final Col FRONT_SIDE_HANDLER = new Col("Front-Side Handler");
		public static final Col SERVICE_TYPE = new Col("Service Type");
		public static final Col FRONT_URI = new Col("Front URI");

		@Override
		public void setColsIndices() {
			this.colsIndices.addAll(Arrays.asList(DEVICE, DOMAIN, SERVICE, FRONT_SIDE_HANDLER, SERVICE_TYPE, FRONT_URI));
		}
	}

	public class ConfigurationFilters extends PageFilters {
		public FilterUtils DEVICE = new FilterUtils("Device");
		public FilterUtils DOMAIN = new FilterUtils("Domain");
		public FilterUtils SERVICE = new FilterUtils("Service");
		public FilterUtils FRONTSIDEHANDLER = new FilterUtils("Front-Side Handler");
		public FilterUtils SERVICETYPE = new FilterUtils("Service Type");
		public FilterUtils FRONTURI = new FilterUtils("Front URI");
	}

	@Find(by = FBy.className, value = "WdpServicesConfigurationGrid")
	WebElement service_conf_table;

	public Table srvcCnfgTable;

	public IDGServicesConfiguration() {
		super();
	}

	@Override
	public void initPageElement() {
		super.initPageElement();
		this.srvcCnfgTable = new Table(service_conf_table, new SrvCnfCols());
	}

	public boolean checkServiceType(int row, String service_type) {
		String regex_pattern = "";
		if (service_type == "soap")
			regex_pattern = "WSGateway \\(soap-1+[1,2]\\)";
		TableRow trow = this.srvcCnfgTable.getRow(row);
		return StringUtils.compareRegex(regex_pattern, trow.getCeil(SrvCnfCols.SERVICE_TYPE).getText());
	}

	@Override
	public ConfigurationFilters getPageFilters() {
		if (this.pageFilters == null)
			this.pageFilters = new ConfigurationFilters();
		return (ConfigurationFilters) this.pageFilters;
	}

}
