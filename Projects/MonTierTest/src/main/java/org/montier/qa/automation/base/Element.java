package org.montier.qa.automation.base;

import org.montier.qa.automation.base.utils.RunTestException;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;

public abstract class Element extends SelObject {
	public WebElement baseElement;

	public Element(WebElement base_elem) {
		super();
		this.baseElement = base_elem;
		this.initWebElements();
	}

	@Override
	protected SearchContext getSearchBy() {
		if (this.baseElement != null)
			return this.baseElement;
		else
			return this.driver.getDriver();
	}

	public Element() throws RunTestException {
		super();
		this.baseElement = null;
		this.initWebElements();
	}

	public void click() {
		this.baseElement.click();
	}
}
