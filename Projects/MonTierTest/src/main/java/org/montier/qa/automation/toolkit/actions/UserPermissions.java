package org.montier.qa.automation.toolkit.actions;

import org.montier.qa.automation.base.Log;
import org.montier.qa.automation.elements.SideMenu;
import org.montier.qa.automation.pages.RoleViewPage;
import org.montier.qa.automation.pages.UserViewPage;
import org.montier.qa.automation.pages.UsersPage;

public class UserPermissions {
	public static RoleViewPage loadUserPermissions(String user_name) {
		Log.instep("Go to Users page and click on " + user_name + " user");
		UsersPage users_page = (UsersPage) SideMenu.MenuSelect.Manage_Security_Users.getPage();
		UserViewPage user_view_page = users_page.clickOnUser(user_name);
		Log.instep("In UserViewPage click on user role");
		String user_role = user_view_page.findUserRole();
		Log.info("User role is " + user_role);
		RoleViewPage role_view_page = user_view_page.clickOnRole(user_role);
		return role_view_page;
	}

}
