package org.montier.qa.automation.elements;

import org.openqa.selenium.WebElement;

public class SVGCountChartElement extends SVGChartElement {
	/**
	 * 
	 * For example: service activity chart, no x line just Y values of objects
	 */
	public SVGCountChartElement(WebElement chart_elem) {
		super(chart_elem);
	}

	public float getValue(String line_name) {
		ChartLine line = new ChartLine(line_name, this.chartElement);
		this.lines.put(line_name, line);
		line.buildTraceBars();
		float y_pix = this.lines.get(line_name).getLinePoints().firstEntry().getValue();
		return this.fromYHeightToValue(y_pix);
	}
}
